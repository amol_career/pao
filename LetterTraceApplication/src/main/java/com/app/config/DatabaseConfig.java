package com.app.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DatabaseConfig {

	   @Bean(name = "dbProductService")
	   @ConfigurationProperties(prefix = "spring.datasource")
	   @Primary
	   public DataSource createDataSource() {
  	      return DataSourceBuilder.create().build();
	   } 
	   
	   @Bean(name = "jdbcProductService")
	   @Autowired
	   public JdbcTemplate createJdbcTemplate_ProductService( DataSource dataSrc) {
	      return new JdbcTemplate(dataSrc);
	   }
}
