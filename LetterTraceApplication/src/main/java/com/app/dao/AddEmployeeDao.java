package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class AddEmployeeDao
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	 @Autowired
	private JdbcTemplate template;
	private Connection con;
	@SuppressWarnings("unused")
	public int storeAddEmployeeData(String empname, String severthid,String designation,String branch)
	{
	      LOGGER.info("Enter into AddEmployeeDao storeAddEmployeeData()");

		String deptName=null;
		String deptid =null;
		String roleId=null;
		String roleId1=null;
		String password="pao@123";
		
		ResultSet rst=null;
	
		
		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
			
		String a = branch;
		System.out.println(a);
		String designationName = designation;
		System.out.println("desig-"+designationName);
	      
		LOGGER.info("Get designationName :: "+designationName);

		
		String getdept ="select deptName,dId  from dept where deptNameMarathi='"+a+"'";
			
			  PreparedStatement stmt2=con.prepareStatement(getdept);
			  rst=stmt2.executeQuery();
			  while(rst.next()) {
			  deptName=rst.getString("deptName");
			  deptid=rst.getString("dId"); 
			  }
			 System.out.println("DeptName::"+deptName+deptid);
		
			 LOGGER.info("DeptName:: "+deptName+deptid);
	 		
		String getdesignationId ="select roll_id,Designation  from pao1 where Designation='"+designationName+"'";
		PreparedStatement stmt5=con.prepareStatement(getdesignationId);
		rst=stmt5.executeQuery();
		while(rst.next())
		{
			
			roleId=rst.getString("roll_id");
		}
		System.out.println("RoleId::"+roleId);
		
		LOGGER.info("RoleId:: "+roleId);
	
			 String sql="insert into aao(empName,sevarthid,designation,branch,deptid,roleId)values(?,?,?,?,?,?)"; 
			 PreparedStatement stmt=con.prepareStatement(sql);
			  
			  stmt.setString(1, empname); 
			  stmt.setString(2, severthid);
			  stmt.setString(3,designation); 
			  stmt.setString(4,branch);
			  stmt.setString(5,deptid);
			  stmt.setString(6,roleId);
			  rowAffect=stmt.executeUpdate();
			  
			  String sql1="insert into "+deptName+ "(EMPLOYEE_NAME,EMPLOYEE_DESIGNATION,SEVARTH_ID)values(?,?,?)";
			  PreparedStatement stmt1=con.prepareStatement(sql1); 
			  stmt1.setString(1, empname);
			  stmt1.setString(3, severthid);
			  stmt1.setString(2, designation);
			 
			System.out.println("Employee into data sucessfully into Dept departmant");
			
			LOGGER.info("Employee into data sucessfully into Dept departmant ");

			rowAffect=stmt1.executeUpdate();
			
			 String sql2="insert into login (sewarthId,deptId,password,roleId)values(?,?,?,?)";
			  PreparedStatement stmt3=con.prepareStatement(sql2); 
			  stmt3.setString(1, severthid);
			  stmt3.setString(2, deptid);
			  stmt3.setString(3, password);
			  stmt3.setString(4, roleId);
			 
			rowAffect=stmt3.executeUpdate();
			
		      LOGGER.info("rowAffect ::"+rowAffect);

			System.out.println("Employee into data sucessfully into Login departmant");
			
			LOGGER.info("Employee into data sucessfully into Login departmant");

			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	     
		LOGGER.info("Exit from AddEmployeeDao storeAddEmployeeData()");
		return rowAffect;
	}
}
