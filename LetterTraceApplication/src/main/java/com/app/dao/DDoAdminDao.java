package com.app.dao;

import com.app.model.DDO;
import com.app.model.DDOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DDoAdminDao extends Dao{
	
	@Autowired
	private JdbcTemplate template;
	private Connection con;
	public int storeDDoData (String ltrdeptName, String senderofficeName,String ddonumber,String ddosection)
	{
		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
		
		
			String sql="insert into ddo(DDO_DESIGNATION_IN_MARATHI,DDO_DESIGNATION_IN_ENGLISH,DDO_NUMBER,SECTION)values(?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrdeptName);
			stmt.setString(2, senderofficeName);
			stmt.setString(3, ddonumber);
			stmt.setString(4, ddosection);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	public int storeBankData (String bankName)
	{
		List<LinkedHashMap> lDataList=new ArrayList<>();
		int rowAffect=0;
		ResultSet rst=null;
		try {
			
			con=template.getDataSource().getConnection();
			
			
			String sql = "SELECT COUNT(*) FROM bank where bank='"+bankName+"'";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			// ResultSet rs = stmt.executeQuery(sql);
			while(rst.next())
			{
			
				String Countrow = rst.getString(1);
				
				if(Countrow.equals("0")){
					
					//int rowAffect=0;
					try {
						
						con=template.getDataSource().getConnection();
						String sqlinsert="insert into bank(bank)values(?)";
						PreparedStatement stmt1=con.prepareStatement(sqlinsert);
						stmt1.setString(1, bankName);
						rowAffect=stmt1.executeUpdate();
						if(con!=null);
						{
							con.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
			
					
					}else{
					System.out.println("Bank Name already exists!");
					}
				
			}
			
		
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	public int storeBankBranchData ( String bankBranchName,int bankid)
	{
		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
		
			String sql1="insert into bankbranches(BankBranchNames,bank_id)values(?,?)";
			PreparedStatement  stmt2 =con.prepareStatement(sql1);
			stmt2.setString(1, bankBranchName);
			stmt2.setInt(2, bankid);
			
			rowAffect=stmt2.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	/*public int storeBankid ( String bankid)
	{
		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
		
		
			String sql="insert into bankbranches(bank_id)values(?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1, bankid);
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	} */

	public List<Map> fetchBank()
	{
		List<Map> lbankList=new ArrayList<>();
		ResultSet rst=null;
		int count=1;
		try {
			
			con=template.getDataSource().getConnection();
			String sql="select * from bank";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> bankNames=new LinkedHashMap<>();
				bankNames.put("id",count++);
				bankNames.put("bank",rst.getString("bank"));
				
				lbankList.add(bankNames);
				
			}
					rst.close();
			//System.out.println("MarathiList:"+lddoMarathiList);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lbankList;
	}

	public List<DDO> getDDOData(String whereClause){
		log.info(getClass().getName() + " :: getDDOData :: where " + whereClause );
		String sql="select * from ddo ";
		if(whereClause!=null && ! "".equalsIgnoreCase(whereClause))
		{
			sql += " where " + whereClause;
		}
		return template.query(sql, new DDOMapper());
	}


}
