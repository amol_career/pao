package com.app.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;

public abstract class Dao {

    protected final Logger log = LoggerFactory.getLogger(Dao.class);

    @Autowired
    protected JdbcTemplate template;

    private Connection connection = null;

    protected Connection getConnection() throws Exception{
        try {
            connection = template.getDataSource().getConnection();
        }catch(Exception exception) {
            String errorMsg = "Error occured while getting conection."+ exception.getMessage();
            log.error(errorMsg);
            throw new Exception(errorMsg);
        }
        return connection;
    }
}
