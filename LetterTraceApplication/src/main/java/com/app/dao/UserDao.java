package com.app.dao;

import com.app.model.User;
import com.app.model.UserMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class UserDao extends Dao {

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String strTimestamp = dateFormat.format(new Date());
    Timestamp timestamp = new Timestamp(new Date().getTime());

    public List<User> getUserList(String whereClause)
    {
        log.info("UserDao ::getUserList()");
        String sql="select * from user ";
        if(whereClause!=null)
        {
            sql+=" where "+ whereClause;
        }
        return template.query(sql,new UserMapper());
    }

    public long createUser( User user){

        log.info("UserDao :: createUser()");

        user.setAddedTz(timestamp);
        user.setUpdatedTz(timestamp);
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(template)
                .withTableName("user")
                .usingGeneratedKeyColumns("id");
        return simpleJdbcInsert.executeAndReturnKey(user.toMap()).longValue();
    }

    public void updateUser ( User user){
        log.info("UserDao :: updateUser()");

        String sql = "update user set nondani_shakha= ?,  user_name= ?,  " +
                " sub_dept_id= ?,  role_id= ?,  updated_by= ?,  updated_tz = ?  where sevarth_id= ? ";

        template.update(sql
                , user.getNondaniShakha()
                , user.getUserName()
                , user.getSubDepartmentId()
                , user.getRoleId()
                , user.getUpdatedBy()
                , strTimestamp
                , user.getSevarthId()
        );
    }

    public boolean deleteUser ( String sevarthId){
        log.info("UserDao :: deleteUser()");

        String sql = "delete from user where sevarthId = ? ";
        return template.update(sql, sevarthId) > 0;
    }

}
