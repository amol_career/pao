package com.app.dao;


import com.app.model.LetterReport1;
import com.app.model.StatsReport1;
import com.app.model.WorkflowMapper;
import com.app.model.WorkflowVO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class WorkflowDao extends Dao {

    @Transactional
    public int addNewWorkflow(WorkflowVO workflowVO){
        log.info("WorkflowDao ::addNewWorkflow()");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String setClause = " isAssigned=false, update_by='"+workflowVO.getFromUser()+"', updated_tz= '2020-01-01 00:00:00'";
        String whereClause = " ltrInwardNum='"+workflowVO.getLtrInwardNum()+"' and isAssigned=true";
        updateLtrWorkflow(setClause ,whereClause);
        int rowAffect=setLtrWorkflow(workflowVO);
        return rowAffect;
    }

    public List<WorkflowVO> getLtrWorkflowList(String whereClause)
    {
        log.info("WorkflowDao ::getLtrWorkflowList()");
        String sql="select * from workflow ";
        if(whereClause!=null)
        {
            sql+=" where "+whereClause;
        }
        return template.query(sql,new WorkflowMapper());
    }
    public int setLtrWorkflow(WorkflowVO workflowVO)
    {
        log.info("WorkflowDao ::setLtrWorkflow()");
        int rowAffect=0;
        String sql = "insert into workflow ( ltrInwardNum, ltrOutwardNum, ltrPargamanNum, fromUser, toUser, ltrStatus , ltrSubject, ltrDate, ltrType , ltrCategory, isAssigned , dept_id , added_by  , added_tz  , update_by, updated_tz , goshwara, route_outward, route_paragaman, route_nondani_shakha)   values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        if(workflowVO != null)
        {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = dateFormat.format(new Date());

            rowAffect = template.update(sql,
                    workflowVO.getLtrInwardNum(),
                    workflowVO.getLtrOutwardNum() != null ? workflowVO.getLtrOutwardNum() : "",
                    workflowVO.getLtrPargamanNum() != null ? workflowVO.getLtrPargamanNum() : "",
                    workflowVO.getFromUser(),
                    workflowVO.getToUser(),
                    workflowVO.getLtrStatus(),
                    workflowVO.getLtrSubject(),
                    workflowVO.getLtrDate(),
                    workflowVO.getLtrType(),
                    workflowVO.getLtrCategory(),
                    1,
                    workflowVO.getDeptId(),
                    workflowVO.getFromUser(),
                    timestamp,
                    workflowVO.getFromUser(),
                    timestamp,
                    workflowVO.getGoshwara() != null ? workflowVO.getGoshwara() : "",
                    workflowVO.getRoute_outward() != null ? workflowVO.getRoute_outward() : "",
                    workflowVO.getRoute_paragaman() != null ? workflowVO.getRoute_paragaman() : "",
                    workflowVO.getRoute_nondani_shakha() );
        }
        return rowAffect;
    }
    public void updateLtrWorkflow(String setClause ,String whereClause)
    {
        log.info("WorkflowDao ::updateLtrWorkflow()");
        String sql="update workflow set ";
        if(whereClause != null)
        {
            sql += setClause + " where " + whereClause;
            template.update(sql);
        }
    }

    public List<Map<String,Object>> getInwardNoAndStatus(List<String> adList, java.sql.Date fromDate, java.sql.Date toDate){
        log.info("WorkflowDao ::getInwardNoAndStatus()");
        String inClause = "";
        for(int i=0;i<adList.size();i++){
            if(i==0) inClause += "'"+adList.get(i)+"'";
            else inClause += ",'"+adList.get(i)+"'";
        }
        String sql = "select id, ltrInwardNum, ltrStatus FROM workflow WHERE id IN (SELECT MAX(id) AS id FROM workflow\n" +
                " WHERE ltrInwardNum in " +
                "(SELECT distinct ltrInwardNum FROM letter where added_by in ( "+inClause+" ) AND" +
                " added_tz >= '"+fromDate+"' AND added_tz <= '"+toDate+"' ) " +
                " GROUP BY ltrInwardNum)";
        log.info("SQL => ::"+sql);
        return template.queryForList(sql);
    }

    public List<Map<String,Object>> getInwardNoAndStatusForAD(List<String> adList, java.sql.Date fromDate, java.sql.Date toDate){
        log.info("WorkflowDao ::getInwardNoAndStatus()");
        //List<String> adList = new ArrayList<>();
        //adList.add("DATSRKF7901"); adList.add("DATNAPM6501");adList.add("DATNAPM6500");adList.add("DATNAPM6507");
        String inClause = "";
        for(int i=0;i<adList.size();i++){
            if(i==0) inClause += "'"+adList.get(i)+"'";
            else inClause += ",'"+adList.get(i)+"'";
        }
        String sql = "select id,ltrStatus, ltrType, count(*) as count FROM workflow WHERE id IN (SELECT MAX(id) AS id FROM workflow\n" +
                " WHERE ltrInwardNum in " +
                "(SELECT distinct ltrInwardNum FROM letter where added_by in ( "+inClause+" ) AND" +
                " added_tz >= '"+fromDate+"' AND added_tz <= '"+toDate+"' ) " +
                " GROUP BY ltrInwardNum) GROUP BY ltrType, ltrStatus ";
        log.info("SQL => ::"+sql);
        return template.queryForList(sql);
    }

    public void getAndUpdatePendencyReportsForADs(final List<String> adList,
                                                 final java.sql.Date fromDate,
                                                 final java.sql.Date toDate,
                                                 final String departId,
                                                 final StatsReport1 statsReport){
        log.info("WorkflowDao ::getPendencyReportsForADs() :: adList :" +adList + ", fromDate:"+fromDate+
                                                    " ,toDate:"+ toDate + " ,departId :" + departId +
                                                    " ,statsReport:"+ statsReport);
        //Pending states : [ltr_status in ('in_progress','accepted','prcessed') anf touser= AD] or [ltr_status= processed ]
        String inClause = "";
        for(int i=0;i<adList.size();i++){
            if(i==0) inClause += "'"+adList.get(i)+"'";
            else inClause += ",'"+adList.get(i)+"'";
        }

        //Opening balance
        StringBuffer openingBalanceSql = new StringBuffer();
        openingBalanceSql.append("SELECT count(*) as opening_balance from workflow where isAssigned= 1 ");

        if(departId!=null && departId.length()>0 && !"ALL".equalsIgnoreCase(departId)) {
            openingBalanceSql.append(" AND dept_id = '"+departId+"'");
        }

        openingBalanceSql.append(" AND (added_tz >= '"+fromDate+"' AND  added_tz <'"+toDate+"') AND ((ltrStatus IN ('in_progress', 'accepted', 'processed') AND toUser in ("+inClause+")) OR (ltrStatus IN ('processed') AND fromUser IN ("+inClause+")))");

        log.info("Opening Balance SQL => ::"+ openingBalanceSql.toString());
        List<Map<String,Object>> pendingCount = template.queryForList(openingBalanceSql.toString());
        if(pendingCount!=null && pendingCount.size()>0){
            Object obj = pendingCount.get(0).get("opening_balance");
            if(obj !=null){
                 statsReport.setOpeningbalance(((Long)obj).intValue());
                log.info("Number of opening balance letters [ "+ ((Long)obj).intValue() +" ] got for AD's :"+ adList);
            }
        }

        //Today letters
        StringBuffer todayLettersSql = new StringBuffer();
        todayLettersSql.append("SELECT count(*) as today_ltr_balance FROM workflow WHERE isAssigned= 1 ");

        if(departId!=null && departId.length()>0 && !"ALL".equalsIgnoreCase(departId)) {
            todayLettersSql.append(" AND dept_id = '"+departId+"'");
        }

        todayLettersSql.append(" AND (added_tz >= '"+toDate+"' AND  added_tz <='"+toDate+"') AND ((ltrStatus IN ('in_progress', 'accepted', 'processed') AND toUser in ("+inClause+")) OR (ltrStatus IN ('processed') AND fromUser IN ("+inClause+")))");
        log.info("Today letters SQL => ::"+ todayLettersSql.toString());
        List<Map<String,Object>> todayLettersCount = template.queryForList(todayLettersSql.toString());
        if(todayLettersCount!=null && todayLettersCount.size()>0){
            Object obj = todayLettersCount.get(0).get("today_ltr_balance");
            if(obj !=null){
                statsReport.setNewLetters(((Long)obj).intValue());
                log.info("Number of new letters [ "+ ((Long)obj).intValue() +" ] got for AD's :"+ adList);
            }
        }

        //Completed letters
        StringBuffer completeLettersSql = new StringBuffer();
        completeLettersSql.append("SELECT count(*) complete_ltr_count FROM workflow WHERE isAssigned= 1  ");

        if( departId!=null && departId.length() > 0  && !"ALL".equalsIgnoreCase(departId) ) {
            completeLettersSql.append("AND dept_id = '"+departId+"'");
        }
        completeLettersSql.append(" AND (added_tz >= '"+toDate+"' AND  added_tz <='"+toDate+"') AND ltrStatus in ('outwarded','complete') AND toUser in ("+inClause+")");
        log.info("Completed letters SQL => ::"+ completeLettersSql.toString());
        List<Map<String,Object>> completeLettersCount = template.queryForList(completeLettersSql.toString());
        if(completeLettersCount!=null && completeLettersCount.size()>0){
            Object obj = completeLettersCount.get(0).get("complete_ltr_count");
            if(obj !=null){
                statsReport.setTodayCompleted(((Long)obj).intValue());
                log.info("Number of complete letters today [ "+ ((Long)obj).intValue() +" ] got for AD's :"+ adList);
            }
        }

        //Total pendency
        statsReport.setPendency(statsReport.getOpeningbalance()+statsReport.getNewLetters()-statsReport.getTodayCompleted());

        log.info("StatsReport updated with pendecncy ::" + statsReport);
    }

    public List<LetterReport1> getPendencyReportsForIndividualAD(final String ad,
                                                                      final java.sql.Date fromDate,
                                                                      final java.sql.Date toDate){
        log.info("WorkflowDao ::getPendencyReportsForIndividualAD() :: ad :"+ ad +" ,fromDate:"+fromDate
                                                                      + " ,toDate:"+toDate);
        List<LetterReport1> result = null;
        LetterReport1 letterReport = null;
        //Opening balance
        //String allPendingSql = "SELECT ltrInwardNum, ltrSubject, ltrDate, ltrType FROM WORKFLOW where isAssigned= 1 AND ((ltrStatus IN ('in_progress', 'accepted', 'processed') AND toUser in ('"+ad+"')) OR (ltrStatus IN ('processed') AND fromUser IN ('"+ad+"'))) AND added_tz >= '"+fromDate+"' AND  added_tz <='"+toDate+"'";
        String allPendingSql = "SELECT ltrInwardNum, ltrSubject, ltrDate, ltrType FROM workflow where isAssigned= 1 AND (added_tz >= '"+fromDate+"' AND  added_tz <='"+toDate+"') AND ((ltrStatus IN ('in_progress', 'accepted', 'processed') AND toUser in ('"+ad+"')) OR (ltrStatus IN ('processed') AND fromUser IN ('"+ad+"')))";
        log.info("All pending letters SQL => ::"+ allPendingSql);
        List<Map<String,Object>> pendingCount = template.queryForList(allPendingSql);
        if(pendingCount!=null && pendingCount.size()>0){
            log.info("All pending letters received ::" + pendingCount);
            result = new ArrayList<>();
            for(Map<String,Object> row : pendingCount){
                letterReport = new LetterReport1();
                letterReport.setInwardNumber(row.get("ltrInwardNum").toString());
                letterReport.setLtrDate(row.get("ltrDate").toString());
                letterReport.setLtrId((Integer)row.get("ltrType"));
                letterReport.setSubject(row.get("ltrSubject").toString());
                result.add(letterReport);
            }
        }
        log.info("List of pending letters [ "+ result +" ] got for AD's :"+ ad);
        return result;
    }
}
