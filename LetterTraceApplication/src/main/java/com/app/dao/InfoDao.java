package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class InfoDao 
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	
	@Autowired
	private JdbcTemplate template;
	private Connection con;
	public int AddInfo (String subject,String date, String info,String file)
	{
	      LOGGER.info("Enter into InfoDao AddInfo()");

		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
		
		
			String sql="insert into infodata values(?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			
			stmt.setString(1, subject);
			stmt.setString(2, date);
			stmt.setString(3, info);
			stmt.setString(4, file);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from InfoDao AddInfo()");

		return rowAffect;
	}
	
	
	public int AddInfo2(String subject,String date, String info,String file)
	{
	      LOGGER.info("Enter into InfoDao AddInfo2()");

		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
		
		
			String sql="insert into outinfo values(?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			
			stmt.setString(1, subject);
			stmt.setString(2, date);
			stmt.setString(3, info);
			stmt.setString(4, file);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from InfoDao AddInfo2()");

		return rowAffect;
	}
	/*
	 * public static List<String> fetchEmpdata(String subject) { // TODO
	 * Auto-generated method stub return null; }
	 */
	
	@SuppressWarnings("rawtypes")
	public List<Map> fetchInfo()
	{
	      LOGGER.info("Enter into InfoDao fetchInfo()");

		List<Map> lDataList=new ArrayList<>();
		ResultSet rst=null;
		int count=1;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from infodata";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("subject",rst.getString("subject"));
				letterMap.put("date", rst.getString("date"));
				letterMap.put("information", rst.getString("info"));
				letterMap.put("file",rst.getString("file") );
				lDataList.add(letterMap);
				
				System.out.println("database file");
			      LOGGER.info("database file inserted in infodata");

			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from InfoDao fetchInfo()");

		return lDataList;
	}
	@SuppressWarnings("rawtypes")
	public List<Map> fetchInfo2()
	{
	    LOGGER.info("Enter into InfoDao fetchInfo2()");

		List<Map> lDataList=new ArrayList<>();
		/*ResultSet rst=null;
		int count=1;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from outinfo";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("subject",rst.getString("subject"));
				letterMap.put("date", rst.getString("date"));
				letterMap.put("information", rst.getString("info"));
				letterMap.put("file",rst.getString("file") );
				lDataList.add(letterMap);
				
				System.out.println("database file");
			
				LOGGER.info("database file inserted in fetchInfo2()");

			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from InfoDao fetchInfo2()");*/

		return lDataList;
	}
     
		
	

}
