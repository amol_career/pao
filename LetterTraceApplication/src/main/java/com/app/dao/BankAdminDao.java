package com.app.dao;

import com.app.model.Bank;
import com.app.model.BankMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BankAdminDao extends Dao{

    @Autowired
    private JdbcTemplate template;

    public List<Bank> getBankData(String whereClause){
        log.info(getClass().getName() + " :: getBankData :: where " + whereClause );
        StringBuffer sql = new StringBuffer();
        sql.append("select * from bank ");
        if(whereClause!=null && ! "".equalsIgnoreCase(whereClause))
        {
            sql.append(" where " + whereClause);
        }
        return template.query(sql.toString(), new BankMapper());
    }
}
