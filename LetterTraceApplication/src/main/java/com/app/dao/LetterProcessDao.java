package com.app.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
//import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class LetterProcessDao
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);


	@Autowired
	private JdbcTemplate template;
	private Connection con;
	
	
	public int storeLetterOneData(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String toDept,String ddoNum,String ddoMrt,String ddoEng,String inwardNum)
	{
	      LOGGER.info("Enter into LetterProcessDao storeAddEmployeeData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypeone(LtrMedium,LtrMediumRef,LtrTypesId,DdoNameMarathi,DdoNameEnglish,DdoNumber,LtrOutNum,LtrDate,LtrSubject,Supplement,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor,InwardNum)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ddoMrt);
			stmt.setString(5,ddoEng);
			stmt.setString(6,ddoNum);
			stmt.setString(7,ltrOutNo);
			stmt.setDate(8,ltrDate);
			stmt.setString(9,ltrSubject);
			stmt.setBoolean(10, supplement);
			stmt.setString(11,toDept);
			stmt.setString(12,salgnaShakha);
			stmt.setString(13,remark);
			stmt.setString(14,ltrAcceptor);
			stmt.setString(15,inwardNum);
			
	
			
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeAddEmployeeData()");

		return rowAffect;
	}
	public int storeLetterTwoData(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String chkNumber,Date chkDate,String amount,String inwardStr)
	{
	      LOGGER.info("Enter into LetterProcessDao storeLetterTwoData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypetwo(LtrMedium,LtrMediumRef,LtrTypesId,LtrOutNum,LtrDate,LtrSubject,Supplement,CheckNumber,CheckDate,amount,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor,InwardNum)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrOutNo);
			stmt.setDate(5,ltrDate);
			stmt.setString(6,ltrSubject);
			stmt.setBoolean(7, supplement);
			stmt.setString(8,chkNumber);	
			stmt.setDate(9,chkDate);
			stmt.setFloat(10,Float.parseFloat(amount));
			stmt.setString(11,fromDept);
			stmt.setString(12,salgnaShakha);
			stmt.setString(13,remark);
			stmt.setString(14,ltrAcceptor);
			stmt.setString(15,inwardStr);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeLetterTwoData()");

		return rowAffect;
	}
	public int storeLetterThreeData(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String chkNumber,Date chkDate,String bankName,String inwardStr)
	{
	      LOGGER.info("Enter into LetterProcessDao storeLetterThreeData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypethree(LtrMedium,LtrMediumRef,LtrTypesId,LtrOutNum,LtrDate,LtrSubject,Supplement,CheckNumber,CheckDate,BankName,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor,InwardNum)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrOutNo);
			stmt.setDate(5,ltrDate);
			stmt.setString(6,ltrSubject);
			stmt.setBoolean(7, supplement);
			stmt.setString(8,chkNumber);	
			stmt.setDate(9,chkDate);
			stmt.setString(10,bankName);
			stmt.setString(11,fromDept);
			stmt.setString(12,salgnaShakha);
			stmt.setString(13,remark);
			stmt.setString(14,ltrAcceptor);
			stmt.setString(15,inwardStr);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeLetterThreeData()");

		return rowAffect;
	}
	public int storeLetterFiveData(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String mobile,String ltrFrom)
	{
	      LOGGER.info("Enter into LetterProcessDao storeLetterFiveData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypefive(LtrMedium,LtrMediumRef,LtrTypesId,LtrFrom,mobile,LtrOutNum,LtrDate,LtrSubject,Supplement,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor)values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrFrom);
			stmt.setString(5,mobile);
			stmt.setString(6,ltrOutNo);
			stmt.setDate(7,ltrDate);
			stmt.setString(8,ltrSubject);
			stmt.setBoolean(9, supplement);
			stmt.setString(10,fromDept);
			stmt.setString(11,salgnaShakha);
			stmt.setString(12,remark);
			stmt.setString(13,ltrAcceptor);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeLetterFiveData()");

		return rowAffect;
	}
	/*
    stmt.setString(3, ltrLocation);
	stmt.setString(8,toDept);
	stmt.setString(10,nagpurLtr);
	stmt.setString(11,subLetter);
	stmt.setString(13,checkNumber);
	stmt.setString(14,bankName);
	stmt.setDate(15,checkDate);
	stmt.setString(16,amount);
	stmt.setString(17,manualLtrMedium);
	stmt.setString(18,mumbaiType);
	stmt.setString(19,subMumbai);
	stmt.setString(20,mumbaiGpf);
	stmt.setString(21,gpfSubSelect);
	stmt.setString(22,raoselect);
	stmt.setString(26,phNum);
	*/
	
	public List<LinkedHashMap> fetchAlldata()
	{
	      LOGGER.info("Enter into LetterProcessDao fetchAlldata()");

		List<LinkedHashMap> lDataList=new ArrayList<>();
		ResultSet rst=null;
		String deptName=null;
		try {
			con=template.getDataSource().getConnection();
			
			
			String sql="select * from lettertypeone ";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				LinkedHashMap<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("LtrTypeOneId",rst.getString("LtrTypeOneId"));
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("DdoNameMarathi",rst.getString("DdoNameMarathi"));
				letterMap.put("DdoNameEnglish",rst.getString("DdoNameEnglish"));
				//letterMap.put("DdoNumber",rst.getString("DdoNumber"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));	
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				lDataList.add(letterMap);
				
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao fetchAlldata()");

		return lDataList;
	}
	public List<LinkedHashMap> fetchDeptLtrdata(String deptId)
	{
	      LOGGER.info("Enter into LetterProcessDao fetchDeptLtrdata()");

		List<LinkedHashMap> lDataList=new ArrayList<>();
		ResultSet rst=null;
		String deptName=null;
		try {
			con=template.getDataSource().getConnection();
			String sqls="select deptNameMarathi from dept where dId=?";
			PreparedStatement pstmt=con.prepareStatement(sqls);
			pstmt.setString(1,deptId);
			ResultSet rslt=pstmt.executeQuery();
			while(rslt.next())
			{
				deptName=rslt.getString("deptNameMarathi");
			}
			
			System.out.println("DeptName From fetchDeptLtrdata()::"+deptName);
			String sql="select * from letter1 where SalgnaShakha=?";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1, deptName);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				LinkedHashMap<String,Object> letterMap=new LinkedHashMap<>();
			//	letterMap.put("LtrTypeOneId",rst.getString("LtrTypeOneId"));
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("DdoNameMarathi",rst.getString("DdoNameMarathi"));
				letterMap.put("DdoNameEnglish",rst.getString("DdoNameEnglish"));
				//letterMap.put("DdoNumber",rst.getString("DdoNumber"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));	
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				lDataList.add(letterMap);
				
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao fetchDeptLtrdata()");

		return lDataList;
	}
	
	public List<String> fetchEmpdata(String deptId)
	{
	      LOGGER.info("Enter into LetterProcessDao fetchEmpdata()");

		List<String> emplist=new ArrayList();
		String deptName=null;
		ResultSet rst=null;
		String empName=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select deptName from dept where dId=?";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,deptId);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				deptName=rst.getString("deptName");
			}
			System.out.println("DeptName::"+deptName);
			String sqltext="select EMPLOYEE_NAME from " + deptName + "";
			PreparedStatement stmtone=con.prepareStatement(sqltext);
			rst=stmtone.executeQuery();
			rst.next();
			while(rst.next())
			{
				empName=rst.getString("EMPLOYEE_NAME");
				emplist.add(empName);
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao fetchEmpdata()");

		return emplist;
	}
	
	public int getTotalLtrCount()
	{
	      LOGGER.info("Enter into LetterProcessDao getTotalLtrCount()");

		int count=0;
		String countstr=null;
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select sum(a.count) total_count from(select count(*) as count from lettertypeone UNION All select count(*) as count from lettertypetwo)a";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				countstr=rst.getString("total_count");
				System.out.println("count from Dao::"+countstr);
			      LOGGER.info("count from Dao::"+countstr);

			}
			if(con!=null);
			{
				con.close();
			}
			return count;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao getTotalLtrCount()");

		return count;
	}
	public int getLtrCount(String ltrType)
	{
	      LOGGER.info("Enter into LetterProcessDao getLtrCount()");
	
		String countstr=null;
		int cnt=0;
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select count(*) as ltr_count from "+ltrType+"";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				countstr=rst.getString("ltr_count");
				System.out.println("ltrCount::"+countstr);
			}
			cnt=Integer.parseInt(countstr);
			return cnt;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 LOGGER.info("Exit into LetterProcessDao getLtrCount()");

		return cnt;
	}
	public List<Map> fetchddoList()
	{
	      LOGGER.info("Enter into LetterProcessDao fetchddoList()");

		
		List<Map> lddoMarathiList=new ArrayList<>();
		ResultSet rst=null;
		int count=1;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from ddo";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("ddoMarathiName",rst.getString("DDO_DESIGNATION_IN_MARATHI"));
				letterMap.put("ddoNameEnglish", rst.getString("DDO_DESIGNATION_IN_ENGLISH"));
				letterMap.put("ddoNumber",rst.getString("DDO_NUMBER"));
				letterMap.put("ddobranch",rst.getString("SECTION") );
				lddoMarathiList.add(letterMap);
				
			}
			System.out.println("MarathiList:"+lddoMarathiList);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao fetchddoList()");

		return lddoMarathiList;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Map> getThirdAdminTblData(String sewrathId)
	{
	      LOGGER.info("Enter into LetterProcessDao getThirdAdminTblData()");

		String empName=null;
		List<Map> dataList=new ArrayList<>();
		
		try {
			con=template.getDataSource().getConnection();
			String sqlOne="select empName from aao where sevarthid='"+sewrathId+"'";
			PreparedStatement stmtOne=con.prepareStatement(sqlOne);
			ResultSet rstOne=stmtOne.executeQuery();
			while(rstOne.next())
			{
				empName=rstOne.getString("empName");
			}
			rstOne.close();
			String sql="select * from ltrfromthird where AllotedEmp='"+empName+"'";
			PreparedStatement stmt=con.prepareStatement(sql);
			ResultSet rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("Inward_no",rst.getString("InwardNo"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("LtrDivision", rst.getString("LtrDivision"));
				letterMap.put("AllotedEmp",rst.getString("AllotedEmp"));
				letterMap.put("LtrTransferDate",rst.getString("LtrTransferDate"));
				dataList.add(letterMap);
				
			}
			
			System.out.println("LtrFromThirdList:"+dataList);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao getThirdAdminTblData()");

		return dataList;
	}
	
	public int storeThirdAdminData(String inward,String ltrSubject,Date ltrTransferDate,String wibhagni,String allotedEmp)
	{ 
	      LOGGER.info("Enter into LetterProcessDao storeThirdAdminData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String sql="insert into ltrfromthird(InwardNo,LtrTransferDate,LtrSubject,LtrDivision,AllotedEmp) values(?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setDate(2, ltrTransferDate);
			stmt.setString(3,ltrSubject);
			stmt.setString(4,wibhagni);
			stmt.setString(5,allotedEmp);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao storeThirdAdminData()");

		return rowAffect;
	}
	
	public int storeFifthAdminData(String inward,String ltrSubject,Date acceptDate,Date completeDate,String goshwara)
	{
	      LOGGER.info("Enter into LetterProcessDao storeFifthAdminData()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String sql="insert into ltrfromfifthadmin(inwardNum,ltrSubject,ltrAcceptDate,ltrCompletionDate,goshwara)values(?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setString(2,ltrSubject);
			stmt.setDate(3,acceptDate);
			stmt.setDate(4,completeDate);
			stmt.setString(5,goshwara);
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao storeFifthAdminData()");

		return rowAffect;
	}
	public int storeLtrAcceptData(String inward, String ltrSubject, Date acceptDate) {
		int rowAffect=0;
		try {
		      LOGGER.info("Enter into LetterProcessDao storeLtrAcceptData()");

			con=template.getDataSource().getConnection();
			String sql="insert into fifthadminacceptltr(InwardNum,LtrSubject,LtrAcceptDate)values(?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setString(2,ltrSubject);
			stmt.setDate(3,acceptDate);
			
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeLtrAcceptData()");

		return rowAffect;
	}
	public List<Map> getFifthAdminAcceptLtr() 
	{
	      LOGGER.info("Enter into LetterProcessDao getFifthAdminAcceptLtr()");

		List<Map> datalist=new ArrayList<>();
		
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from fifthadminacceptltr";
			PreparedStatement stmt=con.prepareStatement(sql);
			ResultSet rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("AcceptDate",rst.getDate("LtrAcceptDate"));
				datalist.add(letterMap);
			}
		    System.out.println("dataFromfifthadminAcceptLtr::"+datalist);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao getFifthAdminAcceptLtr()");

		return datalist;
	}
	public int storePendingLtrAcceptData(String inward, String ltrSubject, Date acceptDate, String goshwara) {
		int rowAffect=0;
		try {
		      LOGGER.info("Enter into LetterProcessDao storePendingLtrAcceptData()");

			con=template.getDataSource().getConnection();
			String sql="insert into fifthadminpendingltr(InwardNum,LtrSubject,LtrDate,Goshwara)values(?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setString(2,ltrSubject);
			stmt.setDate(3,acceptDate);
			stmt.setString(4,goshwara);
			
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storePendingLtrAcceptData()");

		return rowAffect;
		
	}
	public List<Map> getFifthAdminPendingLtr() {
        List<Map> datalist=new ArrayList<>();
		
		try {
		      LOGGER.info("Enter into LetterProcessDao getFifthAdminPendingLtr()");

			con=template.getDataSource().getConnection();
			String sql="select * from fifthadminpendingltr";
			PreparedStatement stmt=con.prepareStatement(sql);
			ResultSet rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("AcceptDate",rst.getDate("LtrDate"));
				letterMap.put("Goshwara",rst.getString("Goshwara"));
				datalist.add(letterMap);
			}
		    System.out.println("dataFromfifthadminPendingLtr::"+datalist);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao getFifthAdminPendingLtr()");

		return datalist;
	}
	public int storeKaryawahiCompleteLtrData(String inward, String ltrSubject, Date acceptDate, String goshwara) {
		int rowAffect=0;
		try {
		      LOGGER.info("Enter into LetterProcessDao storeKaryawahiCompleteLtrData()");

			con=template.getDataSource().getConnection();
			String sql="insert into karyawahicompleteltr(InwardNum,LtrSubject,LtrDate,Goshwara)values(?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setString(2,ltrSubject);
			stmt.setDate(3,acceptDate);
			stmt.setString(4,goshwara);
			
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit into LetterProcessDao storeKaryawahiCompleteLtrData()");
    	return rowAffect;
	}
	public int storeRejectLtrFromFifthAdminData(String inward, String ltrSubject, Date acceptDate) {
		int rowAffect=0;
		try {
		      LOGGER.info("Enter into LetterProcessDao storeRejectLtrFromFifthAdminData()");

			con=template.getDataSource().getConnection();
			String sql="insert into rejectletterfromfifth(InwardNum,LtrTransferDate,LtrSubject)values(?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);
			stmt.setDate(2,acceptDate);
			stmt.setString(3,ltrSubject);
			
			
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao storeRejectLtrFromFifthAdminData()");

		return rowAffect;
	}
	public int removeLtrFromThirdAdminData(String inward) {
		int rowAffect=0;
		try {
		      LOGGER.info("Enter into LetterProcessDao removeLtrFromThirdAdminData()");

			con=template.getDataSource().getConnection();
			String sql="delete from ltrfromthird where InwardNo=?";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,inward);		
			rowAffect=stmt.executeUpdate();
			
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from LetterProcessDao removeLtrFromThirdAdminData()");
		return rowAffect;
	}
	
}
