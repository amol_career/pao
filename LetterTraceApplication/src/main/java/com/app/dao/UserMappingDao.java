package com.app.dao;

import com.app.model.UserMappingMapper;
import com.app.model.UserMapping;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class UserMappingDao extends Dao {

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String strTimestamp = dateFormat.format(new Date());
    Timestamp timestamp = new Timestamp(new Date().getTime());

    public List<UserMapping> getUserMapping(String whereClause){
      log.info("UserMappingDao::getUserMapping()");
      String sql="select * from user_hierarchy ";
      if(whereClause!=null)
      {
          sql+=" where " + whereClause;
      }
      return template.query(sql,new UserMappingMapper());
    }

    public long createUserMapping( UserMapping userMapping){
        log.info("UserMappingDao :: createUserMapping()");

        userMapping.setAddedTz(timestamp);
        userMapping.setUpdatedTz(timestamp);

        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(template)
                .withTableName("user_hierarchy")
                .usingGeneratedKeyColumns("id");
        return simpleJdbcInsert.executeAndReturnKey(userMapping.toMap()).longValue();
    }

    public void updateUserMapping (  UserMapping userMapping){
        log.info("UserMappingDao :: updateUserMapping()");

        String sql = "UPDATE pao_main.user_hierarchy" +
                "SET " +
                " pao = ?," +
                " pao_name = ?," +
                " dypao = ?," +
                " dypao_name = ?," +
                " apao = ?," +
                " apao_name = ?," +
                " so = ?," +
                " so_name = ?," +
                " ad = ?," +
                " ad_name = ?," +
                " sub_dept_id = ?," +
                " updated_by = ?," +
                " updated_tz = ?" +
                " WHERE id = ?";

        template.update(sql
                , userMapping.getPao()
                , userMapping.getPao_name()
                , userMapping.getDypao()
                , userMapping.getDypao_name()
                , userMapping.getApao()
                , userMapping.getApao_name()
                , userMapping.getSo()
                , userMapping.getSo_name()
                , userMapping.getAd()
                , userMapping.getAd_name()
                , userMapping.getSub_dept_id()
                , userMapping.getUpdatedBy()
                , strTimestamp
                , userMapping.getId()
        );
    }

    public boolean deleteUserMapping ( int id){
        log.info("UserMappingDao :: deleteUserMapping()");

        String sql = "delete from user_hierarchy where id = ? ";
        return template.update(sql, id) > 0;
    }


    public List<Map<String,Object>> getUserMappingForReport(String roleName, String reportyRoleName, String sewarthId){
        log.info("UserMappingDao::getUserMappingForReport () :: roleName:"+roleName +" , reportyRoleName:"+reportyRoleName
                 +", sewarthId:"+sewarthId);
        String sql="select "+reportyRoleName+" as reporty, "+reportyRoleName+"_name as reporty_name, ad from user_hierarchy where "+reportyRoleName+" " +
                "in (select distinct ("+reportyRoleName+") from user_hierarchy where "+roleName+" = '"+sewarthId+"');";
        log.info("SQL:"+sql);
        return template.queryForList(sql);
    }

    public List<Map<String,Object>> getUserMappingForReportBreadCrum(String roleName, String sewarthId){
        log.info("UserMappingDao::getUserMappingForReportBreadCrum () :: roleName:"+roleName +" , sewarthId:"+sewarthId);
        String sql="select * from user_hierarchy where "+roleName+" = '"+sewarthId+"'";
        log.info("SQL:"+sql);
        return template.queryForList(sql);
    }
}
