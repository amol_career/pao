package com.app.dao;

import com.app.model.BaseVO;
import com.app.model.WorkflowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;

@Repository
public class CommonLetterDao extends BaseDao{

    @Autowired
    private WorkflowDao workflowDao;

    @Override
    @Transactional
    public int storeData(BaseVO baseVO, WorkflowVO workflowVO) throws Exception {
        int result = 0;
        Connection connection = null;
        PreparedStatement stmt = null;
        try {
            if(baseVO != null){
                connection = getConnection();
                String sql = "insert into letter(ltrId,ltrType,ltrSubject,ltrDate,ltrMedium,ltrRemark,ltrToDept,ltrMediumRef,ltrSenderEnglish,ltrSenderMarathi,\n" +
                        "ltrDdoNumber,ltrSandarbhiyaShakha,ltrSupplement,ltrToSubDept,ltrOutwardNum,ltrInwardNum,ltrOther,ltrMobileNo,\n" +
                        "added_by,added_tz,update_by,updated_tz,chkNumber,chkAmount,chkDate,bankName,raoLtrs,raoDCBills,raoOBARegisters,nondani_shakha,ltrRevised,ltrReceived)" +
                        " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                stmt = connection.prepareStatement(sql);

                stmt.setInt(1,baseVO.getLtrId());
                stmt.setInt(2,baseVO.getLtrType());
                stmt.setString(3,baseVO.getLtrSubject());
                stmt.setDate(4,baseVO.getLtrDate());
                stmt.setString(5,baseVO.getLtrMedium());
                stmt.setString(6,baseVO.getLtrRemark());
                stmt.setString(7,baseVO.getLtrToDept());
                stmt.setString(8,baseVO.getLtrMediumRef());
                stmt.setString(9,baseVO.getLtrSenderEnglish());
                stmt.setString(10,baseVO.getLtrSenderMarathi());
                stmt.setString(11,baseVO.getLtrDdoNumber());
                stmt.setString(12,baseVO.getLtrSandarbhiyaShakha());
                stmt.setBoolean(13,baseVO.isLtrSupplement());
                stmt.setInt(14,baseVO.getLtrToSubDept());
                stmt.setString(15,baseVO.getLtrOutwardNum());
                stmt.setString(16,baseVO.getLtrInwardNum());
                stmt.setBoolean(17,baseVO.isLtrOther());
                stmt.setString(18,baseVO.getLtrMobileNo());
                stmt.setString(19,baseVO.getAdded_by());
                stmt.setTimestamp(20,baseVO.getAdded_tz());
                stmt.setString(21,baseVO.getUpdate_by());
                stmt.setTimestamp(22,baseVO.getUpdated_tz());
                stmt.setString(23,baseVO.getChkNumber());
                stmt.setDouble(24,baseVO.getChkAmount());
                stmt.setDate(25,baseVO.getChkDate());
                stmt.setString(26,baseVO.getBankName());
                stmt.setBoolean(27,baseVO.isRaoLtrs());
                stmt.setBoolean(28,baseVO.isRaoDCBills());
                stmt.setBoolean(29,baseVO.isRaoOBARegisters());
                stmt.setString(30,baseVO.getNondaniShakha());
                stmt.setBoolean(31,baseVO.isLtrRevised());
                stmt.setBoolean(32,baseVO.isLtrReceived());
                result = stmt.executeUpdate();

                workflowDao.setLtrWorkflow(workflowVO);
            }
        }catch(Exception exception){
            String errorMsg = "Error occured while saving letter data"+ exception.getMessage();
            log.error(errorMsg);
            throw new Exception(errorMsg);
        }finally {
            if (stmt != null) stmt.close();
            if (connection != null) connection.close();
        }
        return result;
    }




}
