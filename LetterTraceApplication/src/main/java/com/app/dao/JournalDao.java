package com.app.dao;

import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class JournalDao extends Dao {

    public List<HashMap<String,Object>> karyaVivaranNondWahi(String sewarthId) throws Exception {

        log.info("JournalDao :: karyaVivaranNondWahi for : " + sewarthId);

        List<HashMap<String,Object>> result = new ArrayList<>();
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            connection = getConnection();

            String sql = "SELECT w.ltrInwardNum , w.ltrpargamanNum, w.ltrOutwardNum , w.ltrSubject, w.goshwara, l.ltrSenderMarathi, " +
                    " (SELECT min(added_tz) FROM pao_main.workflow where ltrInwardNum = w.ltrInwardNum and ltrStatus = 'new' ) ltrCreatedDate,  " +
                    " (SELECT min(added_tz) FROM pao_main.workflow where ltrInwardNum = w.ltrInwardNum and ltrStatus = 'accepted' ) ltrAcceptedDate, " +
                    " (SELECT min(added_tz) FROM pao_main.workflow where ltrInwardNum = w.ltrInwardNum and ltrStatus in ('outwarded', 'completed') ) ltrCompletedDate " +
                    " FROM pao_main.workflow w , pao_main.letter l " +
                    " where w.isAssigned = 1 and w.ltrInwardNum = l.ltrInwardNum " +
                    " and w.ltrInwardNum  in (SELECT distinct ltrInwardNum  from pao_main.workflow where toUser = ? and ltrStatus = 'accepted' )";

            stmt = connection.prepareStatement(sql);
            stmt.setString(1,sewarthId);
            rs= stmt.executeQuery();
            while(rs.next()){
                HashMap<String,Object> journal = new HashMap<>();
                journal.put("ltrInwardNum", rs.getString("ltrInwardNum"));
                journal.put("ltrPargamanNum", rs.getString("ltrpargamanNum"));
                journal.put("ltrOutwardNum", rs.getString("ltrOutwardNum"));
                journal.put("ltrSubject", rs.getString("ltrSubject"));
                journal.put("goshwara", rs.getString("goshwara"));
                journal.put("ltrSenderMarathi", rs.getString("ltrSenderMarathi"));
                journal.put("ltrCreatedDate", rs.getString("ltrCreatedDate"));
                journal.put("ltrAcceptedDate", rs.getString("ltrAcceptedDate"));
                journal.put("ltrCompletedDate", rs.getString("ltrCompletedDate"));
                result.add(journal);
            }
        }catch(Exception exception){
            String errorMsg = "Error occurred while fetching karya vivaran nond wahi"+ exception.getMessage();
            log.error(errorMsg);
            throw new Exception(errorMsg);
        }finally {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
            if (connection != null) connection.close();
        }
        return result;

    }

}
