package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class internalChangeDao
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	
	@Autowired
	private JdbcTemplate template;
	private Connection con;
	int rowAffect=0;
	public int changeEmpData(String sevarthid, String empname,String branch,String changebranch)
	{
	      LOGGER.info("Enter into internalChangeDao changeEmpData()");

		try
		{
			  String  sql="insert into internalchange(sevarthid,empname,branch,subbranch)values(?,?,?,?)";
			  PreparedStatement stmt=con.prepareStatement(sql);
			  
			  stmt.setString(1,sevarthid); 
			  stmt.setString(2, empname);
			  stmt.setString(3,branch); 
			  stmt.setString(4,changebranch);
			  
			  rowAffect = stmt.executeUpdate();
			 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	     
		LOGGER.info("Exit from internalChangeDao changeEmpData()");

		return rowAffect;
	}

	public List<Map> fetchInfo()
	{
	      LOGGER.info("Enter into internalChangeDao fetchInfo()");

		List<Map> lDataList=new ArrayList<>();
		ResultSet rst=null;
		int count=1;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from changebranch";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("sevarthid",rst.getString("sevarthid"));
				letterMap.put("empname", rst.getString("empname"));
				letterMap.put("branch",rst.getString("branch"));
				letterMap.put("changebranch",rst.getString("changebranch") );
				lDataList.add(letterMap);
				
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from internalChangeDao fetchInfo()");

		return lDataList;
	}

	
}
