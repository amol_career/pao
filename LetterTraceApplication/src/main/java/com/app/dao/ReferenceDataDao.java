package com.app.dao;

import com.app.model.ReferenceData;
import com.app.model.ReferenceDataMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReferenceDataDao extends Dao {

    public List<ReferenceData> getReferenceData(String whereClause){
        log.info(getClass().getName() + " :: getReferenceData :: where " + whereClause );
        String sql="select * from reference_data ";
        if(whereClause!=null)
        {
            sql += " where " + whereClause;
        }
        return template.query(sql, new ReferenceDataMapper());
    }

    public List<ReferenceData> getReferenceDataBySql(String sql){
        log.info(getClass().getName() + " :: getReferenceDataBySql :: sql " + sql );
        return template.query(sql, new ReferenceDataMapper());
    }

}
