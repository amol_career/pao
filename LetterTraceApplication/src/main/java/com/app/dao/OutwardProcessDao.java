package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class OutwardProcessDao 
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private JdbcTemplate template;
	private Connection con;
	@SuppressWarnings("unused")
	public int storeOutwardLetterData(String subject, String lettertypeI,String lettertypeII,String sendbranch,String sendsubbranch)
	{
	      LOGGER.info("Enter into OutwardProcessDao storeOutwardLetterData()");

		int rowAffect=0;
		try {
			
			con=template.getDataSource().getConnection();
			
			String ltrTypeId="";
			
		
			String sql="insert into outwardletter(LetterSubject,LetterType1,LetterType2,Branch,SubBranch)values(?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, subject);
			stmt.setString(2, lettertypeI);
			stmt.setString(3, lettertypeII);
			stmt.setString(4,sendbranch);
			stmt.setString(5,sendsubbranch);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from OutwardProcessDao storeOutwardLetterData()");

		return rowAffect;
	}

}
