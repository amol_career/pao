package com.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.app.model.ReferenceData;
import com.app.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;
import com.app.model.Employee;

@Repository
public class AuthenticationDao extends Dao
{
	public User validateUser(String sewarthId,String password) throws  Exception {
	     log.info("AuthenticationDao :: Validating user. ");
         Connection connection = null;
		 PreparedStatement statement = null;
		 ResultSet resultSet=null;
	     boolean result = false;
		 User user = null;
		try {
			connection = getConnection();
			String sql="select id, nondani_shakha, sevarth_id, user_name, password, sub_dept_id, role_id from user where sevarth_id=? and password=?";
			statement = connection.prepareStatement(sql);
			statement.setString(1,sewarthId);
			statement.setString(2 ,password);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setSevarthId(resultSet.getString("sevarth_id"));
				user.setUserName(resultSet.getString("user_name"));
				user.setPassword(resultSet.getString("password"));
				user.setSubDepartmentId(resultSet.getInt("sub_dept_id"));
				user.setRoleId(resultSet.getInt("role_id"));
				user.setNondaniShakha(resultSet.getString("nondani_shakha"));
				log.info("AuthenticationDao :: Provided user " + sewarthId + " validate successfully.");
				break;
			}

		} catch (Exception e) {
		      String errorMsg = "Error occured while authentication user - "+sewarthId +" Error -"+e.getMessage();
		      log.error(errorMsg);
		       throw new Exception(errorMsg);
	     }finally {
			  if (resultSet != null) resultSet.close();
	          if (statement != null) statement.close();
	          if (connection != null) connection.close();
		}
		return user;
	}

	public User getUser(String sevartId) throws  Exception{
		User user = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
			if(sevartId != null){
			try{
				connection = getConnection();
				//String sql="select id, sevarth_id, user_name, password, dept_id, role_id, manager_id from user where sevarth_id=?";
				String sql="select id, nondani_shakha, sevarth_id, user_name, password, sub_dept_id, role_id from user where sevarth_id=?";

				statement = connection.prepareStatement(sql);
				statement.setString(1,sevartId);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					user = new User();
					user.setId(resultSet.getInt("id"));
					user.setSevarthId(resultSet.getString("sevarth_id"));
					user.setUserName(resultSet.getString("user_name"));
					user.setPassword(resultSet.getString("password"));
					user.setSubDepartmentId(resultSet.getInt("sub_dept_id"));
					user.setRoleId(resultSet.getInt("role_id"));
					user.setNondaniShakha(resultSet.getString("nondani_shakha"));
					log.info("AuthenticationDao :: Provided user " + sevartId + " validate successfully.");
					break;
				}
			} catch (Exception e) {
				String errorMsg = "Error occured while authentication user - "+sevartId +" Error -"+e.getMessage();
				log.error(errorMsg);
				throw new Exception(errorMsg);
			}finally {
				if (resultSet != null) resultSet.close();
				if (statement != null) statement.close();
				if (connection != null) connection.close();
			}
		}
		return  user;
	}

	public int changePass(String sewarthId,String password ) throws Exception
	{
	    log.info("AuthenticationDao :: Enter into AuthenticationDao changePass()");
		int result=0;
        Connection connection = null;
		PreparedStatement statement = null;
        try {
			connection= getConnection();
			String sql="update user set password=? where sevarth_id=?";
			statement =connection.prepareStatement(sql);
			statement.setString(1,password);
			statement.setString(2 ,sewarthId);
			result = statement.executeUpdate();
		} catch (Exception e) {
			String errorMsg = "Error occured while changing password for user - "+sewarthId +" Error -"+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
        return result;
	}
	
	public ReferenceData getReferenceDataById(int id, String entityKey) throws Exception 	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		ReferenceData referenceData = null;
	    log.info("AuthenticationDao :: getReference Data...");
		try {
			connection = getConnection();
			String sql="select id,entity_key,entity_value from reference_data where id =? and entity_key=?" ;
			statement=connection.prepareStatement(sql);
			statement.setInt(1,id);
			statement.setString(2,entityKey);

			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				referenceData =new ReferenceData();
				referenceData.setId(resultSet.getInt("id"));
				referenceData.setEntityName(resultSet.getString("entity_key"));
				referenceData.setEntityValue(resultSet.getString("entity_value"));
				break;
			}
		} catch (Exception e) {
			String errorMsg = "Error occured while getDepartment() for id - "+id + " and entity key: "+entityKey+" Error -"+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return referenceData;
	}

	public List<ReferenceData> getReferenceDataByEntityKey(String entityKey) throws Exception 	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		ReferenceData referenceData = null;
		List<ReferenceData> result = null;
		log.info("AuthenticationDao :: getReference Data...");
		try {
			connection = getConnection();
			String sql="select id, entity_key, entity_value from reference_data where entity_key=?" ;
			statement=connection.prepareStatement(sql);
			statement.setString(1,entityKey);
			result = new ArrayList<>();
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				referenceData =new ReferenceData();
				referenceData.setId(resultSet.getInt("id"));
				referenceData.setEntityName(resultSet.getString("entity_key"));
				referenceData.setEntityValue(resultSet.getString("entity_value"));
				result.add(referenceData);
			}
		} catch (Exception e) {
			String errorMsg = "Error occured while getReferenceDataByEntityKey() for entity key - "+entityKey  +" Error -"+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	public ReferenceData getReferenceDataByKeyAndValue(String entityKey, String entityValue) throws Exception 	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		ReferenceData referenceData = null;
		log.info("AuthenticationDao :: getReference Data...");
		try {
			connection = getConnection();
			String sql="select id,entity_key,entity_value from reference_data where entity_value =? and entity_key=?" ;
			statement=connection.prepareStatement(sql);
			statement.setString(1,entityValue);
			statement.setString(2,entityKey);

			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				referenceData =new ReferenceData();
				referenceData.setId(resultSet.getInt("id"));
				referenceData.setEntityName(resultSet.getString("entity_key"));
				referenceData.setEntityValue(resultSet.getString("entity_value"));
				break;
			}
		} catch (Exception e) {
			String errorMsg = "Error occured while getDepartment() for entityValue - "+entityValue + " and entity key: "+entityKey+" Error -"+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return referenceData;
	}

}
