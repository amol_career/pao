package com.app.dao;

import com.app.model.BaseVO;
import com.app.model.BaseVOMapper;
import com.app.model.ReferenceData;
import com.app.model.WorkflowVO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Clock;
import java.util.*;


public abstract class BaseDao extends Dao {

	protected abstract int storeData(BaseVO baseVO, WorkflowVO workflowVO) throws Exception;

	public List<HashMap<String,Object>> fetchLtrMediumDropdown() throws Exception{

		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="SELECT entity_value FROM `reference_data` WHERE entity_key= 'medium' ORDER BY entity_value DESC";
			statement=connection.prepareStatement(sql);
			rs=statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
  			    letterMap.put("entity_value",rs.getString("entity_value"));
				result.add(letterMap);
			}
		}catch (Exception e) {
			String errorMsg = "Error occured while fetchLtrMediumDropdown() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> fetchLtrAcceptorDropdown() throws Exception{

		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="SELECT entity_value FROM `reference_data` WHERE entity_key= 'role' ";
			// " and entity_value in ('उप.अ.व ले.अ.', 'स.ले.अ.', 'स.अ.व ले.अ.', 'मा.अ.व ले.अ.', 'सुविधा कक्ष', 'पी.पी.ओ.डेस्क.')";
			statement =connection.prepareStatement(sql);
			rs =statement.executeQuery();

			String val = "";
			while(rs.next()) {
				val = rs.getString("entity_value");
				if ("उप.अ.व ले.अ.".equalsIgnoreCase(val) ||
						"स.ले.अ.".equalsIgnoreCase(val) ||
						"स.अ.व ले.अ.".equalsIgnoreCase(val) ||
						"मा.अ.व ले.अ.".equalsIgnoreCase(val) ||
						"सुविधा कक्ष".equalsIgnoreCase(val) ||
						"पी.पी.ओ.डेस्क.".equalsIgnoreCase(val)){
					HashMap<String,Object> letterMap = new HashMap<>();
					letterMap.put("entity_value",val);
					result.add( letterMap);
				}
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchLtrAcceptorDropdown() :: BaseDao ::  Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> fetchDDOInfoDropDown() throws Exception{
		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		int count=1;
		try {
			connection = getConnection();
			String sql="select * from ddo";
			statement = connection.prepareStatement(sql);
			rs=statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("ddoNameEnglish", rs.getString("DDO_DESIGNATION_IN_ENGLISH"));
				letterMap.put("ddoMarathiName",rs.getString("DDO_DESIGNATION_IN_MARATHI"));
				letterMap.put("ddoNumber",rs.getString("DDO_NUMBER"));
				letterMap.put("ddobranch",rs.getString("SECTION") );
				result.add(letterMap);
			}
		}catch (Exception e) {
			String errorMsg = "Error occured while fetchDDOInfoDropDown() :: BaseDao ::  Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	private void populateDDOInterlinkingInfo(HashMap<String,Object> input, ResultSet rs, String condition) throws Exception{
		if(condition == "ENGLISH"){
			input.put("ddoMarathiName", rs.getString("DDO_DESIGNATION_IN_MARATHI"));
			input.put("ddoNumber",rs.getString("DDO_NUMBER"));
		} else if(condition == "MARATHI"){
			input.put("ddoNameEnglish", rs.getString("DDO_DESIGNATION_IN_ENGLISH"));
			input.put("ddoNumber",rs.getString("DDO_NUMBER"));
		} else{
			input.put("ddoMarathiName", rs.getString("DDO_DESIGNATION_IN_MARATHI"));
			input.put("ddoNameEnglish", rs.getString("DDO_DESIGNATION_IN_ENGLISH"));
		}
	}


	public List<HashMap<String,Object>> fetchDDOSelectBranches(String ddomarathiId, String typeOfSelection)  throws Exception{
		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="select * from ddo where id="+ ddomarathiId+"";
			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("ddoId", rs.getString("id"));
				populateDDOInterlinkingInfo(letterMap,rs,typeOfSelection);
				letterMap.put("ddobranch",rs.getString("SECTION") );
				result.add(letterMap);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchDDOSelectBranches() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> fetchLtrMainBranchDropdown() throws Exception{
		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="SELECT entity_value FROM `reference_data` WHERE entity_key= 'department' ORDER BY entity_value DESC";
			statement=connection.prepareStatement(sql);
			rs=statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("entity_value",rs.getString("entity_value"));
				result.add( letterMap);
			}
		}catch (Exception e) {
			String errorMsg = "Error occured while fetchLtrMainBranchDropdown() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}

		return result;
	}

	public List<HashMap<String,Object>> fetchltrSubject(String subDept) throws  Exception {

		List<HashMap<String,Object>> letterSubjectList=new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		PreparedStatement statement1 = null;
		ResultSet rs1 = null;
		try {
			connection = getConnection();

			String sql="SELECT id FROM reference_data WHERE entity_key = 'sub_department' and entity_value= '"+subDept+"'";
			Map<String,Object> subBranchesId=new LinkedHashMap<>();

			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			int subDeptId = 0 ;
			if(rs.next()){
				subDeptId = rs.getInt("id");
			}

			String sql1="SELECT subject FROM `letter_subject`"
					+ " WHERE dept_id="+subDeptId+"";

			statement1 = connection.prepareStatement(sql1);
			rs1=statement1.executeQuery();
			while(rs1.next()) {
				HashMap<String,Object> subBranchesNames = new HashMap<>();
				subBranchesNames.put("subject",rs1.getString("subject") );
				letterSubjectList.add(subBranchesNames);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchltrSubject() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (rs1 != null) rs1.close();
			if (statement1 != null) statement1.close();
			if (connection != null) connection.close();
		}

		return letterSubjectList;
	}

	public List<HashMap<String,Object>> fetchSubBranches(String mainDeptValue) throws Exception {
		log.info("Entering into BaseDao::fetchSubBranches()");
		List<HashMap<String,Object>> subBranchListVal=new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();

			log.info("mainDeptValue ::" + mainDeptValue);

			String sql="SELECT b.entity_value FROM pao_main.reference_data a , pao_main.mapping m , pao_main.reference_data b " +
					" WHERE a.id = m.parent and m.child = b.id and a.entity_value = '"+ mainDeptValue +"'";

			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> subBranchesNames = new HashMap<>();
				subBranchesNames.put("entity_value",rs.getString("entity_value") );
				log.info("IN side RS: subBranchesNames ::" + subBranchesNames.get("entity_value"));

				subBranchListVal.add(subBranchesNames);
			}

			log.info("OUT side RS: subBranchListVal ::" + subBranchListVal);

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchSubBranches() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return subBranchListVal;
	}
	public List<ReferenceData> fetchSubBranchesFromMain(String mainDeptValue) throws Exception {
		log.info("Entring into BaseDao::fetchSubBranchesFromMain()");
		List<ReferenceData> subBranchListVal=new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		PreparedStatement statement1 = null;
		ResultSet rs1 = null;
		try {
			connection = getConnection();

			String sql="SELECT id FROM reference_data WHERE entity_value= '"+mainDeptValue+"'";
			Map<String,Object> branchesNames=new LinkedHashMap<>();
			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			int id = 0;
			if(rs.next()){
				id = rs.getInt("id");
			}

			String sql1="SELECT id,entity_value FROM reference_data"
					+ " WHERE id IN (SELECT child from mapping WHERE parent="+id+")";

			statement1 = connection.prepareStatement(sql1);
			rs1 = statement1.executeQuery();
			while(rs1.next()) {
				ReferenceData referenceData=new ReferenceData();
				//HashMap<Integer,Object> subBranchesNames = new HashMap<>();
				//subBranchesNames.put(rs1.getInt("id"),rs1.getString("entity_value") );
				referenceData.setId(rs1.getInt("id"));
				referenceData.setEntityValue(rs1.getString("entity_value"));
				subBranchListVal.add(referenceData);
			}
		}catch (Exception e) {
			String errorMsg = "Error occured while fetchSubBranches() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (rs1 != null) rs1.close();
			if (statement1 != null) statement1.close();
			if (connection != null) connection.close();
		}
		log.info("Exit from BaseDao::fetchSubBranchesFromMain()");
		return subBranchListVal;
	}

	public List<HashMap<String,Object>> fetchLtrSubBranchDropdownforSarvShakha() throws Exception {
		List<HashMap<String,Object>> result=new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			connection = getConnection();
			String sql="SELECT entity_value FROM reference_data WHERE entity_key= 'sub_department'";

			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("entity_value",rs.getString("entity_value"));
				result.add( letterMap);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchLtrSubBranchDropdownforSarvShakha() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}



	public Map<String,Integer> getIndividualAndAllLetterCount(int letterTypeId, String nondaniShakha) throws Exception{
		Map<String,Integer> result = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		PreparedStatement statement1 = null;
		ResultSet rs1 = null;
		try {
			result = new HashMap<>();
			connection = getConnection();
			String sql="select max(ltrId) as count from letter where ltrType=? ";
			String sql1="select count(*) as count from letter";
			if(nondaniShakha != null){
				sql = sql + " and nondani_shakha = '"+nondaniShakha+"'";
				sql1 = sql1 + " where nondani_shakha = '"+nondaniShakha+"'";
			}
			log.info("Sql prepared for individual count :: => "+ sql);
			statement=connection.prepareStatement(sql);
			statement.setInt(1,letterTypeId);
			rs=statement.executeQuery();
			if(rs.next()){
				result.put("IndividualLtrCount",rs.getInt("count"));
			}
			log.info("Sql prepared for all count :: => "+ sql1);
			statement1=connection.prepareStatement(sql1);
			rs1=statement1.executeQuery();
			if(rs1.next()){
				int ltrCount = rs1.getInt("count");
				result.put("AllLetterCount", ltrCount);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while getIndividualAndAllLetterCount() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (rs1 != null) rs1.close();
			if (statement1 != null) statement1.close();
			if (connection != null) connection.close();
		}

		return result;
	}

    private String getRollMapping(String role){
		Map<String,String> result = new HashMap<>();
		result.put("उप.अ.व ले.अ.","dypao");
		result.put("स.ले.अ.","so");
		result.put("स.अ.व ले.अ.","apao");
		result.put("मा.अ.व ले.अ.","pao");
		return result.get(role);
	}

	public String getSewartIdForWorkflowNo(int subDeptId, String roleName) throws Exception {
		String result = "Dummy_ID";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql = "SELECT distinct "+getRollMapping(roleName)+ " as sewarthId FROM user_hierarchy where sub_dept_id= "+ subDeptId;
			statement=connection.prepareStatement(sql);
			rs=statement.executeQuery();
			while(rs.next()) {
				result = rs.getString("sewarthId");
				break;
			}
		}catch (Exception e) {
			String errorMsg = "Error occured while getSewartIdForWorkflowNo() :: BaseDao :: Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}

		return result;
	}
	public List<HashMap<String,Object>> fetchBankNameDropdown() throws Exception{

		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="SELECT DISTINCT bank_name FROM pao_main.bank;";
			statement =connection.prepareStatement(sql);
			rs =statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("bank_name",rs.getString("bank_name"));
				result.add( letterMap);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchBankNameDropdown() :: BaseDao ::  Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}
	
	public List<HashMap<String,Object>> fetchBankBranchNameDropdown(String bankName) throws Exception{

		List<HashMap<String,Object>> result = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			String sql="SELECT DISTINCT branch_name FROM pao_main.bank where bank_name= '"+bankName+"'";
			statement =connection.prepareStatement(sql);
			rs =statement.executeQuery();
			while(rs.next()) {
				HashMap<String,Object> letterMap = new HashMap<>();
				letterMap.put("branch_name",rs.getString("branch_name"));
				result.add( letterMap);
			}

		}catch (Exception e) {
			String errorMsg = "Error occured while fetchBankBranchNameDropdown() :: BaseDao ::  Error - "+e.getMessage();
			log.error(errorMsg);
			throw new Exception(errorMsg);
		}finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (connection != null) connection.close();
		}
		return result;
	}

	public List<BaseVO> getLetterDispalyList(String whereClause)
	{
		log.info("BaseDao ::getLetterDispalyList()");
		//whereClause = "SELECT entity_value FROM pao_main.reference_data where id in (select ltrType from letter)";

		String sql="select * from letter ";
		// SELECT * from `pao_main`.`letter `WHERE STR_TO_DATE(`date`, '%d-%m-%Y') BETWEEN
		//  STR_TO_DATE('12-5-2005', '%d-%m-%Y') AND STR_TO_DATE('20-6-2005', '%d-%m-%Y');
		if(whereClause!=null)
		{
			sql+=" where "+whereClause;
		}
		return template.query(sql,new BaseVOMapper());
	}

}