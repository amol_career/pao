package com.app.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
//import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.controller.LoginController;

@Repository
public class FordProcessDao 
{
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);


	@Autowired
	private JdbcTemplate template;
	private Connection con;
	
	
	public int storeLetterOneDataFord(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String toDept,String ddoNum,String ddoMrt,String ddoEng,String DdoSection)
	//String inwardNum
	{
	      LOGGER.info("Enter into FordProcessDao storeLetterOneDataFord()");

		
		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypeoneford(LtrMedium,LtrMediumRef,LtrTypesId,DdoNameMarathi,DdoNameEnglish,DdoNumber,DdoSection,LtrOutNum,LtrDate,LtrSubject,Supplement,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ddoMrt);
			stmt.setString(5,ddoEng);
			stmt.setString(6,ddoNum);
			stmt.setString(7,DdoSection);
			stmt.setString(8,ltrOutNo);
			stmt.setDate(9,ltrDate);
			stmt.setString(10,ltrSubject);
			stmt.setBoolean(11, supplement);
			stmt.setString(12,toDept);
			stmt.setString(13,salgnaShakha);
			stmt.setString(14,remark);
			stmt.setString(15,ltrAcceptor);
			//stmt.setString(16,inwardNum);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao storeLetterOneDataFord()");

		return rowAffect;
	}
	public int storeLetterTwoDataFord(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String chkNumber,Date chkDate,String amount,String inwardStr)
	{
	      LOGGER.info("Enter into FordProcessDao storeLetterTwoDataFord()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypetwoford(LtrMedium,LtrMediumRef,LtrTypesId,LtrOutNum,LtrDate,LtrSubject,Supplement,CheckNumber,CheckDate,amount,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor,InwardNum)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrOutNo);
			stmt.setDate(5,ltrDate);
			stmt.setString(6,ltrSubject);
			stmt.setBoolean(7, supplement);
			stmt.setString(8,chkNumber);	
			stmt.setDate(9,chkDate);
			stmt.setFloat(10,Float.parseFloat(amount));
			stmt.setString(11,fromDept);
			stmt.setString(12,salgnaShakha);
			stmt.setString(13,remark);
			stmt.setString(14,ltrAcceptor);
			stmt.setString(15,inwardStr);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao storeLetterTwoDataFord()");
		return rowAffect;
	}
	public int storeLetterThreeDataFord(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String chkNumber,Date chkDate,String bankName,String inwardStr)
	{
	      LOGGER.info("Enter into FordProcessDao storeLetterTwoDataFord()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypethreeford(LtrMedium,LtrMediumRef,LtrTypesId,LtrOutNum,LtrDate,LtrSubject,Supplement,CheckNumber,CheckDate,BankName,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor,InwardNum)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrOutNo);
			stmt.setDate(5,ltrDate);
			stmt.setString(6,ltrSubject);
			stmt.setBoolean(7, supplement);
			stmt.setString(8,chkNumber);	
			stmt.setDate(9,chkDate);
			stmt.setString(10,bankName);
			stmt.setString(11,fromDept);
			stmt.setString(12,salgnaShakha);
			stmt.setString(13,remark);
			stmt.setString(14,ltrAcceptor);
			stmt.setString(15,inwardStr);
			
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao storeLetterTwoDataFord()");
	 return rowAffect;
	}
	public int storeLetterFiveDataFord(String ltrMedium,String manualLtrMedium,String ltrTypesId,String ltrOutNo,
			Date ltrDate,String ltrSubject,boolean supplement,String salgnaShakha,String ltrAcceptor,String remark,
			String fromDept,String mobile,String ltrFrom)
	{
	      LOGGER.info("Enter into FordProcessDao storeLetterFiveDataFord()");

		int rowAffect=0;
		try {
			con=template.getDataSource().getConnection();
			String ltrTypeId="";
			String sqlStr="select LtrTypeId from lettertypes where LetterType=?";
			PreparedStatement stmtone=con.prepareStatement(sqlStr);
			stmtone.setString(1, ltrTypesId);
			ResultSet rs=stmtone.executeQuery();
			while(rs.next())
				ltrTypeId=rs.getString(1);
			
			String sql="insert into lettertypefiveford(LtrMedium,LtrMediumRef,LtrTypesId,LtrFrom,mobile,LtrOutNum,LtrDate,LtrSubject,Supplement,LtrFromDept,SalgnaShakha,Remark,LtrAcceptor)values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt=con.prepareStatement(sql);
			
			stmt.setString(1, ltrMedium);
			stmt.setString(2, manualLtrMedium);
			stmt.setString(3, ltrTypeId);
			stmt.setString(4,ltrFrom);
			stmt.setString(5,mobile);
			stmt.setString(6,ltrOutNo);
			stmt.setDate(7,ltrDate);
			stmt.setString(8,ltrSubject);
			stmt.setBoolean(9, supplement);
			stmt.setString(10,fromDept);
			stmt.setString(11,salgnaShakha);
			stmt.setString(12,remark);
			stmt.setString(13,ltrAcceptor);
			rowAffect=stmt.executeUpdate();
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao storeLetterFiveDataFord()");

		return rowAffect;
	}
	/*
    stmt.setString(3, ltrLocation);
	stmt.setString(8,toDept);
	stmt.setString(10,nagpurLtr);
	stmt.setString(11,subLetter);
	stmt.setString(13,checkNumber);
	stmt.setString(14,bankName);
	stmt.setDate(15,checkDate);
	stmt.setString(16,amount);
	stmt.setString(17,manualLtrMedium);
	stmt.setString(18,mumbaiType);
	stmt.setString(19,subMumbai);
	stmt.setString(20,mumbaiGpf);
	stmt.setString(21,gpfSubSelect);
	stmt.setString(22,raoselect);
	stmt.setString(26,phNum);
	*/
	
	
	@SuppressWarnings("rawtypes")
	public List<LinkedHashMap> fetchAlldataFord()
	{
	      LOGGER.info("Enter into FordProcessDao fetchAlldataFord()");

		List<LinkedHashMap> lDataList=new ArrayList<>();
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from lettertypeoneford";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				LinkedHashMap<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("LtrTypeOnefordId",rst.getString("LtrTypeOnefordId"));
				letterMap.put("InwardNum",rst.getString("InwardNum"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("DdoNameMarathi",rst.getString("DdoNameMarathi"));
				letterMap.put("DdoNameEnglish",rst.getString("DdoNameEnglish"));
				letterMap.put("DdoNumber",rst.getString("DdoNumber"));
				letterMap.put("DdoSection",rst.getString("DdoSection"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));	
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				lDataList.add(letterMap);
				
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchAlldataFord()");

		return lDataList;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> fetchEmpdataFord(String deptId)
	{
	      LOGGER.info("Enter into FordProcessDao fetchEmpdataFord()");

		@SuppressWarnings("rawtypes")
		List<String> emplist=new ArrayList();
		String deptName=null;
		ResultSet rst=null;
		String empName=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select deptName from dept where dId=?";
			PreparedStatement stmt=con.prepareStatement(sql);
			stmt.setString(1,deptId);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				deptName=rst.getString("deptName");
			}
			System.out.println("DeptName::"+deptName);
		    
		LOGGER.info("DeptName::"+deptName);

			String sqltext="select EMPLOYEE_NAME from " + deptName + "";
			PreparedStatement stmtone=con.prepareStatement(sqltext);
			rst=stmtone.executeQuery();
			rst.next();
			while(rst.next())
			{
				empName=rst.getString("EMPLOYEE_NAME");
				emplist.add(empName);
			}
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchEmpdataFord()");

		return emplist;
	}
	
	public int getTotalLtrCountFord()
	{
	      LOGGER.info("Enter into FordProcessDao getTotalLtrCountFord()");

		int count=0;
		String countstr=null;
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select sum(subcount) as total_count from ( (select count(*) as subcount from lettertypeoneford)as T union (select count(*) as subcount from lettertypetwoford)as T2)";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				countstr=rst.getString("total_count");
				System.out.println("count from Dao::"+countstr);
			}
			if(con!=null);
			{
				con.close();
			}
			return count;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	LOGGER.info("Exit from FordProcessDao getTotalLtrCountFord()");

		return count;
	}
	public int getLtrCountFord(String ltrType)
	{
	      LOGGER.info("Enter into FordProcessDao getLtrCountFord()");

		String countstr=null;
		int cnt=0;
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select count(*) as ltr_count from "+ltrType+"";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				countstr=rst.getString("ltr_count");
				System.out.println("ltrCount::"+countstr);
			}
			cnt=Integer.parseInt(countstr);
			return cnt;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao getLtrCountFord()");

		return cnt;
	}
	@SuppressWarnings("rawtypes")
	public List<Map> fetchddoListFord()
	{
	      LOGGER.info("Enter into FordProcessDao fetchddoListFord()");

		List<Map> lddoMarathiList=new ArrayList<>();
		ResultSet rst=null;
		int count=1;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from ddo";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("sr_no",count++);
				letterMap.put("ddoMarathiName",rst.getString("DDO_DESIGNATION_IN_MARATHI"));
				letterMap.put("ddoNameEnglish", rst.getString("DDO_DESIGNATION_IN_ENGLISH"));
				letterMap.put("ddoNumber",rst.getString("DDO_NUMBER"));
				letterMap.put("ddobranch",rst.getString("SECTION") );
				lddoMarathiList.add(letterMap);
				
			}
			//System.out.println("MarathiList:"+lddoMarathiList);
			if(con!=null);
			{
				con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchddoListFord()");

		return lddoMarathiList;
	}
	@SuppressWarnings("rawtypes")
	public List<Map> fetchltrdatatwoFord()
	{
	      LOGGER.info("Enter into FordProcessDao fetchltrdatatwoFord()");

		List<Map> lDataListTwo=new ArrayList<>();
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from lettertypetwoford";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("LtrTypeTwofordId",rst.getString("LtrTypeTwofordId"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				letterMap.put("CheckNumber",rst.getString("CheckNumber"));
				letterMap.put("CheckDate",rst.getString("CheckDate"));
				letterMap.put("amount",rst.getString("amount"));
			
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				lDataListTwo.add(letterMap);
			
			}
			//System.out.println("lDhanadeshDataList::"+lDhanadeshDataList);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchltrdatatwoFord()");

		return lDataListTwo;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Map> fetchltrdataThreeFord()
	{
	      LOGGER.info("Enter into FordProcessDao fetchltrdataThreeFord()");

		List<Map> lDataListThree=new ArrayList<>();
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from lettertypethreeford";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("LtrTypeThreefordId",rst.getString("LtrTypeThreefordId"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				letterMap.put("CheckNumber",rst.getString("CheckNumber"));
				letterMap.put("CheckDate",rst.getString("CheckDate"));
				letterMap.put("amount",rst.getString("amount"));
				letterMap.put("BankName",rst.getString("BankName"));
			
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				lDataListThree.add(letterMap);
			
			}
			//System.out.println("lDataList::"+lDataListThree);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchltrdataThreeFord()");

		return lDataListThree;
	}
	
	@SuppressWarnings("rawtypes")
	public List<Map> fetchltrdataFourFord()
	{
	      LOGGER.info("Enter into FordProcessDao fetchltrdataFourFord()");

		List<Map> lDataListFour=new ArrayList<>();
		ResultSet rst=null;
		try {
			con=template.getDataSource().getConnection();
			String sql="select * from lettertypefourford";
			PreparedStatement stmt=con.prepareStatement(sql);
			rst=stmt.executeQuery();
			while(rst.next())
			{
				Map<String,Object> letterMap=new LinkedHashMap<>();
				letterMap.put("LtrTypeFourId",rst.getString("LtrTypeFourId"));
				letterMap.put("LtrMedium", rst.getString("LtrMedium"));
				letterMap.put("LtrMediumRef",rst.getString("LtrMediumRef"));
				letterMap.put("LtrTypesId",rst.getString("LtrTypesId"));
				letterMap.put("LtrOutNum",rst.getString("LtrOutNum"));
				letterMap.put("LtrDate", rst.getDate("LtrDate"));
				letterMap.put("LtrSubject",rst.getString("LtrSubject"));
				letterMap.put("Supplement",rst.getBoolean("Supplement"));
				
				letterMap.put("RAO",rst.getString("RAO"));
				letterMap.put("LtrFromDept",rst.getString("LtrFromDept"));
				letterMap.put("SalgnaShakha",rst.getString("SalgnaShakha"));
				letterMap.put("Remark",rst.getString("Remark"));
				letterMap.put("LtrAcceptor",rst.getString("LtrAcceptor"));
				lDataListFour.add(letterMap);
			
			}
			//System.out.println("lDataList::"+lDataListThree);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      LOGGER.info("Exit from FordProcessDao fetchltrdataFourFord()");

		return lDataListFour;
	}
}
