package com.app; 

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LetterTraceAppliactionApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LetterTraceAppliactionApplication.class, args);
		
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LetterTraceAppliactionApplication.class);
	}
	
}
