package com.app.model;

public class DDO {
    private int id;
    private String ddo_designation_in_english;
    private String section;
    private String ddo_designation_in_marathi;
    private String ddo_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDdo_designation_in_english() {
        return ddo_designation_in_english;
    }

    public void setDdo_designation_in_english(String ddo_designation_in_english) {
        this.ddo_designation_in_english = ddo_designation_in_english;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDdo_designation_in_marathi() {
        return ddo_designation_in_marathi;
    }

    public void setDdo_designation_in_marathi(String ddo_designation_in_marathi) {
        this.ddo_designation_in_marathi = ddo_designation_in_marathi;
    }

    public String getDdo_number() {
        return ddo_number;
    }

    public void setDdo_number(String ddo_number) {
        this.ddo_number = ddo_number;
    }

    @Override
    public String toString() {
        return "DDO{" +
                "id=" + id +
                ", ddo_designation_in_english='" + ddo_designation_in_english + '\'' +
                ", section='" + section + '\'' +
                ", ddo_designation_in_marathi='" + ddo_designation_in_marathi + '\'' +
                ", ddo_number='" + ddo_number + '\'' +
                '}';
    }
}
