package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseVOMapper implements RowMapper<BaseVO> {


    @Override
    public BaseVO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BaseVO baseVO=new BaseVO();
        baseVO.setId(rs.getInt("id"));
        baseVO.setLtrInwardNum(rs.getString("ltrInwardNum"));
        baseVO.setLtrSubject(rs.getString("ltrSubject"));
        baseVO.setLtrDate(rs.getDate("ltrDate"));
        baseVO.setLtrSenderMarathi(rs.getString("ltrSenderMarathi"));
        baseVO.setLtrSenderEnglish(rs.getString("ltrSenderEnglish"));
        baseVO.setLtrMedium(rs.getString("ltrMedium"));
        baseVO.setLtrMediumRef(rs.getString("ltrMediumRef"));
        baseVO.setLtrToSubDept(rs.getInt("ltrToSubDept"));
        baseVO.setLtrToDept(rs.getString("ltrToDept"));
        baseVO.setLtrSupplement(rs.getBoolean("ltrSupplement"));
        baseVO.setLtrId(rs.getInt("ltrId"));
        baseVO.setLtrType(rs.getInt("ltrType"));
        baseVO.setLtrRemark(rs.getString("ltrRemark"));
        baseVO.setLtrOther(rs.getBoolean("ltrOther"));
        baseVO.setBankName(rs.getString("bankName"));
        baseVO.setChkAmount(rs.getInt("chkAmount"));
        baseVO.setChkDate(rs.getDate("chkDate"));
        baseVO.setChkNumber(rs.getString("chkNumber"));
        baseVO.setLtrMobileNo(rs.getString("ltrMobileNo"));
        baseVO.setLtrOutwardNum(rs.getString("ltrOutwardNum"));
        baseVO.setLtrSandarbhiyaShakha(rs.getString("ltrSandarbhiyaShakha"));
        baseVO.setRaoLtrs(rs.getBoolean("raoLtrs"));
        baseVO.setRaoDCBills(rs.getBoolean("raoDCBills"));
        baseVO.setRaoOBARegisters(rs.getBoolean("raoOBARegisters"));
        baseVO.setNondaniShakha(rs.getString("nondani_shakha"));
        baseVO.setLtrDdoNumber(rs.getString("ltrDdoNumber"));
        baseVO.setAdded_by(rs.getString("added_by"));
        baseVO.setAdded_tz(rs.getTimestamp("added_tz"));
        baseVO.setUpdate_by(rs.getString("update_by"));
        baseVO.setUpdated_tz(rs.getTimestamp("updated_tz"));


        return baseVO;
    }
}
