package com.app.model;

import java.io.Serializable;

public class ThirdAdminData implements Serializable{

	private String Inward;
	private String Subject;
	private String TransDate;
	private String Wibhagni;
	private String Emp;
	
	
	public ThirdAdminData(String inward, String subject, String transDate, String wibhagni, String emp) {
		//super();
		Inward = inward;
		Subject = subject;
		TransDate = transDate;
		Wibhagni = wibhagni;
		Emp = emp;
	}

	public String getInward() {
		return Inward;
	}

	public void setInward(String inward) {
		Inward = inward;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getTransDate() {
		return TransDate;
	}

	public void setTransDate(String transDate) {
		TransDate = transDate;
	}

	public String getWibhagni() {
		return Wibhagni;
	}

	public void setWibhagni(String wibhagni) {
		Wibhagni = wibhagni;
	}

	public String getEmp() {
		return Emp;
	}
	public void setEmp(String emp) {
		Emp = emp;
	}
	
	@Override
	public String toString() {
		return "ThirdAdminData [Inward=" + Inward + ", Subject=" + Subject + ", TransDate=" + TransDate + ", Wibhagni="
				+ Wibhagni + ", Emp=" + Emp + "]";
	}
	
	
}
