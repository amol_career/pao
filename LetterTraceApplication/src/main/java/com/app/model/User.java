package com.app.model;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class User {
  	private int id;
  	private String nondaniShakha;
	private String sevarthId;
	private String userName;
	private String password;
	private int subDepartmentId;
	private int roleId;
	private String addedBy;
	private Timestamp addedTz;
	private String updatedBy;
	private Timestamp updatedTz;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNondaniShakha() {
		return nondaniShakha;
	}

	public void setNondaniShakha(String nondaniShakha) {
		this.nondaniShakha = nondaniShakha;
	}

	public String getSevarthId() {
		return sevarthId;
	}

	public void setSevarthId(String sevarthId) {
		this.sevarthId = sevarthId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getSubDepartmentId() {
		return subDepartmentId;
	}

	public void setSubDepartmentId(int subDepartmentId) {
		this.subDepartmentId = subDepartmentId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public Timestamp getAddedTz() {
		return addedTz;
	}

	public void setAddedTz(Timestamp addedTz) {
		this.addedTz = addedTz;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedTz() {
		return updatedTz;
	}

	public void setUpdatedTz(Timestamp updatedTz) {
		this.updatedTz = updatedTz;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return id == user.id &&
				subDepartmentId == user.subDepartmentId &&
				roleId == user.roleId &&
				nondaniShakha.equals(user.nondaniShakha) &&
				sevarthId.equals(user.sevarthId) &&
				userName.equals(user.userName) &&
				password.equals(user.password) &&
				addedBy.equals(user.addedBy) &&
				addedTz.equals(user.addedTz) &&
				updatedBy.equals(user.updatedBy) &&
				updatedTz.equals(user.updatedTz);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nondaniShakha, sevarthId, userName, password, subDepartmentId, roleId, addedBy, addedTz, updatedBy, updatedTz);
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", nondaniShakha='" + nondaniShakha + '\'' +
				", sevarthId='" + sevarthId + '\'' +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", subDepartmentId=" + subDepartmentId +
				", roleId=" + roleId +
				", addedBy='" + addedBy + '\'' +
				", addedTz=" + addedTz +
				", updatedBy='" + updatedBy + '\'' +
				", updatedTz=" + updatedTz +
				'}';
	}

    public Map<String, Object> toMap() {
        Map<String, Object> value = new HashMap<>();
        value.put("nondaniShakha", nondaniShakha);
        value.put("sevarthId", sevarthId);
        value.put("userName", userName);
        value.put("password", password);
        value.put("subDepartmentId", subDepartmentId);
        value.put("roleId", roleId);
        value.put("addedBy", addedBy);
        value.put("addedTz", addedTz);
        value.put("updatedBy", updatedBy);
        value.put("updatedTz", updatedTz);
        return value;
    }
}
