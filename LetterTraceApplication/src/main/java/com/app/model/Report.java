package com.app.model;

import java.util.List;

public class Report {
    List<StatsReport> statsReportList;
    List<LetterReport> letterReportList;

    public List<StatsReport> getStatsReportList() {
        return statsReportList;
    }

    public void setStatsReportList(List<StatsReport> statsReportList) {
        this.statsReportList = statsReportList;
    }

    public List<LetterReport> getLetterReportList() {
        return letterReportList;
    }

    public void setLetterReportList(List<LetterReport> letterReportList) {
        this.letterReportList = letterReportList;
    }

    @Override
    public String toString() {
        return "Report{" +
                "statsReportList=" + statsReportList +
                ", letterReportList=" + letterReportList +
                '}';
    }
}
