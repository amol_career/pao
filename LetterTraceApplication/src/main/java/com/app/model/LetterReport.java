package com.app.model;

import java.util.Objects;

public class LetterReport {
    private String ltrType;
    private int ltrId;
    private int total;
    private int inProgressLetters;
    private int completedLetters;
    private int rejectedLetters;
    private int newLetters;

    public String getLtrType() {
        return ltrType;
    }

    public void setLtrType(String ltrType) {
        this.ltrType = ltrType;
    }

    public int getLtrId() {
        return ltrId;
    }

    public void setLtrId(int ltrId) {
        this.ltrId = ltrId;
    }

    public int getTotal() {
        total = inProgressLetters + completedLetters + rejectedLetters + newLetters ;
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getInProgressLetters() {
        return inProgressLetters;
    }

    public void setInProgressLetters(int inProgressLetters) {
        this.inProgressLetters = inProgressLetters;
    }

    public int getCompletedLetters() {
        return completedLetters;
    }

    public void setCompletedLetters(int completedLetters) {
        this.completedLetters = completedLetters;
    }

    public int getRejectedLetters() {
        return rejectedLetters;
    }

    public void setRejectedLetters(int rejectedLetters) {
        this.rejectedLetters = rejectedLetters;
    }

    public int getNewLetters() {
        return newLetters;
    }

    public void setNewLetters(int newLetters) {
        this.newLetters = newLetters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LetterReport that = (LetterReport) o;
        return ltrType.equals(that.ltrType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ltrType);
    }

    @Override
    public String toString() {
        return "LetterReport{" +
                "ltrType='" + ltrType + '\'' +
                ", ltrId=" + ltrId +
                ", total=" + total +
                ", inProgressLetters=" + inProgressLetters +
                ", completedLetters=" + completedLetters +
                ", rejectedLetters=" + rejectedLetters +
                ", newLetters=" + newLetters +
                '}';
    }
}
