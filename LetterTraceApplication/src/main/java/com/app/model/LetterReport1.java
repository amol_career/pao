package com.app.model;

import java.util.Objects;

public class LetterReport1 {
    private String ltrType;
    private int ltrId;
    private String inwardNumber;
    private String subject;
    private String ltrDate;

    public String getLtrType() {
        return ltrType;
    }

    public void setLtrType(String ltrType) {
        this.ltrType = ltrType;
    }

    public int getLtrId() {
        return ltrId;
    }

    public void setLtrId(int ltrId) {
        this.ltrId = ltrId;
    }

    public String getInwardNumber() {
        return inwardNumber;
    }

    public void setInwardNumber(String inwardNumber) {
        this.inwardNumber = inwardNumber;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLtrDate() {
        return ltrDate;
    }

    public void setLtrDate(String ltrDate) {
        this.ltrDate = ltrDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LetterReport1 that = (LetterReport1) o;
        return ltrId == that.ltrId &&
                ltrType.equals(that.ltrType) &&
                inwardNumber.equals(that.inwardNumber) &&
                subject.equals(that.subject) &&
                ltrDate.equals(that.ltrDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ltrType, ltrId, inwardNumber, subject, ltrDate);
    }

    @Override
    public String toString() {
        return "LetterReport{" +
                "ltrType='" + ltrType + '\'' +
                ", ltrId=" + ltrId +
                ", inwardNumber='" + inwardNumber + '\'' +
                ", subject='" + subject + '\'' +
                ", ltrDate='" + ltrDate + '\'' +
                '}';
    }
}
