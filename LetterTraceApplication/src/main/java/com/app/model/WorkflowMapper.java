package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkflowMapper implements RowMapper<WorkflowVO> {

    @Override
    public WorkflowVO mapRow(ResultSet rs, int rowNum) throws SQLException {
        WorkflowVO workflowvo=new WorkflowVO();
        workflowvo.setId(rs.getInt("id"));
        workflowvo.setLtrInwardNum(rs.getString("ltrInwardNum"));
        workflowvo.setLtrOutwardNum(rs.getString("ltrOutwardNum"));
        workflowvo.setLtrPargamanNum(rs.getString("ltrPargamanNum"));
        workflowvo.setFromUser(rs.getString("fromUser"));
        workflowvo.setToUser(rs.getString("toUser"));
        workflowvo.setLtrStatus(rs.getString("ltrStatus"));
        workflowvo.setLtrSubject(rs.getString("ltrSubject"));
        workflowvo.setLtrDate(rs.getDate("ltrDate"));
        workflowvo.setLtrType(rs.getInt("ltrType"));
        workflowvo.setLtrCategory(rs.getString("ltrCategory"));
        workflowvo.setAssigned(rs.getBoolean("isAssigned"));
        workflowvo.setDeptId(rs.getInt("dept_id"));
        workflowvo.setAdded_by(rs.getString("added_by"));
        workflowvo.setUpdate_by(rs.getString("update_by"));
        workflowvo.setAdded_tz(rs.getTimestamp("added_tz"));
        workflowvo.setUpdated_tz(rs.getTimestamp("updated_tz"));
        workflowvo.setGoshwara(rs.getString("goshwara"));
        workflowvo.setRoute_outward(rs.getString("route_outward"));
        workflowvo.setRoute_paragaman(rs.getString("route_paragaman"));
        workflowvo.setRoute_nondani_shakha(rs.getInt("route_nondani_shakha"));

        return workflowvo;
    }
}
