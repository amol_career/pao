package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReferenceDataMapper  implements RowMapper<ReferenceData>
{
    @Override
    public ReferenceData mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReferenceData referenceData = new ReferenceData();
        referenceData.setId(rs.getInt("id"));
        referenceData.setEntityName(rs.getString("entity_key"));
        referenceData.setEntityValue(rs.getString("entity_value"));

        return referenceData;
    }
}
