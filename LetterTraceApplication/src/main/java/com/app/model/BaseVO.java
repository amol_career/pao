package com.app.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

public class BaseVO {
    private int id;
    private String nondaniShakha;
    private int ltrId;
    private int ltrType;
    private String ltrSubject;
    private Date ltrDate;
    private String ltrMedium;
    private String ltrRemark;
    private String ltrToDept;
    private String ltrMediumRef;
    private String ltrSenderEnglish;
    private String ltrSenderMarathi;
    private String ltrDdoNumber;
    private String ltrSandarbhiyaShakha;
    private boolean ltrSupplement;
    private int ltrToSubDept;
    private String ltrOutwardNum;
    private String ltrInwardNum;
    private boolean ltrOther;
    private String ltrMobileNo;
    private String added_by;
    private Timestamp added_tz;
    private Timestamp updated_tz;
    private String update_by;
    private String chkNumber;
    private double chkAmount;
    private Date chkDate;
    private String bankName;
    private boolean raoLtrs;
    private boolean raoDCBills;
    private boolean raoOBARegisters;
    private String ltrAcceptor;
    private boolean ltrReceived;
    private boolean ltrRevised;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNondaniShakha() {
        return nondaniShakha;
    }

    public void setNondaniShakha(String nondaniShakha) {
        this.nondaniShakha = nondaniShakha;
    }

    public int getLtrId() {
        return ltrId;
    }

    public void setLtrId(int ltrId) {
        this.ltrId = ltrId;
    }

    public int getLtrType() {
        return ltrType;
    }

    public void setLtrType(int ltrType) {
        this.ltrType = ltrType;
    }

    public String getLtrSubject() {
        return ltrSubject;
    }

    public void setLtrSubject(String ltrSubject) {
        this.ltrSubject = ltrSubject;
    }

    public Date getLtrDate() {
        return ltrDate;
    }

    public void setLtrDate(Date ltrDate) {
        this.ltrDate = ltrDate;
    }

    public String getLtrMedium() {
        return ltrMedium;
    }

    public void setLtrMedium(String ltrMedium) {
        this.ltrMedium = ltrMedium;
    }

    public String getLtrRemark() {
        return ltrRemark;
    }

    public void setLtrRemark(String ltrRemark) {
        this.ltrRemark = ltrRemark;
    }

    public String getLtrToDept() {
        return ltrToDept;
    }

    public void setLtrToDept(String ltrToDept) {
        this.ltrToDept = ltrToDept;
    }

    public String getLtrMediumRef() {
        return ltrMediumRef;
    }

    public void setLtrMediumRef(String ltrMediumRef) {
        this.ltrMediumRef = ltrMediumRef;
    }

    public String getLtrSenderEnglish() {
        return ltrSenderEnglish;
    }

    public void setLtrSenderEnglish(String ltrSenderEnglish) {
        this.ltrSenderEnglish = ltrSenderEnglish;
    }

    public String getLtrSenderMarathi() {
        return ltrSenderMarathi;
    }

    public void setLtrSenderMarathi(String ltrSenderMarathi) {
        this.ltrSenderMarathi = ltrSenderMarathi;
    }

    public String getLtrDdoNumber() {
        return ltrDdoNumber;
    }

    public void setLtrDdoNumber(String ltrDdoNumber) {
        this.ltrDdoNumber = ltrDdoNumber;
    }

    public String getLtrSandarbhiyaShakha() {
        return ltrSandarbhiyaShakha;
    }

    public void setLtrSandarbhiyaShakha(String ltrSandarbhiyaShakha) {
        this.ltrSandarbhiyaShakha = ltrSandarbhiyaShakha;
    }

    public boolean isLtrSupplement() {
        return ltrSupplement;
    }

    public void setLtrSupplement(boolean ltrSupplement) {
        this.ltrSupplement = ltrSupplement;
    }

    public int getLtrToSubDept() {
        return ltrToSubDept;
    }

    public void setLtrToSubDept(int ltrToSubDept) {
        this.ltrToSubDept = ltrToSubDept;
    }

    public String getLtrOutwardNum() {
        return ltrOutwardNum;
    }

    public void setLtrOutwardNum(String ltrOutwardNum) {
        this.ltrOutwardNum = ltrOutwardNum;
    }

    public String getLtrInwardNum() {
        return ltrInwardNum;
    }

    public void setLtrInwardNum(String ltrInwardNum) {
        this.ltrInwardNum = ltrInwardNum;
    }

    public boolean isLtrOther() {
        return ltrOther;
    }

    public void setLtrOther(boolean ltrOther) {
        this.ltrOther = ltrOther;
    }

    public String getLtrMobileNo() {
        return ltrMobileNo;
    }

    public void setLtrMobileNo(String ltrMobileNo) {
        this.ltrMobileNo = ltrMobileNo;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public Timestamp getAdded_tz() {
        return added_tz;
    }

    public void setAdded_tz(Timestamp added_tz) {
        this.added_tz = added_tz;
    }

    public Timestamp getUpdated_tz() {
        return updated_tz;
    }

    public void setUpdated_tz(Timestamp updated_tz) {
        this.updated_tz = updated_tz;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }

    public String getChkNumber() {
        return chkNumber;
    }

    public void setChkNumber(String chkNumber) {
        this.chkNumber = chkNumber;
    }

    public double getChkAmount() {
        return chkAmount;
    }

    public void setChkAmount(double chkAmount) {
        this.chkAmount = chkAmount;
    }

    public Date getChkDate() {
        return chkDate;
    }

    public void setChkDate(Date chkDate) {
        this.chkDate = chkDate;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public boolean isRaoLtrs() {
        return raoLtrs;
    }

    public void setRaoLtrs(boolean raoLtrs) {
        this.raoLtrs = raoLtrs;
    }

    public boolean isRaoDCBills() {
        return raoDCBills;
    }

    public void setRaoDCBills(boolean raoDCBills) {
        this.raoDCBills = raoDCBills;
    }

    public boolean isRaoOBARegisters() {
        return raoOBARegisters;
    }

    public void setRaoOBARegisters(boolean raoOBARegisters) {
        this.raoOBARegisters = raoOBARegisters;
    }

    public String getLtrAcceptor() {
        return ltrAcceptor;
    }

    public void setLtrAcceptor(String ltrAcceptor) {
        this.ltrAcceptor = ltrAcceptor;
    }

    public boolean isLtrReceived() {
        return ltrReceived;
    }

    public void setLtrReceived(boolean ltrReceived) {
        this.ltrReceived = ltrReceived;
    }

    public boolean isLtrRevised() {
        return ltrRevised;
    }

    public void setLtrRevised(boolean ltrRevised) {
        this.ltrRevised = ltrRevised;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseVO baseVO = (BaseVO) o;
        return id == baseVO.id &&
                ltrId == baseVO.ltrId &&
                ltrType == baseVO.ltrType &&
                ltrSupplement == baseVO.ltrSupplement &&
                ltrToSubDept == baseVO.ltrToSubDept &&
                ltrOther == baseVO.ltrOther &&
                Double.compare(baseVO.chkAmount, chkAmount) == 0 &&
                raoLtrs == baseVO.raoLtrs &&
                raoDCBills == baseVO.raoDCBills &&
                raoOBARegisters == baseVO.raoOBARegisters &&
                ltrReceived == baseVO.ltrReceived &&
                ltrRevised == baseVO.ltrRevised &&
                Objects.equals(nondaniShakha, baseVO.nondaniShakha) &&
                Objects.equals(ltrSubject, baseVO.ltrSubject) &&
                Objects.equals(ltrDate, baseVO.ltrDate) &&
                Objects.equals(ltrMedium, baseVO.ltrMedium) &&
                Objects.equals(ltrRemark, baseVO.ltrRemark) &&
                Objects.equals(ltrToDept, baseVO.ltrToDept) &&
                Objects.equals(ltrMediumRef, baseVO.ltrMediumRef) &&
                Objects.equals(ltrSenderEnglish, baseVO.ltrSenderEnglish) &&
                Objects.equals(ltrSenderMarathi, baseVO.ltrSenderMarathi) &&
                Objects.equals(ltrDdoNumber, baseVO.ltrDdoNumber) &&
                Objects.equals(ltrSandarbhiyaShakha, baseVO.ltrSandarbhiyaShakha) &&
                Objects.equals(ltrOutwardNum, baseVO.ltrOutwardNum) &&
                Objects.equals(ltrInwardNum, baseVO.ltrInwardNum) &&
                Objects.equals(ltrMobileNo, baseVO.ltrMobileNo) &&
                Objects.equals(added_by, baseVO.added_by) &&
                Objects.equals(added_tz, baseVO.added_tz) &&
                Objects.equals(updated_tz, baseVO.updated_tz) &&
                Objects.equals(update_by, baseVO.update_by) &&
                Objects.equals(chkNumber, baseVO.chkNumber) &&
                Objects.equals(chkDate, baseVO.chkDate) &&
                Objects.equals(bankName, baseVO.bankName) &&
                Objects.equals(ltrAcceptor, baseVO.ltrAcceptor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nondaniShakha, ltrId, ltrType, ltrSubject, ltrDate, ltrMedium, ltrRemark, ltrToDept, ltrMediumRef, ltrSenderEnglish, ltrSenderMarathi, ltrDdoNumber, ltrSandarbhiyaShakha, ltrSupplement, ltrToSubDept, ltrOutwardNum, ltrInwardNum, ltrOther, ltrMobileNo, added_by, added_tz, updated_tz, update_by, chkNumber, chkAmount, chkDate, bankName, raoLtrs, raoDCBills, raoOBARegisters, ltrAcceptor, ltrReceived, ltrRevised);
    }

    @Override
    public String toString() {
        return "BaseVO{" +
                "id=" + id +
                ", nondaniShakha='" + nondaniShakha + '\'' +
                ", ltrId=" + ltrId +
                ", ltrType=" + ltrType +
                ", ltrSubject='" + ltrSubject + '\'' +
                ", ltrDate=" + ltrDate +
                ", ltrMedium='" + ltrMedium + '\'' +
                ", ltrRemark='" + ltrRemark + '\'' +
                ", ltrToDept='" + ltrToDept + '\'' +
                ", ltrMediumRef='" + ltrMediumRef + '\'' +
                ", ltrSenderEnglish='" + ltrSenderEnglish + '\'' +
                ", ltrSenderMarathi='" + ltrSenderMarathi + '\'' +
                ", ltrDdoNumber='" + ltrDdoNumber + '\'' +
                ", ltrSandarbhiyaShakha='" + ltrSandarbhiyaShakha + '\'' +
                ", ltrSupplement=" + ltrSupplement +
                ", ltrToSubDept=" + ltrToSubDept +
                ", ltrOutwardNum='" + ltrOutwardNum + '\'' +
                ", ltrInwardNum='" + ltrInwardNum + '\'' +
                ", ltrOther=" + ltrOther +
                ", ltrMobileNo='" + ltrMobileNo + '\'' +
                ", added_by='" + added_by + '\'' +
                ", added_tz=" + added_tz +
                ", updated_tz=" + updated_tz +
                ", update_by='" + update_by + '\'' +
                ", chkNumber='" + chkNumber + '\'' +
                ", chkAmount=" + chkAmount +
                ", chkDate=" + chkDate +
                ", bankName='" + bankName + '\'' +
                ", raoLtrs=" + raoLtrs +
                ", raoDCBills=" + raoDCBills +
                ", raoOBARegisters=" + raoOBARegisters +
                ", ltrAcceptor='" + ltrAcceptor + '\'' +
                ", ltrReceived=" + ltrReceived +
                ", ltrRevised=" + ltrRevised +
                '}';
    }
}
