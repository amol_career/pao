package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DDOMapper implements RowMapper<DDO> {

    @Override
    public DDO mapRow(ResultSet rs, int rowNum) throws SQLException {
        DDO ddo = new DDO();

        ddo.setId(rs.getInt("id"));
        ddo.setDdo_designation_in_english(rs.getString("ddo_designation_in_english"));
        ddo.setSection(rs.getString("section"));
        ddo.setDdo_designation_in_marathi(rs.getString("ddo_designation_in_marathi"));
        ddo.setDdo_number(rs.getString("ddo_number"));

        return ddo;
    }

}
