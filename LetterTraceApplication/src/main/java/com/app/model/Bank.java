package com.app.model;

public class Bank {

    private int id;
    private String bank_name;
    private String branch_name;
    private String auditor_name;
    private String section;

    public Bank() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getAuditor_name() {
        return auditor_name;
    }

    public void setAuditor_name(String auditor_name) {
        this.auditor_name = auditor_name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", bank_name='" + bank_name + '\'' +
                ", branch_name='" + branch_name + '\'' +
                ", auditor_name='" + auditor_name + '\'' +
                ", section='" + section + '\'' +
                '}';
    }
}
