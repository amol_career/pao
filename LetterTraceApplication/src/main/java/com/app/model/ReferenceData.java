package com.app.model;

import java.util.Objects;

public class ReferenceData {
    private int id;
    private String entityName;
    private String entityValue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityValue() {
        return entityValue;
    }

    public void setEntityValue(String entityValue) {
        this.entityValue = entityValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReferenceData that = (ReferenceData) o;
        return id == that.id &&
                entityName.equals(that.entityName) &&
                entityValue.equals(that.entityValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, entityName, entityValue);
    }

    @Override
    public String toString() {
        return "ReferenceData{" +
                "id=" + id +
                ", entityName='" + entityName + '\'' +
                ", entityValue='" + entityValue + '\'' +
                '}';
    }
}
