package com.app.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.app.controller.LoginController;

public class Employee
{
	 @SuppressWarnings("unused")

	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);
    
	 
	private String empName;
	private String designation;
	private String sewarthId;
	private String branch;
	private String empDeptId;
	private String empRoleId;

	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getSewarthId() {
		return sewarthId;
	}
	public void setSewarthId(String sewarthId) {
		this.sewarthId = sewarthId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	
	public String getEmpDeptId() {
		return empDeptId;
	}
	public void setEmpDeptId(String empDeptId) {
		this.empDeptId = empDeptId;
	}
	public String getEmpRoleId() {
		return empRoleId;
	}
	public void setEmpRoleId(String empRoleId) {
		this.empRoleId = empRoleId;
	}
	@Override
	public String toString() {
		return "Employee [empName=" + empName + ", designation=" + designation + ", sewarthId=" + sewarthId
				+ ", branch=" + branch + ", empDeptId=" + empDeptId + ", empRoleId=" + empRoleId + "]";
	      
	}
	
	
}
