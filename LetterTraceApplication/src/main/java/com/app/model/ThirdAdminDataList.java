package com.app.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class ThirdAdminDataList implements Serializable{

	private List<ThirdAdminData> thirdAdminDataList;
	
	public ThirdAdminDataList() {
		super();
	}
    
	public ThirdAdminDataList(List<ThirdAdminData> thirdAdminDataList) {
		super();
		this.thirdAdminDataList = thirdAdminDataList;
	}

	@JsonUnwrapped
	public ThirdAdminData[] thirdAdminDataArr;
	
	
	@JsonCreator
	public ThirdAdminDataList(ThirdAdminData[] thirdAdminDataList) {
	    this.thirdAdminDataArr = thirdAdminDataList;
	}

	public List<ThirdAdminData> getDataList() {
		return thirdAdminDataList;
	}

	public void setDataList(List<ThirdAdminData> dataList) {
		this.thirdAdminDataList = dataList;
	}
	
	
}
