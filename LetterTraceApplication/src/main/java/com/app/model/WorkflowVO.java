package com.app.model;

import java.sql.Date;
import java.sql.Timestamp;

public class WorkflowVO {
    private int id;
    private String ltrInwardNum;
    private String ltrOutwardNum;
    private String ltrPargamanNum;
    private String fromUser;
    private String toUser;
    private String ltrStatus;
    private String ltrSubject;
    private Date ltrDate;
    private int ltrType;
    private String ltrCategory;
    private boolean isAssigned;
    private int deptId;
    private String added_by;
    private Timestamp added_tz;
    private String update_by;
    private Timestamp updated_tz;
    private String goshwara;
    private String route_outward;
    private String route_paragaman;
    private int route_nondani_shakha;
    private String route_dept_Name;
    private String route_nondani_shakha_name;
    private String route_outward_ddo_Names;
    private String route_paragaman_dept_Names;
    private String ltrCategoryName;
    private String ltrSenderNameInMarathi;
    private String ltrFromUserName;
    private Date ltrCreateDate;
    private Date ltrCompleteDate;


    public Date getLtrCompleteDate() {
        return ltrCompleteDate;
    }

    public void setLtrCompleteDate(Date ltrCompleteDate) {
        this.ltrCompleteDate = ltrCompleteDate;
    }

    public Date getLtrCreateDate() {
        return ltrCreateDate;
    }
    public void setLtrCreateDate(Date ltrCreateDate) {
        this.ltrCreateDate = ltrCreateDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLtrInwardNum() {
        return ltrInwardNum;
    }

    public void setLtrInwardNum(String ltrInwardNum) {
        this.ltrInwardNum = ltrInwardNum;
    }

    public String getLtrOutwardNum() {
        return ltrOutwardNum;
    }

    public void setLtrOutwardNum(String ltrOutwardNum) {
        this.ltrOutwardNum = ltrOutwardNum;
    }

    public String getLtrPargamanNum() {
        return ltrPargamanNum;
    }

    public void setLtrPargamanNum(String ltrPargamanNum) {
        this.ltrPargamanNum = ltrPargamanNum;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getLtrStatus() {
        return ltrStatus;
    }

    public void setLtrStatus(String ltrStatus) {
        this.ltrStatus = ltrStatus;
    }

    public String getLtrSubject() {
        return ltrSubject;
    }

    public void setLtrSubject(String ltrSubject) {
        this.ltrSubject = ltrSubject;
    }

    public Date getLtrDate() {
        return ltrDate;
    }

    public void setLtrDate(Date ltrDate) {
        this.ltrDate = ltrDate;
    }

    public int getLtrType() {
        return ltrType;
    }

    public void setLtrType(int ltrType) {
        this.ltrType = ltrType;
    }

    public String getLtrCategory() {
        return ltrCategory;
    }

    public void setLtrCategory(String ltrCategory) {
        this.ltrCategory = ltrCategory;
    }

    public boolean isAssigned() {
        return isAssigned;
    }

    public void setAssigned(boolean assigned) {
        isAssigned = assigned;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getAdded_by() {
        return added_by;
    }

    public void setAdded_by(String added_by) {
        this.added_by = added_by;
    }

    public Timestamp getAdded_tz() {
        return added_tz;
    }

    public void setAdded_tz(Timestamp added_tz) {
        this.added_tz = added_tz;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }

    public Timestamp getUpdated_tz() {
        return updated_tz;
    }

    public void setUpdated_tz(Timestamp updated_tz) {
        this.updated_tz = updated_tz;
    }

    public String getGoshwara() {
        return goshwara;
    }

    public void setGoshwara(String goshwara) {
        this.goshwara = goshwara;
    }

    public String getRoute_outward() {
        return route_outward;
    }

    public void setRoute_outward(String route_outward) {
        this.route_outward = route_outward;
    }

    public String getRoute_paragaman() {
        return route_paragaman;
    }

    public void setRoute_paragaman(String route_paragaman) {
        this.route_paragaman = route_paragaman;
    }

    public int getRoute_nondani_shakha() {
        return route_nondani_shakha;
    }

    public void setRoute_nondani_shakha(int route_nondani_shakha) {
        this.route_nondani_shakha = route_nondani_shakha;
    }

    public String getRoute_dept_Name() {
        return route_dept_Name;
    }

    public void setRoute_dept_Name(String route_dept_Name) {
        this.route_dept_Name = route_dept_Name;
    }

    public String getRoute_nondani_shakha_name() {
        return route_nondani_shakha_name;
    }

    public void setRoute_nondani_shakha_name(String route_nondani_shakha_name) {
        this.route_nondani_shakha_name = route_nondani_shakha_name;
    }

    public String getRoute_outward_ddo_Names() {
        return route_outward_ddo_Names;
    }

    public void setRoute_outward_ddo_Names(String route_outward_ddo_Names) {
        this.route_outward_ddo_Names = route_outward_ddo_Names;
    }

    public String getRoute_paragaman_dept_Names() {
        return route_paragaman_dept_Names;
    }

    public void setRoute_paragaman_dept_Names(String route_paragaman_dept_Names) {
        this.route_paragaman_dept_Names = route_paragaman_dept_Names;
    }

    public String getLtrCategoryName() {
        return ltrCategoryName;
    }

    public void setLtrCategoryName(String ltrCategoryName) {
        this.ltrCategoryName = ltrCategoryName;
    }

    public String getLtrSenderNameInMarathi() {
        return ltrSenderNameInMarathi;
    }

    public void setLtrSenderNameInMarathi(String ltrSenderNameInMarathi) {
        this.ltrSenderNameInMarathi = ltrSenderNameInMarathi;
    }

    public String getLtrFromUserName() {
        return ltrFromUserName;
    }

    public void setLtrFromUserName(String ltrFromUserName) {
        this.ltrFromUserName = ltrFromUserName;
    }

    @Override
    public String toString() {
        return "WorkflowVO{" +
                "id=" + id +
                ", ltrInwardNum='" + ltrInwardNum + '\'' +
                ", ltrOutwardNum='" + ltrOutwardNum + '\'' +
                ", ltrPargamanNum='" + ltrPargamanNum + '\'' +
                ", fromUser='" + fromUser + '\'' +
                ", toUser='" + toUser + '\'' +
                ", ltrStatus='" + ltrStatus + '\'' +
                ", ltrSubject='" + ltrSubject + '\'' +
                ", ltrDate=" + ltrDate +
                ", ltrType=" + ltrType +
                ", ltrCategory='" + ltrCategory + '\'' +
                ", isAssigned=" + isAssigned +
                ", deptId=" + deptId +
                ", added_by='" + added_by + '\'' +
                ", added_tz=" + added_tz +
                ", update_by='" + update_by + '\'' +
                ", updated_tz=" + updated_tz +
                ", goshwara='" + goshwara + '\'' +
                ", route_outward='" + route_outward + '\'' +
                ", route_paragaman='" + route_paragaman + '\'' +
                ", route_nondani_shakha=" + route_nondani_shakha +
                ", route_dept_Name='" + route_dept_Name + '\'' +
                ", route_nondani_shakha_name='" + route_nondani_shakha_name + '\'' +
                ", route_outward_ddo_Names='" + route_outward_ddo_Names + '\'' +
                ", route_paragaman_dept_Names='" + route_paragaman_dept_Names + '\'' +
                ", ltrCategoryName='" + ltrCategoryName + '\'' +
                ", ltrSenderNameInMarathi='" + ltrSenderNameInMarathi + '\'' +
                ", ltrCreateDate=" + ltrCreateDate +
                '}';
    }

}
