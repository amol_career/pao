package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.security.PrivateKey;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UserMapping {

    private int id;
    private String pao;
    private String pao_name;
    private String dypao;
    private String dypao_name;
    private String apao;
    private String apao_name;
    private String so;
    private String so_name;
    private String ad;
    private String ad_name;
    private int sub_dept_id;
    private String addedBy;
    private Timestamp addedTz;
    private String updatedBy;
    private Timestamp updatedTz;

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public Timestamp getAddedTz() {
        return addedTz;
    }

    public void setAddedTz(Timestamp addedTz) {
        this.addedTz = addedTz;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Timestamp getUpdatedTz() {
        return updatedTz;
    }

    public void setUpdatedTz(Timestamp updatedTz) {
        this.updatedTz = updatedTz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPao() {
        return pao;
    }

    public void setPao(String pao) {
        this.pao = pao;
    }

    public String getPao_name() {
        return pao_name;
    }

    public void setPao_name(String pao_name) {
        this.pao_name = pao_name;
    }

    public String getDypao() {
        return dypao;
    }

    public void setDypao(String dypao) {
        this.dypao = dypao;
    }

    public String getDypao_name() {
        return dypao_name;
    }

    public void setDypao_name(String dypao_name) {
        this.dypao_name = dypao_name;
    }

    public String getApao() {
        return apao;
    }

    public void setApao(String apao) {
        this.apao = apao;
    }

    public String getApao_name() {
        return apao_name;
    }

    public void setApao_name(String apao_name) {
        this.apao_name = apao_name;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getSo_name() {
        return so_name;
    }

    public void setSo_name(String so_name) {
        this.so_name = so_name;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getAd_name() {
        return ad_name;
    }

    public void setAd_name(String ad_name) {
        this.ad_name = ad_name;
    }

    public int getSub_dept_id() {
        return sub_dept_id;
    }

    public void setSub_dept_id(int sub_dept_id) {
        this.sub_dept_id = sub_dept_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMapping that = (UserMapping) o;
        return id == that.id &&
                sub_dept_id == that.sub_dept_id &&
                Objects.equals(pao, that.pao) &&
                Objects.equals(pao_name, that.pao_name) &&
                Objects.equals(dypao, that.dypao) &&
                Objects.equals(dypao_name, that.dypao_name) &&
                Objects.equals(apao, that.apao) &&
                Objects.equals(apao_name, that.apao_name) &&
                Objects.equals(so, that.so) &&
                Objects.equals(so_name, that.so_name) &&
                Objects.equals(ad, that.ad) &&
                Objects.equals(ad_name, that.ad_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pao, pao_name, dypao, dypao_name, apao, apao_name, so, so_name, ad, ad_name, sub_dept_id, addedBy, addedTz, updatedBy, updatedTz );
    }

    @Override
    public String toString() {
        return "UserMapping{" +
                "id=" + id +
                ", pao='" + pao + '\'' +
                ", pao_name='" + pao_name + '\'' +
                ", dypao='" + dypao + '\'' +
                ", dypao_name='" + dypao_name + '\'' +
                ", apao='" + apao + '\'' +
                ", apao_name='" + apao_name + '\'' +
                ", so='" + so + '\'' +
                ", so_name='" + so_name + '\'' +
                ", ad='" + ad + '\'' +
                ", ad_name='" + ad_name + '\'' +
                ", sub_dept_id=" + sub_dept_id +
                ", addedBy='" + addedBy + '\'' +
                ", addedTz=" + addedTz +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedTz=" + updatedTz +
                '}';
    }

    public Map<String, Object> toMap() {
        Map<String, Object> value = new HashMap<>();
        value.put("pao", pao);
        value.put("pao_name", pao_name);
        value.put("dypao", dypao);
        value.put("dypao_name", dypao_name);
        value.put("apao", apao);
        value.put("apao_name", apao_name);
        value.put("so", so);
        value.put("so_name", so_name);
        value.put("ad", ad);
        value.put("ad_name", ad_name);
        value.put("sub_dept_id", sub_dept_id);
        value.put("addedBy", addedBy);
        value.put("addedTz", addedTz);
        value.put("updatedBy", updatedBy);
        value.put("updatedTz", updatedTz);

        return value;
    }

}
