package com.app.model;

import java.io.Serializable;

public class FifthAdminData implements Serializable{

	private String inward;
	private String subject;
	private String acceptDate;
	private String completeDate;
	private String goshwara;
	
	
	public FifthAdminData(String inward, String subject, String acceptDate, String completeDate, String goshwara) {
		//super();
		this.inward = inward;
		this.subject = subject;
		this.acceptDate = acceptDate;
		this.completeDate = completeDate;
		this.goshwara = goshwara;
	}
	
	
	public String getInward() {
		return inward;
	}


	public void setInward(String inward) {
		this.inward = inward;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		this.acceptDate = acceptDate;
	}
	public String getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}
	public String getGoshwara() {
		return goshwara;
	}
	public void setGoshwara(String goshwara) {
		this.goshwara = goshwara;
	}
	
	
}
