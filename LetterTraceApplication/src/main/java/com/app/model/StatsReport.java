package com.app.model;

import java.util.Objects;

public class StatsReport {

    String sewarthId;
    String emplName;
    int ltrNewCount;
    int ltrInProgressCount;
    int ltrRejectedCount;
    int ltrCompletedCount;

    public String getSewarthId() {
        return sewarthId;
    }

    public void setSewarthId(String sewarthId) {
        this.sewarthId = sewarthId;
    }

    public String getEmplName() {
        return emplName;
    }

    public void setEmplName(String emplName) {
        this.emplName = emplName;
    }

    public int getLtrNewCount() {
        return ltrNewCount;
    }

    public void setLtrNewCount(int ltrNewCount) {
        this.ltrNewCount = ltrNewCount;
    }

    public int getLtrInProgressCount() {
        return ltrInProgressCount;
    }

    public void setLtrInProgressCount(int ltrInProgressCount) {
        this.ltrInProgressCount = ltrInProgressCount;
    }

    public int getLtrRejectedCount() {
        return ltrRejectedCount;
    }

    public void setLtrRejectedCount(int ltrRejectedCount) {
        this.ltrRejectedCount = ltrRejectedCount;
    }

    public int getLtrCompletedCount() {
        return ltrCompletedCount;
    }

    public void setLtrCompletedCount(int ltrCompletedCount) {
        this.ltrCompletedCount = ltrCompletedCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatsReport that = (StatsReport) o;
        return Objects.equals(sewarthId, that.sewarthId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sewarthId);
    }

    @Override
    public String toString() {
        return "StatsReport{" +
                "sewarthId='" + sewarthId + '\'' +
                ", emplName='" + emplName + '\'' +
                ", ltrNewCount=" + ltrNewCount +
                ", ltrInProgressCount=" + ltrInProgressCount +
                ", ltrRejectedCount=" + ltrRejectedCount +
                ", ltrCompletedCount=" + ltrCompletedCount +
                '}';
    }
}
