package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMappingMapper implements RowMapper<UserMapping> {


    @Override
    public UserMapping mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserMapping userMapping=new UserMapping();

        userMapping.setId(rs.getInt("id"));
        userMapping.setPao(rs.getString("pao"));
        userMapping.setPao_name(rs.getString("pao_name"));
        userMapping.setDypao(rs.getString("dypao"));
        userMapping.setDypao_name(rs.getString("dypao_name"));
        userMapping.setApao(rs.getString("apao"));
        userMapping.setApao_name(rs.getString("apao_name"));
        userMapping.setSo(rs.getString("so"));
        userMapping.setSo_name(rs.getString("so_name"));
        userMapping.setAd(rs.getString("ad"));
        userMapping.setAd_name(rs.getString("ad_name"));
        userMapping.setSub_dept_id(rs.getInt("sub_dept_id"));
        return userMapping;
    }
}
