package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BankMapper implements RowMapper<Bank> {
    @Override
    public Bank mapRow(ResultSet rs, int rowNum) throws SQLException {
        Bank bank = new Bank();
        bank.setId(rs.getInt("id"));
        bank.setBank_name(rs.getString("bank_name"));
        bank.setBranch_name(rs.getString("branch_name"));
        bank.setAuditor_name(rs.getString("auditor_name"));
        bank.setSection(rs.getString("section"));

        return bank;
    }
}
