package com.app.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();

        user.setId(rs.getInt("id"));
        user.setNondaniShakha(rs.getString("nondani_shakha"));
        user.setSevarthId(rs.getString("sevarth_id"));
        user.setUserName(rs.getString("user_name"));
        user.setPassword(rs.getString("password"));
        user.setSubDepartmentId(rs.getInt("sub_dept_id"));
        user.setRoleId(rs.getInt("role_id"));

        return user;
    }
}
