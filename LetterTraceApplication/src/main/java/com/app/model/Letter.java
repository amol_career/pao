package com.app.model;

import java.util.Date;

public class Letter {

	String ltrType;
	String ltrMedium;
	String ltrLocation;
	String ltrOutNum;
	Date lDate;
	String lSubject;
	boolean supplement;
	String ltrToDept;
	String ltrRelativeDept;
	String remark;
	String nagpurLtr;
	String subLetter;
	int checkNumber;
	String bankName;
	Date checkDate;
	double amount;
	
	
	
	public String getLtrType() {
		return ltrType;
	}
	public void setLtrType(String ltrType) {
		this.ltrType = ltrType;
	}
	public String getLtrMedium() {
		return ltrMedium;
	}
	public void setLtrMedium(String ltrMedium) {
		this.ltrMedium = ltrMedium;
	}
	public String getLtrLocation() {
		return ltrLocation;
	}
	public void setLtrLocation(String ltrLocation) {
		this.ltrLocation = ltrLocation;
	}
	public String getLtrOutNum() {
		return ltrOutNum;
	}
	public void setLtrOutNum(String ltrOutNum) {
		this.ltrOutNum = ltrOutNum;
	}
	public Date getlDate() {
		return lDate;
	}
	public void setlDate(Date lDate) {
		this.lDate = lDate;
	}
	public String getlSubject() {
		return lSubject;
	}
	public void setlSubject(String lSubject) {
		this.lSubject = lSubject;
	}
	public boolean isSupplement() {
		return supplement;
	}
	public void setSupplement(boolean supplement) {
		this.supplement = supplement;
	}
	public String getLtrToDept() {
		return ltrToDept;
	}
	public void setLtrToDept(String ltrToDept) {
		this.ltrToDept = ltrToDept;
	}
	public String getLtrRelativeDept() {
		return ltrRelativeDept;
	}
	public void setLtrRelativeDept(String ltrRelativeDept) {
		this.ltrRelativeDept = ltrRelativeDept;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNagpurLtr() {
		return nagpurLtr;
	}
	public void setNagpurLtr(String nagpurLtr) {
		this.nagpurLtr = nagpurLtr;
	}
	public String getSubLetter() {
		return subLetter;
	}
	public void setSubLetter(String subLetter) {
		this.subLetter = subLetter;
	}
	public int getCheckNumber() {
		return checkNumber;
	}
	public void setCheckNumber(int checkNumber) {
		this.checkNumber = checkNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
