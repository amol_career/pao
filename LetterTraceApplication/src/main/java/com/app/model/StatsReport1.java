package com.app.model;

import java.util.Objects;

public class StatsReport1 {

    String sewarthId;
    String emplName;
    int openingbalance;
    int newLetters;
    int todayCompleted;
    int pendency;

    public String getSewarthId() {
        return sewarthId;
    }

    public void setSewarthId(String sewarthId) {
        this.sewarthId = sewarthId;
    }

    public String getEmplName() {
        return emplName;
    }

    public void setEmplName(String emplName) {
        this.emplName = emplName;
    }

    public int getOpeningbalance() {
        return openingbalance;
    }

    public void setOpeningbalance(int openingbalance) {
        this.openingbalance = openingbalance;
    }

    public int getNewLetters() {
        return newLetters;
    }

    public void setNewLetters(int newLetters) {
        this.newLetters = newLetters;
    }

    public int getTodayCompleted() {
        return todayCompleted;
    }

    public void setTodayCompleted(int todayCompleted) {
        this.todayCompleted = todayCompleted;
    }

    public int getPendency() {
        return pendency;
    }

    public void setPendency(int pendency) {
        this.pendency = pendency;
    }

    @Override
    public String toString() {
        return "StatsReport1{" +
                "sewarthId='" + sewarthId + '\'' +
                ", emplName='" + emplName + '\'' +
                ", openingbalance=" + openingbalance +
                ", newLetters=" + newLetters +
                ", todayCompleted=" + todayCompleted +
                ", pendency=" + pendency +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatsReport1 that = (StatsReport1) o;
        return sewarthId.equals(that.sewarthId) &&
                emplName.equals(that.emplName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sewarthId, emplName);
    }
}
