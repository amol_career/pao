package com.app.controller;

import com.app.dao.AuthenticationDao;
import com.app.dao.CommonLetterDao;
import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.User;
import com.app.model.WorkflowVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class BaseController {

    protected static final Logger log = LoggerFactory.getLogger(BaseLetterController.class);

    @Autowired
    protected CommonLetterDao commonLetterDao;

    @Autowired
    protected AuthenticationDao authenticationDao;

    protected Date getFormattedDate(String dateFromUI) throws Exception {
        Date result = null;
        try {
            if(dateFromUI != null) {
                java.util.Date myDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateFromUI);
                result = new Date(myDate.getTime());
            }
        }catch(Exception e) {
            throw new Exception("Error occured for parsing date :" + dateFromUI);
        }
        return result;
    }

    protected Date getFormattedUIDate(String dateFromUI) throws Exception {
        Date result = null;
        try {
            if(dateFromUI != null) {
                java.util.Date myDate = new SimpleDateFormat("yyyy-mm-dd").parse(dateFromUI);
                result = new Date(myDate.getTime());
            }
        }catch(Exception e) {
            throw new Exception("Error occured for parsing date :" + dateFromUI);
        }
        return result;
    }

    protected Date getFormattedDateForReport(String dateFromUI, boolean isFromDate) throws Exception {
        Date result = null;
        try {
            if(dateFromUI != null && dateFromUI.trim().length()>0) {
                java.util.Date myDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateFromUI);
                result = new Date(myDate.getTime());
            }else if(isFromDate){
                LocalDate backDate = LocalDate.now().minusDays(1);
                return Date.valueOf( backDate );
            }else{
                result = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            }
        }catch(Exception e) {
            throw new Exception("Error occured for parsing date :" + dateFromUI);
        }
        return result;
    }

    protected void populateModelBasedOnUser(final AuthenticationDao authDao, final ModelMap model, final String sevarthId){
        log.info("PageController :: Entering into PageController addemp ");
        try{
            User user=authDao.getUser(sevarthId);
            if(user != null){
                ReferenceData referenceData = authDao.getReferenceDataById(user.getSubDepartmentId(),"sub_department");
                model.addAttribute("sewarthId",sevarthId);
                model.addAttribute("empName",user.getUserName());
                model.addAttribute("branch",referenceData.getEntityValue());
                model.addAttribute("nondaniShakha",user.getNondaniShakha());
            }
        }catch(Exception exception){
            log.error("Error occured while addemp redirect." + exception.getMessage());
        }
    }

    protected String getRoleToRedirectAdmin(String entiryValue){
        Map<String,String> map = new HashMap<>();
        map.put("लेखापाल","dashboardAdmin-5");
        map.put("कनिष्ठ लेखापाल","dashboardAdmin-5");
        map.put("वरिष्ठ लेखापाल","dashboardAdmin-5");
        map.put("स.ले.अ.","dashboardAdmin-3");
        map.put("स.अ.व ले.अ.","dashboardAdmin-4");
        map.put("मा.अ.व ले.अ.","letterpao");
        map.put("उप.अ.व ले.अ.","letterDypao");
        map.put("पी.पी.ओ.डेस्क.","HelpDesk");
        map.put("सुविधा कक्ष","HelpDesk");
        map.put("वरिष्ठ लघुलेखक","dashboardAdmin-5");
        map.put("कनिष्ठ लघुलेखक","dashboardAdmin-5");
        map.put("लघु टंकलेखक","dashboardAdmin-5");
        map.put("वाहनचालक","dashboardAdmin-5");
        map.put("Admin7","recordAdmin");
        map.put("Admin8","addEmployee");
        map.put("Admin9","ddoAdmin");
        map.put("Admin10","informationAdmin");
        map.put("Admin11","RecordOutwardLetterBox");
        map.put("Admin12","rejectedLetter");

        return map.get(entiryValue);
    }

    private Integer getIntFromString(String value){
        Integer result= 0;
        try{
            if(value!=null) result = Integer.parseInt(value);
        }catch (Exception e){
            log.error("Error occured while parsing String ["+ value +"] to int.");
        }
        return result;
    }

    private double getDoubleFromString(String value){
        double result= 0;
        try{
            if (value!=null) result = Double.parseDouble(value);
        }catch (Exception e){
            log.error("Error occured while parsing String ["+ value +"] to double.");
        }
        return result;
    }

    protected BaseVO populateBaseVOFromModel(final MultiValueMap<String, String> letterInfo,
                                             final String user,
                                             final String inwardNumber,
                                             final String nondaniShakha,
                                             final int ltrId,
                                             final int ltrType,
                                             final int subDeptId) throws Exception{
        BaseVO baseVO = null;
        if(letterInfo != null){

            baseVO= new BaseVO();

            //Letter Id
            baseVO.setLtrId(ltrId);

            //Letter Type
            baseVO.setLtrType(ltrType);

            //पत्राचा विषय:
            baseVO.setLtrSubject(letterInfo.getFirst("ltrSubjectName") != null
                    ? letterInfo.getFirst("ltrSubjectName"):letterInfo.getFirst("itrSubject"));

            //पत्राची दिनांक
            baseVO.setLtrDate(getFormattedDate(letterInfo.getFirst("lDate")));

            //टपाल स्वीकारण्याचे माध्यम
            baseVO.setLtrMedium(letterInfo.getFirst("ltrMedium"));

            //शेरा
            baseVO.setLtrRemark(letterInfo.getFirst("remark"));

            //
            baseVO.setLtrToDept(letterInfo.getFirst("ltrToDept"));

            //माध्यमाचा संदर्भ क्रमांक
            baseVO.setLtrMediumRef(letterInfo.getFirst("hiddenLtrMediumNum"));

            //पत्र पाठविणाऱ्या व्यक्तीचे / कार्यालयाचे नाव
            if(letterInfo.getFirst("itarltrSenderName") != null && letterInfo.getFirst("itarltrSenderName").length()>0){
                baseVO.setLtrSenderMarathi(letterInfo.getFirst("itarltrSenderName"));
            }else if(letterInfo.getFirst("ltrFromDnameMarathi") != null){
                baseVO.setLtrSenderMarathi(letterInfo.getFirst("ltrFromDnameMarathi"));
            }

            //Sender Person / Office name
            baseVO.setLtrSenderEnglish(letterInfo.getFirst("ltrFromDnameEng"));

            //D.D.O. No.
            baseVO.setLtrDdoNumber(letterInfo.getFirst("ddoNumBandra"));

            //संदर्भीय शाखा
            baseVO.setLtrSandarbhiyaShakha(letterInfo.getFirst("salgnaShakha"));

            //सहपत्रे
            if(letterInfo.getFirst("supplement")!=null && letterInfo.getFirst("supplement").length()>0 &&
                    "Yes".equalsIgnoreCase(letterInfo.getFirst("supplement"))){
                baseVO.setLtrSupplement(true);
            }else{
                baseVO.setLtrSupplement(false);
            }

            //
            baseVO.setLtrToSubDept(subDeptId);

            //पत्राचा जावक क्रमांक
            baseVO.setLtrOutwardNum(letterInfo.getFirst("ltrOutNum"));

            //पत्र स्वी-कारणारा
            baseVO.setLtrAcceptor(letterInfo.getFirst("ltrAccepter"));

            //धनाकर्षार्या रक्कम
            baseVO.setChkAmount(getDoubleFromString(letterInfo.getFirst("amount")));

            //बँक व बँकेची शाखा
            baseVO.setBankName(letterInfo.getFirst("bankName"));

            //धनाकर्षाची दिनांक
            baseVO.setChkDate(getFormattedDate(letterInfo.getFirst("checkDate")));

            //धनाकर्ष नंबर
            baseVO.setChkNumber(letterInfo.getFirst("dhNumber"));

            //मोबाइल नंबर
            baseVO.setLtrMobileNo(letterInfo.getFirst("phnum"));

            //Letter other checkbox
            if(letterInfo.getFirst("itarcheckbox")!=null && letterInfo.getFirst("itarcheckbox").length()>0 &&
                    "on".equalsIgnoreCase(letterInfo.getFirst("itarcheckbox"))){
                baseVO.setLtrOther(true);
            }else{
                baseVO.setLtrOther(false);
            }

            //Nondani Shankha belong user from
            baseVO.setNondaniShakha(nondaniShakha);

            //Inward Number
            baseVO.setLtrInwardNum(inwardNumber);

            // RAO ltr, DCBill, OBARegisters

            String raoSelection = letterInfo.getFirst("raoradio");
            if ("Letter's".equals(raoSelection)) baseVO.setRaoLtrs(true);
            else if ("DC Bills".equals(raoSelection)) baseVO.setRaoDCBills(true);
            else if ("OBA Registers".equals(raoSelection)) baseVO.setRaoOBARegisters(true);

            //Misc
            baseVO.setAdded_by(user);
            baseVO.setUpdate_by(user);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            baseVO.setAdded_tz(timestamp);
            baseVO.setUpdated_tz(timestamp);

        }
        return baseVO;
    }

    protected WorkflowVO populateWorkflowVO(int deptId, String fromUser, String toUser,
                                            String inwardNo, String subject, int ltrType, Date ltrDate) throws Exception {
        WorkflowVO workflowVO = new WorkflowVO();
        workflowVO.setDeptId(deptId);
        workflowVO.setAssigned(false);
        workflowVO.setFromUser(fromUser);
        workflowVO.setLtrCategory(null);
        workflowVO.setLtrDate(ltrDate);
        workflowVO.setLtrInwardNum(inwardNo);
        workflowVO.setLtrOutwardNum(null);
        workflowVO.setLtrPargamanNum(null);
        workflowVO.setLtrStatus("new");
        workflowVO.setLtrSubject(subject);
        workflowVO.setLtrType(ltrType);
        workflowVO.setToUser(toUser);
        workflowVO.setAdded_by(fromUser);
        workflowVO.setUpdate_by(fromUser);
        return  workflowVO;
    }

    protected String generateInwardNumber(final String sewarthId, final String nondaniShakha,
                                          final String ltrType, final int individualLtrCount,
                                          final int allLtrCount){
        int currentYear =  Calendar.getInstance().get(Calendar.YEAR);
        int nextYear = currentYear + 1;
        String financeYear = String.valueOf(nextYear).substring(2,4);
        StringBuffer inwardNo = new StringBuffer();
        inwardNo.append("अवलेका-").append(nondaniShakha).append("/").
                append(sewarthId).append("/").append(ltrType)
                .append("/").append(individualLtrCount+1).append("/")
                .append(currentYear).append("-").append(financeYear)
                .append("/").append(allLtrCount+1);
        return inwardNo.toString();
    }
}

