package com.app.controller;

import com.app.dao.*;
import com.app.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sectionofficer")
public class SectionOfficerController extends BaseController {

    @Autowired
    private WorkflowDao workflowDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private AuthenticationDao authenticationDao;

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @Autowired
    private DDoAdminDao ddoAdminDao;

    @Autowired
    LetterDao letterDao;

    @Autowired
    UserDao userDao;

    @GetMapping(value="/getletters")
    @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getAssignedLtrList(@RequestParam("sewarthId") String sewarthId,@RequestParam("ltrStatus")String ltrStatus)throws Exception{
        log.info("SectionOfficerController ::getAssignedLtrList() for sewarthId " + sewarthId);
        HashMap<String,Object> letterMap = new HashMap<>();

        if(ltrStatus.equalsIgnoreCase("new")) {
            List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList("toUser='" + sewarthId + "' and isAssigned=true and ltrStatus in('new','IN_PROGRESS','in_progress')");
            for(WorkflowVO workflowVO:ltrWorkflowList) {
                if(workflowVO.getLtrPargamanNum()==null || "".equalsIgnoreCase(workflowVO.getLtrPargamanNum()))
                {
                    workflowVO.setLtrPargamanNum("");
                }
            }
            letterMap.put("ltrWorkflowList", ltrWorkflowList);
        }else if(ltrStatus.equalsIgnoreCase("processed")){
            List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList("toUser='" + sewarthId + "' and isAssigned=true and ltrStatus='" +ltrStatus+ "'");
            DDO ddo=null;

            for(WorkflowVO workfloVo:ltrWorkflowList){
                List<ReferenceData> referenceDataListOne=referenceDataDao.getReferenceData("id="+workfloVo.getLtrCategory()+"");
                ReferenceData referenceDataOne=referenceDataListOne.get(0);
                workfloVo.setLtrCategoryName(referenceDataOne.getEntityValue());

                String paragmanDeptList="";
                if(workfloVo.getRoute_outward()!=null) {

                    int id=workfloVo.getRoute_nondani_shakha();
                    List<ReferenceData> referenceDataList=referenceDataDao.getReferenceData("id="+id+"");
                    if(!referenceDataList.isEmpty()) {
                    ReferenceData referenceData=referenceDataList.get(0);
                    workfloVo.setRoute_nondani_shakha_name(referenceData.getEntityValue());
                    }

                }
                if(workfloVo.getRoute_paragaman()!=null)
                {
                    ReferenceData referenceData = referenceDataDao.getReferenceData("id=" + workfloVo.getDeptId()).get(0);
                    String paragmanString=workfloVo.getRoute_paragaman();
                    String[] paragamDept=paragmanString.split(",");
                    for(String deptId:paragamDept){
                        //List<UserMapping> userMapping=userMappingDao.getUserMapping("sub_dept_id="+Integer.parseInt(deptId)+"");
                        //UserMapping userMappingObj=userMapping.get(0);
                        //paragmanDeptList+=userMappingObj.getSo_name()+",";
                        if( ! "".equalsIgnoreCase(deptId)) {
                            List<ReferenceData> referenceDataListSecond = referenceDataDao.getReferenceData("id=" + deptId + "");
                            if(!referenceDataListSecond.isEmpty()) {
                            ReferenceData referenceDataSecond = referenceDataListSecond.get(0);
                            paragmanDeptList += referenceDataSecond.getEntityValue() + "," + " ";
                            }
                        }
                    }
                    workfloVo.setRoute_paragaman_dept_Names(paragmanDeptList);

                }
            }
            letterMap.put("ltrWorkflowList", ltrWorkflowList);
            return new ResponseEntity<HashMap<String,Object>>(letterMap, HttpStatus.OK);
        }else if(ltrStatus.equalsIgnoreCase("rejected")){
            List<WorkflowVO> ltrList=new ArrayList<>();
            List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList("toUser='" + sewarthId + "' and isAssigned=true and ltrStatus='" +ltrStatus+ "'");
            for(WorkflowVO workflowVO:ltrWorkflowList){
                List<UserMapping> userMapping=userMappingDao.getUserMapping("ad = '" + workflowVO.getFromUser() +"'");
                if(!userMapping.isEmpty()&& userMapping!=null){
                    UserMapping userMappingObj=userMapping.get(0);
                    workflowVO.setFromUser(userMappingObj.getAd_name());
                    ltrList.add(workflowVO);
                }

                if(workflowVO.getLtrPargamanNum()==null || "null".equalsIgnoreCase(workflowVO.getLtrPargamanNum()))
                {
                    workflowVO.setLtrPargamanNum("");
                }
            }
            letterMap.put("ltrWorkflowList", ltrList);
        }else if(ltrStatus.equalsIgnoreCase("completed")){
            List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList("ltrStatus='" +ltrStatus+ "'");
            letterMap.put("ltrWorkflowList", ltrWorkflowList);
        }
        List<UserMapping> listAD = userMappingDao.getUserMapping("so = '" + sewarthId +"'");
        letterMap.put("listAD", listAD);

        try {
            List<ReferenceData> listLtrCategory = authenticationDao.getReferenceDataByEntityKey("category");
            letterMap.put("listLtrCategory", listLtrCategory);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<HashMap<String,Object>>(letterMap, HttpStatus.OK);
    }

    @PostMapping(value="/postLetters",consumes="application/json;charset=UTF-8")
    public ResponseEntity<String> postLetters(@RequestBody WorkflowVO workflowVO,
                                              @RequestParam("sewarthId")String sevarthId,
                                              @RequestParam("ltrStatus")String ltrStatus
    )throws Exception {
        log.info("SectionOfficerController ::postLetters() for sewarthID " + sevarthId);
        // WorkflowVO workflowVO = new WorkflowVO();
        String data = "";
        int rowAffect=0;
        workflowVO.setFromUser(sevarthId);
        if(ltrStatus.equalsIgnoreCase("in_progress")) {
            workflowVO.setLtrStatus(ltrStatus);
            rowAffect=workflowDao.addNewWorkflow(workflowVO);
            data="Approved Successfully";
        }
        else if(ltrStatus.equalsIgnoreCase("rejected")){
            workflowVO.setLtrStatus(ltrStatus);
            //workflowVO.setToUser(workflowVO.getFromUser());
            //workflowVO.setFromUser(sevarthId);
            User user = userDao.getUserList("sevarth_id = '" + sevarthId + "'").get(0);
            ReferenceData referenceData = referenceDataDao.getReferenceData("entity_value = '" + user.getNondaniShakha() +"'").get(0);
            workflowVO.setDeptId(referenceData.getId());
            rowAffect=workflowDao.addNewWorkflow(workflowVO);
            data="Sent to Nondani Shakha Successfully";
        }
        else if(ltrStatus.equalsIgnoreCase("approved")) {
            boolean flagComplete = true;
            ReferenceData referenceData = referenceDataDao.getReferenceData("id=" + workflowVO.getDeptId()).get(0);

            WorkflowVO workflowVO1 = new WorkflowVO();
            workflowVO1.setLtrInwardNum(workflowVO.getLtrInwardNum());
            workflowVO1.setLtrPargamanNum(workflowVO.getLtrPargamanNum());
            workflowVO1.setFromUser(workflowVO.getFromUser());
            workflowVO1.setLtrDate(workflowVO.getLtrDate());
            workflowVO1.setLtrStatus("in_progress");
            workflowVO1.setLtrSubject(workflowVO.getLtrSubject());
            workflowVO1.setLtrType(workflowVO.getLtrType());
            workflowVO1.setLtrCategory(workflowVO.getLtrCategory());
            workflowVO1.setGoshwara(workflowVO.getGoshwara());
            workflowVO1.setRoute_paragaman(workflowVO.getRoute_paragaman());

            if(workflowVO.getRoute_outward()!=null
                    && ! "null".equalsIgnoreCase(workflowVO.getRoute_outward())
                    && ! "".equalsIgnoreCase(workflowVO.getRoute_outward())
            )
            {
                workflowVO.setLtrStatus("outwarded");
//                workflowVO.setToUser("");
                // workflowVO.setRoute_paragaman("");
                // workflowVO.setLtrPargamanNum("");

                Map<String,Integer> result = letterDao.getIndividualAndAllLetterCount(Integer.toString(workflowVO.getDeptId()),"outward");
                workflowVO.setLtrOutwardNum("अवलेका/" + referenceData.getEntityValue() + "/" + sevarthId + "/"
                        + workflowVO.getLtrSubject() + "/जा.क्र.-" + LocalDate.now().getYear() + "/"
                        + (result.get("deptOutwardCount") + 1));

                letterDao.updateLetter("set refOutwardNum = '" + workflowVO.getLtrOutwardNum() + "'  , ltrToSubDept = " + workflowVO.getDeptId() ,
                        " where  ltrInwardNum = '" + workflowVO.getLtrInwardNum() + "'");

                rowAffect=workflowDao.addNewWorkflow(workflowVO);
                data = data + " \r\n " + workflowVO.getLtrOutwardNum();
                flagComplete = false;
            }

            if(workflowVO1.getRoute_paragaman()!=null
                    && ! "null".equalsIgnoreCase(workflowVO1.getRoute_paragaman())
                    && ! "".equalsIgnoreCase(workflowVO1.getRoute_paragaman())
            )
            {

                Map<String,Integer> result = letterDao.getIndividualAndAllLetterCount(Integer.toString(workflowVO.getDeptId()),"paragaman");

                String paragamanString=workflowVO1.getRoute_paragaman();
                String[] deptIdList=paragamanString.split(",");
                for(String deptId:deptIdList){
                    int dept = Integer.parseInt(deptId);
                    List<UserMapping> userMappings=userMappingDao.getUserMapping("sub_dept_id='"+ dept +"'");
                    UserMapping userMapping=userMappings.get(0);
                    workflowVO1.setToUser(userMapping.getSo());
                    workflowVO1.setDeptId(dept);

                    workflowVO1.setLtrPargamanNum("अवलेका/" + referenceData.getEntityValue() + "/" + sevarthId + "/"
                            + workflowVO1.getLtrSubject() + "/पार.-" + LocalDate.now().getYear() + "/" + (result.get("totalParagamanCount") + 1));

                    letterDao.updateLetter("set ltrParagamanNum = '" + workflowVO1.getLtrPargamanNum() + "' , ltrToSubDept = " + workflowVO.getDeptId(),
                            " where  ltrInwardNum = '" + workflowVO1.getLtrInwardNum() + "'");

                    workflowDao.updateLtrWorkflow(" ltrpargamanNum = '" + workflowVO1.getLtrPargamanNum() + "'",
                            " isAssigned= 1 and ltrInwardNum = '" + workflowVO1.getLtrInwardNum() + "'");

                    // In case it is only set for paragaman then original letter need to be completed and hence paragaman needs to set
                    workflowVO.setLtrPargamanNum(workflowVO1.getLtrPargamanNum());

                    rowAffect=workflowDao.setLtrWorkflow(workflowVO1);
                    data = data + " \r\n " + workflowVO1.getLtrPargamanNum();
                }
                // flagComplete = false;
            }

            if(flagComplete) {
                workflowVO.setLtrStatus("completed");
//                workflowVO.setToUser(sevarthId);
                rowAffect=workflowDao.addNewWorkflow(workflowVO);
                data = data + " \r\n " +"Letter approved and completed";
            }
        }

        if(rowAffect>0)
        {
            return ResponseEntity.ok(data);
        }
        return ResponseEntity.badRequest().body("Error");
    }


}
