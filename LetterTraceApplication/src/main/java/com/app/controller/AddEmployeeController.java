package com.app.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.dao.AddEmployeeDao;
import com.app.dao.internalChangeDao;

@Controller
@RequestMapping(value="/app")
public class AddEmployeeController {
	
	@Autowired
	private AddEmployeeDao addEmployeeDao;
	
	 private final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);
	
	@PostMapping(value="/addemployee/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addEmployeeDataStore(@RequestBody MultiValueMap<String, String> outwardinfo)
	{
	      LOGGER.info("Enter into AddEmployeeController addEmployeeDataStore()");

		String empname=outwardinfo.getFirst("empname");
		String severthid=outwardinfo.getFirst("severthid");
		String designation=outwardinfo.getFirst("designation");
		String branch=outwardinfo.getFirst("branch");
		
		
		

		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
		System.out.println("data::"+empname+"/"+severthid+"/"+designation+"/"+branch);
			
		LOGGER.info("parameters Received ::empname::"+empname+"severthid ::"+severthid+"Branch::"+branch);
			int rowAffect=addEmployeeDao.storeAddEmployeeData(empname,severthid,designation,branch);
			if(rowAffect>0)
			{
				return "addEmployee";
			}
			System.out.println("Rresult::"+rowAffect);
	
	   LOGGER.info("Exit from AddEmployeeController addEmployeeDataStore()");
	
	
		return "page-error-400";
	}
	
	
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	@GetMapping(value="/addemployee/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchEmpInfo()
	{
		
		Map<String,Object> ldata=null;
	     List<Map> datalist=addEmployeeDao.fetchInfo();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			//System.out.println("data"+ldata);
		}
		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
	@PostMapping(value="/infoAdmin/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String changeInfoData(@RequestBody MultiValueMap<String, String> addinfo)
	{
		
		String sevarthId=addinfo.getFirst("sevarthId");
		String empname =addinfo.getFirst("empname");
		String design=addinfo.getFirst("design");
		String DeptName=addinfo.getFirst("DeptName");
		String CDeptName=addinfo.getFirst("CDeptName");
		
		System.out.println("data::"+sevarthId+"/"+empname+"/"+design+"/"+DeptName+"/"+CDeptName);
			
			int rowAffect=addEmployeeDao.ChangeInfoData(sevarthId,empname,design,DeptName,CDeptName);
			if(rowAffect>0)
			{
				return "internalEmployeeChange";
			}
			System.out.println("Rresult::"+rowAffect);
			
	
		return "page-error-400";
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/infoAdmin/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchempInfo()
	{
		
		Map<String,Object> ldata=null;
	     List<Map> datalist=addEmployeeDao.fetchempInfo();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			System.out.println("data"+ldata);
		}
		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	} */

	
	

	
	
}
