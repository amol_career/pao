package com.app.controller;

import com.app.dao.WorkflowDao;
import com.app.model.WorkflowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequestMapping(value="/workflow")
public class WorkflowController extends BaseController {

    @Autowired
    private WorkflowDao workflowDao;

    @GetMapping(value="/getWorkflowList/", produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    private ResponseEntity<List<WorkflowVO>> getLtrWorkflowList()
    {
        log.info("WorkflowController ::getLtrWorkflowList()");
        List<WorkflowVO> ltrWorkflowList;
        String whereClause="";
        ltrWorkflowList=workflowDao.getLtrWorkflowList("");
        return new ResponseEntity<List<WorkflowVO>>(ltrWorkflowList, HttpStatus.OK);
    }


    /*
    @PostMapping(value="/addworkflow/")
    private ResponseEntity<String> addNewWorkflow(@RequestBody MultiValueMap<String, String> workflowData,@RequestParam("sevarth")String sevarthId){
        log.info("WorkflowController ::addNewWorkflow()");
        String message="Failed to process letter";
        WorkflowVO workflowVO=new WorkflowVO();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                workflowVO.setLtrInwardNum(workflowData.getFirst(""));
                workflowVO.setLtrOutwardNum(workflowData.getFirst(""));
                workflowVO.setLtrPargamanNum(workflowData.getFirst(""));
                workflowVO.setFromUser(workflowData.getFirst(""));
                workflowVO.setToUser(workflowData.getFirst(""));
                workflowVO.setLtrStatus();
                workflowVO.setLtrSubject(workflowData.getFirst(""));
                workflowVO.setLtrDate(workflowData.getFirst(""));
                workflowVO.setLtrType(workflowData.getFirst(""));
                workflowVO.setLtrCategory(workflowData.getFirst(""));
                workflowVO.setAssigned(workflowData.getFirst(""));
                workflowVO.setDeptId(workflowData.getFirst(""));
                workflowVO.setAdded_by(sevarthId);
                workflowVO.setUpdate_by(sevarthId);
                workflowVO.setAdded_tz(timestamp);
                workflowVO.setUpdated_tz(timestamp);

        String setClause="isAssigned=false,update_by="+sevarthId+"";
        String whereClause="ltrInwardNum=++ and isAssigned=true";
        int count=workflowDao.addNewWorkflow(setClause,whereClause,workflowVO);
        if(count>0)
        {
            message="Letter processed Successfully";
        }

       return new ResponseEntity<String>(message, HttpStatus.OK);

    }

    */
}
