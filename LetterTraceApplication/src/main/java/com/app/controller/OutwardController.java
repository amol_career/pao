package com.app.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.dao.OutwardProcessDao;

@Controller
@RequestMapping(value="/app")
public class OutwardController 
{
	 private static final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private OutwardProcessDao outwardDao;
	@PostMapping(value="/outwardData/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String outwardDataStore(@RequestBody MultiValueMap<String, String> outwardinfo)
	{
		LOGGER.info("Entering into OutwardController outwardDataStore()");
	
		java.sql.Date sqlLDate=null;
		
		
		String subject=outwardinfo.getFirst("subject");
		String ltrtyperadio=outwardinfo.getFirst("ltrtyperadio");
		String ltype2=outwardinfo.getFirst("ltype2");
		String sendToMainBranch=outwardinfo.getFirst("sendToMainBranch");
		String sendToSubBranch=outwardinfo.getFirst("sendToSubBranch");
		

		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
		System.out.println("data::"+subject+"/"+ltrtyperadio+"/"+ltype2+"/"+sendToMainBranch+"/"+sendToSubBranch);
			
			int rowAffect=outwardDao.storeOutwardLetterData(subject,ltrtyperadio,ltype2,sendToMainBranch,sendToSubBranch);
			if(rowAffect>0)
			{
				LOGGER.info("Exit from OutwardController outwardDataStore()");
				return "outwardletter-3";
			}
			System.out.println("Rresult::"+rowAffect);
			
			LOGGER.info("Rresult::"+rowAffect);
			LOGGER.info("Exit from OutwardController outwardDataStore()");
		return "page-error-400";
	}
	
	/*
	 * outward letter count int Ltrcount2=outwardDao.getLtrCount("lettertypeone");
	 * String inwardStr2="PAO/"+"नोंदणी फोर्ट /"+""+sevarthId+"/"+"<input type="
	 * text" name="ltrinfo"> "+"/"+"2019-20"+"/"+(Ltrcount+1)+"";
	 * System.out.println("InwardStr::"+inwardStr); boolean
	 * supp2=Boolean.parseBoolean(supplement); int
	 * rowAffect2=outwarDao.storeLetterOneData(ltrMedium,hiddenLtrMediumNum,ltrType,
	 * ltrOutNo,sqlLDate,ltrSubject,supp,salgnaShakha,ltrAccepter,remark,toDept,
	 * ddoNumBandra,ltrFromDnameMarathi,ltrFromDnameEng,inwardStr); if(rowAffect>0)
	 * { return "dashboardAdmin-2"; } System.out.println("Rresult::"+rowAffect);
	 * 
	 */
	
	
}
