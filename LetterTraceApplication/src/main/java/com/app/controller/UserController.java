package com.app.controller;

import com.app.dao.UserDao;
import com.app.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UserDao userDao;

    @GetMapping(value="/getUserList", produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    private ResponseEntity<List<User>> getUserList(@RequestParam("where") String where)
    {
        log.info( getClass().getName() + " :: getUserList()");
        List<User> userList;
        userList = userDao.getUserList(where);
        return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
    }

    @PostMapping(value="/createUser", consumes="application/json;charset=UTF-8")
    public long createUser(@RequestBody User user){
        log.info( getClass().getName() + " :: createUser()");
        return userDao.createUser(user);
    }

    @PostMapping(value="/updateUser", consumes="application/json;charset=UTF-8")
    public void updateUser(@RequestBody User user){
        log.info( getClass().getName() + " :: updateUser()");
        userDao.updateUser(user);
    }

    @PostMapping(value="/deleteUser", consumes="application/json;charset=UTF-8")
    public boolean deleteUser(@RequestParam("sevarthId") String sevarthId){
        log.info( getClass().getName() + " :: deleteUser()");
        return userDao.deleteUser(sevarthId);
    }

}
