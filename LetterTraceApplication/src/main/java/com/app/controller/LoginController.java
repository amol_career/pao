package com.app.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.app.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.dao.AuthenticationDao;
import com.app.dao.LetterProcessDao;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;

@Controller
@RequestMapping(value="/app")

public class LoginController extends BaseController{

	@Autowired
	private AuthenticationDao authDao;

	@RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model){
        return "login";
    }
	
	@PostMapping(value="/validate/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String validateUser(ModelMap model,@RequestBody MultiValueMap<String, String> inputs) throws Exception	{
		log.info(" LoginController :: Start validating user..");
		String sewarthId = inputs.getFirst("sewarthId");
		String pass = inputs.getFirst("pass");
		String redirectPage = null;
		if(sewarthId!=null && pass!=null) {
			User user = authDao.validateUser(sewarthId,pass);
			log.info("User *****"+ user);
			if(user != null) {
				ReferenceData deptReferenceData = authDao.getReferenceDataById(user.getSubDepartmentId(),"sub_department");
				log.info("Deartment ref data::"+ deptReferenceData);
				ReferenceData rollReferenceData = authDao.getReferenceDataById(user.getRoleId(),"role");
                log.info("Roll ref data::"+ rollReferenceData);
				model.addAttribute("sewarthId",sewarthId);
				model.addAttribute("empName",user.getUserName());

				if(deptReferenceData != null && rollReferenceData!=null){
					model.addAttribute("branch",deptReferenceData.getEntityValue());

					log.info("***********"+deptReferenceData.getEntityValue().trim());
					log.info("***********"+rollReferenceData.getEntityValue().trim());
					if ( ("नोंदणी शाखा".equalsIgnoreCase(deptReferenceData.getEntityValue().trim()) ||
							"नोंदणी शाखा फोर्ट".equalsIgnoreCase(deptReferenceData.getEntityValue().trim()))
						&& (! "स.ले.अ.".equalsIgnoreCase(rollReferenceData.getEntityValue().trim()))){
						log.info("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
						    redirectPage = "dashboardAdmin-2";

					}
					else if ("पी.पी.ओ.डेस्क.".equalsIgnoreCase(deptReferenceData.getEntityValue().trim()) ||
							 "सुविधा कक्ष".equalsIgnoreCase(deptReferenceData.getEntityValue().trim()) ){
						log.info("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
						    redirectPage = getRoleToRedirectAdmin(deptReferenceData.getEntityValue());
					}else{
                            redirectPage = getRoleToRedirectAdmin(rollReferenceData.getEntityValue());
					}

					if ("".equalsIgnoreCase(redirectPage) || redirectPage == null){
						redirectPage = "dashboardAdmin-5";
					}
					log.info("Redirecting user "+ user +" to admin page :" + redirectPage);
				}
                return redirectPage;

			}else {
				log.info("Provided user id or/and password not valid" + "User ::" + sewarthId);
				return "page-error-400";
			}
		}else {
			log.info("Provided user id or/and password is null." + "User ::" + sewarthId );
			return "page-error-400";
		}

	}


	@GetMapping(value="/changePass/")
	public String changePassRedirect(ModelMap model) {
		log.info("LoginController :: Redirect to change Password page ");
	    return "changePassword.html";
	}

	@PostMapping(value="/updatePass/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String changePassword(ModelMap model,@RequestBody MultiValueMap<String, String> input) throws Exception{
		log.info("LoginController :: Inside in Change password page");

		User user = null;
		int updateCount=0;
		String sewarthId=input.getFirst("sewarthId");
		String pass=input.getFirst("CurrPass");
		String newPass=input.getFirst("newPass");
		if(sewarthId!=null && pass!=null && newPass!=null)
		{
			user = authDao.validateUser(sewarthId,pass);
			if(user!=null){
				updateCount = authDao.changePass(sewarthId, newPass);
				if(updateCount>0) {
					log.info("Record Updated");
                    return "index";
				}else return "page-error-400";
			}else return "page-error-400";
		}else {
			return "page-error-400";
		}
	}

}
