package com.app.controller;

import com.app.dao.ReferenceDataDao;
import com.app.dao.UserMappingDao;
import com.app.dao.WorkflowDao;
import com.app.model.LetterReport1;
import com.app.model.ReferenceData;
import com.app.model.StatsReport1;
import com.app.model.User;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Controller
@RequestMapping(value="/app")
public class ReportController1 extends BaseController {

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private WorkflowDao workflowDao;

    public static String[] adRoles = {"कनिष्ठ लघुलेखक","कनिष्ठ लेखापाल","लघु टंकलेखक",
            "लेखा लिपिक","लेखापाल","वरिष्ठ लेखापाल","वरिष्ठ लघुलेखक"};

    Map<String, String> roleMap = new HashMap<String, String>() {{
        put("pao","dypao");
        put("dypao","apao");
        put("apao","so");
        put("so","ad");
    }};

    Map<String, String> rollMappingToColumnName = new HashMap<String, String>() {{
        put("उप.अ.व ले.अ.","dypao");
        put("स.ले.अ.","so");
        put("मा.अ.व ले.अ.","pao");
        put("कनिष्ठ लघुलेखक","ad");
        put("कनिष्ठ लेखापाल","ad");
        put("स.अ.व ले.अ.","apao");
        put("लघु टंकलेखक","ad");
        put("लेखा लिपिक","ad");
        put("लेखापाल","ad");
        put("वरिष्ठ लेखापाल","ad");
        put("वरिष्ठ लघुलेखक","ad");
    }};

    @GetMapping(value="/downloadReport/")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public  HttpEntity<byte[]> getDownloadData(@RequestParam("sevarthId") String sewarthId,
                                                  @RequestParam("fromDate") String fromDate,
                                                  @RequestParam("toDate") String toDate,
                                                  @RequestParam("deptId") String deptId,
                                                  @RequestParam("sevarthOfemp") String sewathIdFromSearch,
                                                  @RequestParam("sevathIdOfLogin") String sevathIdOfLogin) throws Exception {

        log.info("ReportController1 :: getDownloadData :: sewarthId :"+sewarthId+" , fromDate :"+fromDate+
                " ,toDate : "+toDate+" ,deptId :"+deptId+" ,sewathIdFromSearch :"+sewathIdFromSearch+" ,sevathIdOfLogin :"+sevathIdOfLogin);
        HashMap<String,Object> reportMap = new HashMap<>();
        reportMap = generationReports(sewarthId,fromDate,toDate,deptId,sewathIdFromSearch,sevathIdOfLogin);
        log.info("*****************"+reportMap.toString());
        String sevarthIdToReport = sewathIdFromSearch!=null?sewathIdFromSearch:sevathIdOfLogin;
        byte[] byts = downloadReports(reportMap,sevarthIdToReport);
        SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("yyyy-MMM-dd-hh-mm-ss-aa");
        //String fileName = System.getProperty("user.home")+"\\Downloads\\Stats_"+ sewarthId + "_"+ dateTimeInGMT.format(new Date())+".xlsx";
        String fileName = "";
        if(reportMap.keySet().contains("ltrReport")){
            fileName = "Stats_";
        }else{
            fileName = "Letter_";
        }
        fileName = fileName + sevarthIdToReport + "_"+ dateTimeInGMT.format(new Date())+".xlsx";
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+fileName);
        header.setContentLength(byts.length);
        return new HttpEntity<byte[]>(byts, header);
    }


    @GetMapping(value="/searchltrStatus1/")
    @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getReportStats(@RequestParam("sevarthId") String sewarthId,
                                                                 @RequestParam("fromDate") String fromDate,
                                                                 @RequestParam("toDate") String toDate,
                                                                 @RequestParam("deptId") String deptId,
                                                                 @RequestParam("sevarthOfemp") String sewathIdFromSearch,
                                                                 @RequestParam("sevathIdOfLogin") String sevathIdOfLogin) throws Exception {
        HashMap<String,Object> reportMap = new HashMap<>();
        reportMap = generationReports(sewarthId,fromDate,toDate,deptId,sewathIdFromSearch,sevathIdOfLogin);
        log.info("###############Final reports has been generated##############################");
        log.info(reportMap.toString());
        log.info("##############################################################################");
        //downloadReports(reportMap,sewarthId);
        return new ResponseEntity<HashMap<String,Object>>(reportMap, HttpStatus.OK);
    }

    public HashMap<String,Object> generationReports(String sewarthIdToSearchReport,
                                                    final String fromDate,
                                                    final String toDate,
                                                    final String departmentId,
                                                    String sewathIdFromSearch,
                                                    String loggedInUserSevarthId) throws Exception{
        HashMap<String,Object> result = null;
        User reportSearchUser = null;
        String reportSearchUserRole = null;
        User loggedUser = null;
        String loggedUserRole = null;
        log.info("ReportController :: generationReports() :: sewarthIdToSearchReport :"+ sewarthIdToSearchReport
                +" ,fromDate :" + fromDate + " ,toDate :" + toDate +" ,departmentId :" + departmentId
                +" ,sewathIdFromSearch:" + sewathIdFromSearch +" ,loggedInUserSevarthId:" + loggedInUserSevarthId );
        try{
           // if(sewarthIdToSearchReport != null){
                result = new HashMap<>();
                if(sewarthIdToSearchReport == null || "null".equalsIgnoreCase(sewarthIdToSearchReport) || (sewarthIdToSearchReport!=null && sewarthIdToSearchReport.trim().length()==0)){
                    if(loggedInUserSevarthId !=null){
                        sewarthIdToSearchReport = loggedInUserSevarthId;
                    }
                    log.info("sewarthIdToSearchReport Id has been modified..."+ sewarthIdToSearchReport);
                }

                if(sewathIdFromSearch!=null && sewathIdFromSearch.trim().length()>0 && !"ALL".equals(sewathIdFromSearch)){
                    sewarthIdToSearchReport = sewathIdFromSearch;
                    log.info("sewarthIdToSearchReport Id has been modified... :"+ sewarthIdToSearchReport);
                }
                reportSearchUser = authenticationDao.getUser(sewarthIdToSearchReport);
                log.info("Report Search User :: " + reportSearchUser);
                reportSearchUserRole = getUserRole(reportSearchUser);
                log.info("Report Search User has role : "+ reportSearchUserRole);
                log.info("******************* loggedInUserSevarthId::"+loggedInUserSevarthId);
                if(loggedInUserSevarthId == null || "null".equalsIgnoreCase(loggedInUserSevarthId)){
                    loggedInUserSevarthId = sewarthIdToSearchReport;
                    log.info("******************* loggedInUserSevarthId::"+loggedInUserSevarthId);
                }
                loggedUser = authenticationDao.getUser(loggedInUserSevarthId);
                log.info("Logged in User :: " + loggedUser);
                loggedUserRole = getUserRole(loggedUser);
                log.info("Logged in User has role : "+ loggedUserRole);
                if(isUserRoleIsAD(reportSearchUserRole)){
                    log.info("Report Search User role evaluated as AD");
                    result = generateLetterReport(sewarthIdToSearchReport,fromDate,toDate);
                }else{
                    log.info("Report Search User role evaluated as non-AD");
                    result = generateStatReport(sewarthIdToSearchReport,reportSearchUserRole,fromDate,toDate,departmentId);
                }
                result.putAll(getBreadCrum(reportSearchUser,reportSearchUserRole,loggedUserRole));
           // }
        }catch(Exception e){
            log.error("Error occured while generating reports. Error::"+e.getMessage());
            throw e;
        }
        return result;
    }

    private HashMap<String,Object> getBreadCrum(User reprotSearchUser , String reportSearchUserRole, String loggedUserRole){
        HashMap<String,Object> result = null;
        List<String> bredCrums = null;
        log.info("ReportController1 :: getBreadCrum() - reportSearchUserRole :" + reportSearchUserRole + " ,reprotSearchUser:"+reprotSearchUser);
        if(reportSearchUserRole!=null && reportSearchUserRole.trim().length()>0 && reprotSearchUser!=null){
            String reportSearchUserRoleForColName = getRollMappingForColumnName(reportSearchUserRole);
            String loggedUserRoleForColName = getRollMappingForColumnName(loggedUserRole);
            log.info("BreadCrum start preparing for report search user role ["+reportSearchUserRole+"] as per user mapping table");
            List<String> allSuperior = getYourAllSuperior(reportSearchUserRoleForColName,loggedUserRoleForColName);
            log.info("All your superiors : [ "+ allSuperior +" ] for role :" + reportSearchUserRoleForColName);
            List<Map<String,Object>> mapList = userMappingDao.getUserMappingForReportBreadCrum(reportSearchUserRoleForColName, reprotSearchUser.getSevarthId());
            if(mapList!=null && mapList.size()>0 && allSuperior!=null && allSuperior.size()>0){
                result = new HashMap<>();
                bredCrums = new ArrayList<>();
                Map<String,Object> tempMap = mapList.get(0);
                log.info("One sample records fetched from DB for bed crum :: "+ tempMap);
                for(String superior : allSuperior){
                    bredCrums.add(tempMap.get(superior).toString());
                }
                result.put("breadCrum",bredCrums);
            }
            log.info("Bread Crum has been generated :: "+ result);
        }
        return result;
    }

    private HashMap<String,Object> generateStatReport(final String loggedInUser,
                                                      final String userRole,
                                                      final String fromDate,
                                                      final String toDate,
                                                      final String departId) throws Exception{
        log.info("StatReport :: generateStatReport() :: loggedInUser  :"+ loggedInUser +
                " ,userRole :" + userRole + " ,fromDate:"+fromDate + " ,toDate: "+ toDate
                + " ,departId:"+ departId);
        HashMap<String,Object> result = null;
        List<StatsReport1> statsReportList = null;
        List<Map<String,Object>> userMappings = null;
        try{
            if(loggedInUser!=null && userRole != null){
                result = new HashMap<>();
                statsReportList = new ArrayList<>();
                String userRoleForColName = getRollMappingForColumnName(userRole);
                userMappings = userMappingDao.getUserMappingForReport(userRoleForColName,
                        getYourReporty(userRoleForColName),loggedInUser);
                log.info("User Mapping => " + userMappings);
                if(userMappings!=null && userMappings.size()>0){
                    StatsReport1 statsReport = null;
                    Map<StatsReport1,List<String>> tempMapForADList = new HashMap<>();
                    for(Map<String,Object> userMapping : userMappings){
                        statsReport = new StatsReport1();
                        statsReport.setSewarthId(userMapping.get("reporty").toString());
                        statsReport.setEmplName(userMapping.get("reporty_name").toString());
                        if(tempMapForADList.containsKey(statsReport)){
                            List adList = tempMapForADList.get(statsReport);
                            adList.add(userMapping.get("ad").toString());
                        }else{
                            List<String> adList = new ArrayList<>();
                            adList.add(userMapping.get("ad").toString());
                            tempMapForADList.put(statsReport,adList);
                        }
                    }
                    log.info("Ad list has been prepared :: " + tempMapForADList);
                    if(tempMapForADList.size() > 0){
                        for(StatsReport1 record : tempMapForADList.keySet()){
                            log.info("Fetching pendency reports for ::" + record);
                            log.info("Ad's =>" + tempMapForADList.get(record));
                            workflowDao.getAndUpdatePendencyReportsForADs(tempMapForADList.get(record),
                                    getFormattedDateForReport(fromDate,true),
                                    getFormattedDateForReport(toDate,false), departId, record);
                            statsReportList.add(record);
                        }
                    }
                    log.info("StatsReports has been updated ::" + statsReportList);
                    result.put("statReport",statsReportList);
                }
            }
            log.info("Final reports for User :"+ loggedInUser +" has been completed ::" + result);
        }catch(Exception e){
            log.error("Error occurred while generating stat report for user :"+loggedInUser +" Error -" + e.getMessage());
            throw e;
        }
        return result;
    }

    private HashMap<String,Object> generateLetterReport(final String loggedInUser,
                                                        final String fromDate,
                                                        final String toDate) throws Exception{
        log.info("StatReport :: generateLetterReport() :: loggedInUser  :"+ loggedInUser +
                " ,fromDate:"+fromDate + " ,toDate: "+ toDate);
        HashMap<String,Object> result = null;
        List<LetterReport1> letterReportList = null;
        if(loggedInUser!=null){
            result = new HashMap<>();
            letterReportList = workflowDao.getPendencyReportsForIndividualAD(loggedInUser,
                    getFormattedDateForReport(fromDate,true),getFormattedDateForReport(toDate,false));
            log.info("Letters report has been prepared :" + letterReportList);
            result.put("ltrReport", populateLetterName(letterReportList));
            log.info("Final Letters report has been prepared :" + result);
        }
        return result;
    }

    private List<LetterReport1> populateLetterName(List<LetterReport1> letterReports){
        if(letterReports!=null && letterReports.size()>0){
            try {
                log.info("Got letter reports to populate letter name from refrence data::" + letterReports);
                List<ReferenceData> referenceDataList = referenceDataDao.getReferenceData("entity_key = 'letter_type'");
                Map<String, String> map = new HashMap<>();
                for (ReferenceData refData : referenceDataList) {
                    map.put(String.valueOf(refData.getId()), refData.getEntityValue());
                }
                log.info("Letter id -> name map has been prepared..." + map);
                for (LetterReport1 ltr : letterReports) {
                    log.info("LetterReport ::" + ltr);
                    log.info("LtrId ::" + ltr.getLtrId() + " ==> map " + map.get(String.valueOf(ltr.getLtrId())));
                    String ltrNameForReport = getLetterNameForReport(map.get(String.valueOf(ltr.getLtrId())));
                    if(ltrNameForReport!=null){
                        ltr.setLtrType(ltrNameForReport);
                    }else{
                        ltr.setLtrType(map.get(ltr.getLtrId()));
                    }
                }
                log.info("Letter report after letter name changed : " + letterReports);
            }catch(Exception e){
                log.error(e.getMessage());
                throw e;
            }
        }
        return letterReports;
    }

    private String getLetterNameForReport(String ltrType){
        Map<String,String> ltrNames = new HashMap<>();
        ltrNames.put("सर्वसाधारण पत्रे","सर्वसाधारण टपाल");
        ltrNames.put("शासकीय पत्रे","शासकीय टपाल");
        ltrNames.put("धनादेश","धनादेश");
        ltrNames.put("अर्धशासकीय पत्रे","अर्धशासकीय टपाल");
        ltrNames.put("मुंबई","महालेखापाल , मुंबई कार्यालयाकडून प्राप्त टपाल / आदेश");
        ltrNames.put("नागपूर","महालेखापाल ,नागपुर कार्यालयाकडून प्राप्त टपाल /आदेश");
        ltrNames.put("DCPS शाखा","DCPS शाखेकरीता टपाल");
        ltrNames.put("धनाकर्ष","धनाकर्ष");
        ltrNames.put("निवासी लेखा","निवासी लेखा परीक्षा अधिकारी, कार्यालयाकडून प्राप्त टपाल");
        ltrNames.put("माहितीची अधिकार पत्रे","माहितीच्या अधिकाराखाली प्राप्त टपाल");
        ltrNames.put("मा.विधानसभा सदस्य, मा सचिव यांच्याकडील पत्रे","मा. लोकप्रतिनिधी यांचेकडून प्राप्त टपाल");
        ltrNames.put("लोकायुक्त कार्यालयाकडून येणारी पत्रे","लोकायुक्त कार्यालयाकडून प्राप्त टपाल");
        ltrNames.put("आपले सरकार","आपले सरकार पोर्टलद्वारा प्राप्त टपाल");
        ltrNames.put("न्यायालयीन पत्रे","न्यायालयीन टपाल");
        ltrNames.put("PPO-मुंबई","PPO-मुंबई");
        ltrNames.put("GPO-मुंबई","GPO-मुंबई");
        ltrNames.put("CPO-मुंबई","CPO-मुंबई");
        ltrNames.put("GPF-मुंबई","GPF-मुंबई");
        ltrNames.put("LTA-मुंबई","LTA-मुंबई");
        ltrNames.put("LETTER-मुंबई","LETTER-मुंबई");
        ltrNames.put("PPO-नागपूर","PPO-नागपूर");
        ltrNames.put("GPO-नागपूर","GPO-नागपूर");
        ltrNames.put("CPO-नागपूर","CPO-नागपूर");
        ltrNames.put("GPF-नागपूर","GPF-नागपूर");
        ltrNames.put("LTA-नागपूर","LTA-नागपूर");
        ltrNames.put("LETTER-नागपूर","LETTER-नागपूर");
        return ltrNames.get(ltrType);
    }

    private boolean isUserRoleIsAD(String userRole) {
        boolean result = false;
        for (String role : adRoles) {
            if (role.equalsIgnoreCase(userRole)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private String getRollMappingForColumnName(String role){
        return rollMappingToColumnName.get(role);
    }


    private String getUserRole(User loggedInUser){
        String result = null;
        List<ReferenceData> referenceDataList = null;
        if(loggedInUser != null){
            referenceDataList = referenceDataDao.getReferenceData("entity_key='role'");
            for(ReferenceData refData : referenceDataList){
                if(refData.getId()==loggedInUser.getRoleId()){
                    result = refData.getEntityValue();
                    break;
                }
            }
        }
        return result;
    }

    private String getYourReporty(String roleName){
        return roleMap.get(roleName);
    }

    private String getYourImmediateSuperior(String roleName){
        String result = null;
        for(String key : roleMap.keySet()){
            String value = roleMap.get(key);
            if(value.equals(roleName)){
                result = key;
                break;
            }
        }
        return result;
    }

    private List<String> getYourAllSuperior(String roleName, String parentRoleName){
        List<String> roles = new ArrayList<>();
        String temp = roleName;

        while (temp!=null){
            temp = getYourImmediateSuperior(temp);
            if(temp!=null){
                roles.add(temp);
            }
        }

        if(roles.size()>0){
            Collections.reverse(roles);
            List<String> superSuperior = new ArrayList<>();
            for(String role : roles){
                if(!role.equalsIgnoreCase(parentRoleName)){
                    superSuperior.add(role);
                }else{
                    break;
                }
            }
            roles.removeAll(superSuperior);

        }else{
            roles.add(roleName);
        }
        if(!roles.contains(roleName)){
            roles.add(roleName);
        }
        return roles;
    }


    private byte[] downloadReports(HashMap<String,Object> reportMap, String sewarthId) throws Exception{
        Object statReportObj = reportMap.get("statReport");
        Object ltrReportObj = reportMap.get("ltrReport");
        ClassLoader classLoader = getClass().getClassLoader();
        byte[]  byts = null;
        if(statReportObj != null){
            List<StatsReport1> reportList = (List)statReportObj;
            if(reportList != null && reportList.size()>0){
                File file = new File(classLoader.getResource("stats_report_template.xlsx").getFile());
                Workbook templateWorkbook = WorkbookFactory.create(file);
                generateStatReportsInExcel(reportList,templateWorkbook,sewarthId);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                templateWorkbook.write(baos);
                byts = baos.toByteArray();
            }
        }else if (ltrReportObj !=null){
            List<LetterReport1> ltrList = (List)ltrReportObj;
            if(ltrList != null && ltrList.size()>0){
                File file = new File(classLoader.getResource("letter_report_template.xlsx").getFile());
                Workbook templateWorkbook = WorkbookFactory.create(file);
                generateLetterReportsInExcel(ltrList,templateWorkbook,sewarthId);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                templateWorkbook.write(baos);
                byts = baos.toByteArray();
            }
        }
        return byts;
    }

    private void generateStatReportsInExcel(List<StatsReport1> reportList,
                                            Workbook templateWorkbook, String sewarthId){
        Sheet statsReportSheet = templateWorkbook.getSheet("Stats");
        int rowCount = 1;
        Row headerRow = statsReportSheet.getRow(0);
        if(headerRow!=null){
            Cell cell = headerRow.getCell(0);
            cell.setCellValue(sewarthId);
        }
        for(StatsReport1 report : reportList){
            Row row = statsReportSheet.createRow(rowCount+2);
            Cell srNoCell = row.createCell(0);
            srNoCell.setCellValue(rowCount);

            Cell sewarthIdCell = row.createCell(1);
            sewarthIdCell.setCellValue(report.getSewarthId());

            Cell empNameCell = row.createCell(2);
            empNameCell.setCellValue(report.getEmplName());

            Cell openingBalanceCell = row.createCell(3);
            openingBalanceCell.setCellValue(report.getOpeningbalance());

            Cell newLtrCell = row.createCell(4);
            newLtrCell.setCellValue(report.getNewLetters());

            Cell completedCell = row.createCell(5);
            completedCell.setCellValue(report.getTodayCompleted());

            Cell pendencyCell = row.createCell(6);
            pendencyCell.setCellValue(report.getTodayCompleted());

            rowCount++;
        }
    }

    private void generateStatReportsInExcel_New(HashMap<String,Object> reportMap,
                                                Workbook templateWorkbook, String sewarthId){

        LinkedHashMap parentLinkedHashMap = (LinkedHashMap)reportMap.get("statReport");
        ArrayList<LinkedHashMap> list = (ArrayList<LinkedHashMap>)parentLinkedHashMap.get("statReport");

        Sheet statsReportSheet = templateWorkbook.getSheet("Stats");
        int rowCount = 1;
        Row headerRow = statsReportSheet.getRow(0);

        if(headerRow!=null){
            Cell cell = headerRow.getCell(0);
            cell.setCellValue(sewarthId);
        }
        for(LinkedHashMap map : list){
            Row row = statsReportSheet.createRow(rowCount+2);
            Cell srNoCell = row.createCell(0);
            srNoCell.setCellValue(rowCount);

            Cell sewarthIdCell = row.createCell(1);
            sewarthIdCell.setCellValue(map.get("sewarthId").toString());

            Cell empNameCell = row.createCell(2);
            empNameCell.setCellValue(map.get("emplName").toString());

            Cell openingBalanceCell = row.createCell(3);
            openingBalanceCell.setCellValue(map.get("openingbalance").toString());

            Cell newLtrCell = row.createCell(4);
            newLtrCell.setCellValue(map.get("newLetters").toString());

            Cell completedCell = row.createCell(5);
            completedCell.setCellValue(map.get("todayCompleted").toString());

            Cell pendencyCell = row.createCell(6);
            pendencyCell.setCellValue(map.get("pendency").toString());

            rowCount++;
        }
    }

    private void generateLetterReportsInExcel(List<LetterReport1> reportList,
                                              Workbook templateWorkbook, String sewarthId){
        Sheet statsReportSheet = templateWorkbook.getSheet("Letter");
        int rowCount = 1;
        Row headerRow = statsReportSheet.getRow(0);
        if(headerRow!=null){
            Cell cell = headerRow.getCell(0);
            cell.setCellValue(sewarthId);
        }
        for(LetterReport1 report : reportList){
            Row row = statsReportSheet.createRow(rowCount+2);
            Cell srNoCell = row.createCell(0);
            srNoCell.setCellValue(rowCount);

            Cell sewarthIdCell = row.createCell(1);
            sewarthIdCell.setCellValue(report.getLtrType());

            Cell empNameCell = row.createCell(2);
            empNameCell.setCellValue(report.getInwardNumber());

            Cell openingBalanceCell = row.createCell(3);
            openingBalanceCell.setCellValue(report.getSubject());

            Cell newLtrCell = row.createCell(4);
            newLtrCell.setCellValue(report.getLtrDate());

            rowCount++;
        }
    }

    public static void main(String sd[]) throws Exception{
       /* StatsReport1 s = new StatsReport1();
        s.setSewarthId("A");
        s.setPendency(12);
        s.setTodayCompleted(1);
        s.setNewLetters(3);
        s.setOpeningbalance(5);
        s.setEmplName("Name1");

        StatsReport1 s1 = new StatsReport1();
        s1.setSewarthId("A1");
        s1.setPendency(12);
        s1.setTodayCompleted(1);
        s1.setNewLetters(3);
        s1.setOpeningbalance(5);
        s1.setEmplName("Name2");

        List<StatsReport1> list = new ArrayList<>();
        list.add(s1);
        list.add(s);

        HashMap<String, Object> map = new HashMap<>();
        //map.put("statReport",list);

        //**********************************************************************************

        LetterReport1 l = new LetterReport1();
        l.setLtrId(1);
        l.setSubject("Sub1");
        l.setLtrDate("2020-01-01");
        l.setInwardNumber("Inward number -1-1-1");
        l.setLtrType("Letter 1");

        LetterReport1 l1 = new LetterReport1();
        l1.setLtrId(1);
        l1.setSubject("Sub2");
        l1.setLtrDate("2020-01-01");
        l1.setInwardNumber("Inward number -2-2-2");
        l1.setLtrType("Letter 2");

        List<LetterReport1> list1 = new ArrayList<>();
        list1.add(l);
        list1.add(l1);

        map.put("ltrReport",list1);*/

        //new ReportController1().downloadReports(map,"AAAAA12345");

        System.out.println(new ReportController1().getYourAllSuperior("dypao","dypao"));

        //put("pao","dypao");
        //put("dypao","apao");
        //put("apao","so");
        //put("so","ad");

    }

}
