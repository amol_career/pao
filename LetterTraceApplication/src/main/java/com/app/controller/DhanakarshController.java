package com.app.controller;

import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.WorkflowVO;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping(value="/app")
public class DhanakarshController extends BaseLetterController {

    @Override
    @PostMapping(value="/dhanakarsh/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    protected String saveLetter(ModelMap model,@RequestBody MultiValueMap<String, String> letterInfo) {
        try {
            String lettertypeNamehidden = letterInfo.getFirst("lettertypeName");
            String sewarthId=letterInfo.getFirst("sevarthId");
            String nondaniShakha = letterInfo.getFirst("nondaniShakha");
            String deptName = letterInfo.getFirst("salgnaShakha");
            String ltrSubjectName = letterInfo.getFirst("ltrSubjectName");
            String bankName =  letterInfo.getFirst("bankNames");
            String bankBranchName =  letterInfo.getFirst("bankBranchName");
            
            String lettertypename=letterInfo.getFirst("lettertypeName");
            String employeeName=letterInfo.getFirst("employeeName");
            String employeebranch=letterInfo.getFirst("employeebranch");
            String success ="Your data is submmited Successfully";
            
            log.info("***** lettertypeNamehidden::"+lettertypeNamehidden);
            log.info("***** sewarthId::"+sewarthId);
            log.info("***** nondaniShakha::"+nondaniShakha);
            log.info("***** deptName::"+deptName);
            log.info("***** ltrSubjectName::"+ltrSubjectName);
           // deptName = "अराप - 6";
            ReferenceData ltrReferenceData = authenticationDao.getReferenceDataByKeyAndValue("letter_type",lettertypeNamehidden);
            ReferenceData deptReferenceData = authenticationDao.getReferenceDataByKeyAndValue("sub_department",deptName);
            if(ltrReferenceData == null)
                log.warn("Reference data is not available for letter  "+ lettertypeNamehidden);
            if(deptReferenceData == null)
                log.warn("Reference data is not available for department  "+ deptName);

            Map<String,Integer> counts = commonLetterDao.getIndividualAndAllLetterCount(ltrReferenceData.getId(),nondaniShakha);

            log.info("Collected letter counts for inward number ::  "+counts);
            Integer individualLtrCount = 0;
            Integer allLtrCount = 0;

            if(counts.get("IndividualLtrCount")!=null){
                individualLtrCount = counts.get("IndividualLtrCount").intValue();
            }

            if(counts.get("AllLetterCount")!=null){
                allLtrCount = counts.get("AllLetterCount").intValue();
            }

            String inwardNo = generateInwardNumber(sewarthId,nondaniShakha,ltrReferenceData.getEntityValue(),
                    individualLtrCount,allLtrCount);

            log.info("Inward Id has been prepared..."+inwardNo);
            BaseVO vo = populateBaseVOFromModel(letterInfo,sewarthId,
                    inwardNo, nondaniShakha,individualLtrCount+1,ltrReferenceData.getId(),
                    deptReferenceData.getId());

            String toUser = commonLetterDao.getSewartIdForWorkflowNo(vo.getLtrToSubDept(),vo.getLtrAcceptor());

            WorkflowVO workflowVO = populateWorkflowVO(vo.getLtrToSubDept(),sewarthId,toUser,
                    vo.getLtrInwardNum(),vo.getLtrSubject(),ltrReferenceData.getId(),
                    vo.getLtrDate());

            int rowAffect = commonLetterDao.storeData(vo, workflowVO);

            if(rowAffect>0)	{
            	
            	 model.addAttribute("InwardString",inwardNo);
                 model.addAttribute("employeebranch",employeebranch);
                 model.addAttribute("employeeName",employeeName);
                 model.addAttribute("sewarthIdC",sewarthId);
                 model.addAttribute("lettertypenameC",lettertypename);
                 model.addAttribute("nondaniShakhaByC",nondaniShakha);
                 
 //  success
                 model.addAttribute("successMsg",success+lettertypename);
                 
                log.info("Letter has been stores successfully");
                return "DhanakarshLetter";
            }
        } catch (Exception e) {
            log.error("Error occured while storing letter. :: ChequeController :: Error - " + e.getMessage());
        }
        return "page-error-400";
    }
 /* @RequestMapping(value="/ddomarathiid/",method= RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchList(@RequestParam("ddomarathiId") String ddomarathiId) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddomarathiId,"MARATHI");

        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: getBranchList :: Error - " + e.getMessage());
        }

        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @RequestMapping(value="/ddoenglishid/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchListE(@RequestParam("ddoEnglishId") String ddoEnglishId) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddoEnglishId,"ENGLISH");
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. ::getBranchListE() :: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @RequestMapping(value="/ddonumberid/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchListN(@RequestParam("ddoNumberId") String ddonumberid)
    {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddonumberid,"DDONUMBER");
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. ::getBranchListN:: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }
    @GetMapping(value="/ltrmediumdropdown/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchLetterMediumDropdown() {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchLtrMediumDropdown();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchLetterMediumDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }*/
}
