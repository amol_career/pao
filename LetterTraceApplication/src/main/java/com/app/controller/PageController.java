package com.app.controller;
import com.app.dao.AuthenticationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/pageNavigation")
public class PageController extends BaseController {

	@Autowired
	private AuthenticationDao authDao;

	@GetMapping(value="/addemp/")
	public String changePassRedirect(ModelMap model,@RequestParam("sewarth") String sewarthId ) {
		log.info("PageController :: Entering into PageController addemp ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "addEmployee.html";
	}

	@GetMapping(value="/karyvivrannond/")
	public String karyVivranNond(ModelMap model,@RequestParam("sevarthId") String sewarthId) {
		log.info("PageController :: Entering into PageController karyvivrannond ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"KaryVivranNond.html";
	}

	@GetMapping(value="/internalchange/")
	public String internalchange(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController internalchange ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"internalEmployeeChange.html";
	}

	@GetMapping(value = "/newemp/")
	public String addEmployee(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController internalchange ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"addEmployee.html";
	}

	@GetMapping(value="/allemp/")
	public String allEmp(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController allemp ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "AllEmp.html";
	}

	@GetMapping(value="/adminone/")
	public String paoReport(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController adminone ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"dashboardAdmin-1.html";
	}

	@GetMapping(value="/reports/")
	public String report(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController reports ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"Reports2.html";
	}

	@GetMapping(value="/reports4/")
	public String report4(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController reports4 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"Reports4.html";
	}

	@GetMapping(value="/reportRA/")
	public String reportRA(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController reportRA ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"reportRA.html";
	}

	@GetMapping(value="/admin2/")
	public String admin2(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController record ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "dashboardAdmin-2";
	}

	@GetMapping(value="/admin3/")
	//DashBoard 5 Controllers
	public String admin3(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController admin3 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "dashboardAdmin-3.html";
	}

	@GetMapping(value="/adminfive/")
	public String adminFive(ModelMap model,@RequestParam("sevarthId") String sewarthId) {
		log.info("PageController :: Entering into PageController adminfive ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "dashboardAdmin-5.html";
	}

	@GetMapping(value ="/outward3/")

	public String outwardRedirect3(ModelMap model,@RequestParam("sewarth") String sewarthId) {
		log.info("PageController :: Entering into PageController outward3 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "outwardletter-3.html";
	}

	@GetMapping(value="/admin4/")
	//DashBoard 5 Controllers
	public String admin4(ModelMap model,@RequestParam("sewarthId") String sewarthId){
		log.info("PageController :: Entering into PageController admin4 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "dashboardAdmin-4.html";
	}

	@GetMapping(value ="/outward2/")
	public String outwardRedirect4(ModelMap model,@RequestParam("sewarth") String sewarthId){
		log.info("PageController :: Entering into PageController outward2 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "outwardletter-2.html";
	}

	@GetMapping(value ="/pragaman/")
	public String pragamanNondVahi(ModelMap model,@RequestParam("sewarth") String sewarthId){
		log.info("Entering into PageController pragaman ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "pragmanNondVahi.html";
	}

	@GetMapping(value ="/javak/")
	public String javakNondVahi(ModelMap model,@RequestParam("sewarth") String sewarthId){
		log.info("Entering into PageController javak ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return "javaknondvahi.html";
	}

	@GetMapping(value = "/inbox2/")
	public String inbox4(ModelMap model,@RequestParam("sevarthId") String sewarthId){
		log.info("Entering into PageController inbox2 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"inbox-2.html";
	}

	@GetMapping(value = "/inboxthree/")
	public String inboxThree(ModelMap model,@RequestParam("sewarthId") String sewarthId){
		log.info("Entering into PageController inboxThree ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		return"inbox-3.html";
	}
	@GetMapping(value = "/inbox/")
	public String inbox(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController inbox ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController inbox ");
		return"inbox.html";
	}

	/* Admin-3 pending letter for approval */

	@GetMapping(value = "/pending3/")
	public String inboxpending(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController inbox ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController inbox ");
		return"inbox.html";
	}

	//Admin 2 ford

	@GetMapping(value="/recordtwo/")
	public String record2(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordtwo ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController recordtwo ");
		return "dashboardAdminFord.html";
	}

	//Record Admin 2

	@GetMapping(value="/AllRecord/")
	public String allRecord(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController AllRecord ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController AllRecord ");
		return "AllRecordData.html";
	}


	@GetMapping(value="/rejectedletter/")
	public String RejectLetter(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController RejectLetter ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController rejectedletter ");
		return "rejectedletter.html";
	}

	@GetMapping(value="/rrorejectedltrs/")
	public String rroRejectLetters(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController rroRejectLetters()");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController rroRejectLetters()");
		return "rejectedletter.html";
	}
	@GetMapping(value="/Admin7/")
	public String Admin7(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController Admin7 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController Admin7 ");
		return "recordAdmin.html";
	}
	@GetMapping(value = "/information/")
	public String information(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController information ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController information ");
		return"info.html";
	}
	@GetMapping(value = "/informationadmin/")
	public String informationAdmin(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController informationadmin ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController informationadmin ");
		return"informationAdmin.html";
	}

	@GetMapping(value = "/recordoutward/")
	public String recordOutwardLetterBox(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordoutward ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController recordoutward ");
		return"RecordOutwardLetterBox";
	}

	@GetMapping(value="/table3/")
	public String table(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController table3 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController table3 ");

		return"table-3.html";
	}

	@GetMapping(value="/inoffice/")
	public String info1(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController inoffice ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit from PageController inoffice ");

		return"inoffice.html";
	}

	@GetMapping(value="/outoffice/")
	public String info2(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController outoffice ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController outoffice ");


		return"outoffice.html";
	}

	@GetMapping(value="/letterpao/")
	public String letterpao(ModelMap model, @RequestParam("sewarthId") String sewarthId)
	{
		log.info("Entering into PageController letterpao ");
		populateModelBasedOnUser(authDao, model, sewarthId);
		log.info("Exit fromPageController letterpao ");

		return"letterpao.html";
	}

	@GetMapping(value="/letterdypao/")
	public String letterdypao(ModelMap model, @RequestParam("sewarthId") String sewarthId)
	{
		log.info("Entering into PageController letterdypao ");
		populateModelBasedOnUser(authDao, model, sewarthId);
		log.info("Exit fromPageController letterdypao ");

		return"letterDypao.html";
	}

	@GetMapping(value="/reportsdypao/")
	public String reportsdypao(ModelMap model, @RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController reportsdypao ");
		populateModelBasedOnUser(authDao, model, sewarthId);
		log.info("Exit fromPageController reportsdypao ");

		return"dashboardDyAdmin-1.html";
	}



	@GetMapping(value="/rletter3/")
	public String rletter(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController rletter3 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController rletter3 ");

		return"rletter3.html";
	}

	@GetMapping(value="/reportpao/")
	public String rletterpao(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController reportpao ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController reportpao ");

		return"reportspao.html";
	}


	@GetMapping(value="/inboxpao/")
	public String letterspao(ModelMap model,@RequestParam("sewarthId") String sewarthId)
	{
		log.info("Entering into PageController inboxpao ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController inboxpao ");

		return"inboxpao.html";
	}


	@GetMapping(value="/inboxdypao/")
	public String lettersdypao(ModelMap model,@RequestParam("sewarthId") String sewarthId)
	{
		log.info("Entering into PageController inboxdypao ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController inboxdypao ");

		return"inboxdypao.html";
	}

	@GetMapping(value="/reports123/")
	public String letterspao1(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController reports123 ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("Exit fromPageController reports123 ");

		return"reports.html";
	}

	//New Letter page controller's

	@GetMapping(value="/sampleLtrch/")
	public String sampleLtrch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in sampleLtrch::"+sewarthId);

		return"letter1ch.html";
	}
	@GetMapping(value="/shaskiych/")
	public String shaskiych(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in shaskiych::"+sewarthId);

		return"letter2ch.html";
	}
	@GetMapping(value="/dhanadeshch/")
	public String dhanadeshch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in dhanadeshch::"+sewarthId);

		return"letter3.html";
	}
	@GetMapping(value="/shaskiy2ch/")
	public String shaskiy2ch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in shaskiy2ch::"+sewarthId);

		return"letter4ch.html";
	}
	@GetMapping(value="/otherch/")
	public String otherch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in otherch::"+sewarthId);

		return"letter5.html";
	}
	@GetMapping(value="/dcpsch/")
	public String dcpsch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in dcpsch::"+sewarthId);

		return"letter6.html";
	}
	@GetMapping(value="/dhanakarshch/")
	public String dhanakarshch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in dhanakarshch::"+sewarthId);

		return"letter7.html";
	}
	@GetMapping(value="/niwasich/")
	public String niwasich(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in niwasich::"+sewarthId);

		return "NiwasiLetter.html";
	}
	@GetMapping(value="/rtich/")
	public String rtich(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in rtich::"+sewarthId);

		return"letter9.html";
	}
	@GetMapping(value="/leaderletterch/")
	public String leaderletterch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in leaderletterch::"+sewarthId);

		return"letter10ch.html";
	}														//लोकायुक्त कार्यालयाकडून प्राप्त टपाल
	@GetMapping(value="/commissionerch/")
	public String commissionerch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in commissionerch::"+sewarthId);

		return"letter11.html";
	}
	@GetMapping(value="/apaleletterch/")
	public String apaleletterch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in apaleletterch::"+sewarthId);

		return"letter12.html";
	}
	@GetMapping(value="/mumbailetterch/")
	public String mumbailetterch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in mumbailetterch::"+sewarthId);

		return"letter13.html";
	}
	@GetMapping(value="/nagpurletterch/")
	public String nagpurletterch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in nagpurletterch::"+sewarthId);
		return"letter14.html";
	}
	@GetMapping(value="/courtch/")
	public String courtch(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController recordOutwardLetterBox ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in courtch::"+sewarthId);

		return "letter15.html";
	}

	@GetMapping(value="/sampleLtr/")
	public String sampleLtr(ModelMap model,@RequestParam("sewarth") String sewarthId,
							@RequestParam("lettertypename") String lettertypename) {
		log.info("Entering into PageController recordOutwardLetterBox ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		model.addAttribute("lettertypename",lettertypename);
		log.info("sewarthId in sampleLtr::"+sewarthId);
		return "commonletters.html";
	}

	@GetMapping(value="/shaskiy/")
	public String shaskiy(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController shaskiy() ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in shaskiy::"+sewarthId);
		log.info("sewarthId in shaskiy::"+sewarthId);

		return"letter2.html";
	}
	@GetMapping(value="/dhanadesh/")
	public String dhanadesh(ModelMap model,@RequestParam("sewarth") String sewarthId,
							@RequestParam("lettertypename") String lettertypename)
	{
		log.info("Entering into PageController dhanadesh ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		model.addAttribute("lettertypename",lettertypename);
		log.info("sewarthId in dhanadesh::"+sewarthId);

		return"ChequeLetters.html";
	}
	@GetMapping(value="/shaskiy2/")
	public String shaskiy2(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController shaskiy2 ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in shaskiy2::"+sewarthId);
		log.info("sewarthId in shaskiy2::"+sewarthId);

		return"letter4.html";
	}
	@GetMapping(value="/other/")
	public String other(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController other ");
        populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in other::"+sewarthId);
		log.info("sewarthId in other::"+sewarthId);

		return"letter5.html";
	}
	@GetMapping(value="/dcps/")
	public String dcps(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController dcps ");

		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in dcps::"+sewarthId);
		log.info("sewarthId in dcps::"+sewarthId);

		return"letter6.html";
	}
	@GetMapping(value="/dhanakarsh/")
	public String dhanakarsh(ModelMap model,@RequestParam("sewarth") String sewarthId,
							 @RequestParam("lettertypename") String letterTypeName)	{
		log.info("Entering into PageController dhanakarsh ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		model.addAttribute("lettertypename",letterTypeName);
		log.info("sewarthId in dhanakarsh::"+sewarthId);
		return"DhanakarshLetter.html";
	}

	@GetMapping(value="/niwasi/")
	public String niwasi(ModelMap model,@RequestParam("sewarth") String sewarthId,
						 @RequestParam("lettertypename") String letterTypeName)	{
		log.info("Entering into PageController niwasi ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		model.addAttribute("lettertypename",letterTypeName);
		log.info("sewarthId in niwasi::"+sewarthId);
		return"NiwasiLetter.html";
	}

	@GetMapping(value="/rti/")
	public String rti(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController rti ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in rti::"+sewarthId);
		log.info("sewarthId in rti::"+sewarthId);

		return"letter9.html";
	}
	@GetMapping(value="/leaderletter/")
	public String leaderletter(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController leaderletter ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in leaderletter::"+sewarthId);
		log.info("sewarthId in leaderletter::"+sewarthId);

		return"letter10.html";
	}
	@GetMapping(value="/commissioner/")
	public String commissioner(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController commissioner ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in commissioner::"+sewarthId);
		log.info("sewarthId in commissioner::"+sewarthId);

		return"letter11.html";
	}
	@GetMapping(value="/apaleletter/")
	public String apaleletter(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController apaleletter ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in sampleLtr::"+sewarthId);
		log.info("sewarthId in apaleletter::"+sewarthId);

		return"letter12.html";
	}

	@GetMapping(value="/mumbaiAndNagpurLetter/")
	public String mumbaiAndNagpurLetter(ModelMap model,@RequestParam("sewarth") String sewarthId,
										@RequestParam("lettertypename") String letterTypeName) {
		log.info("Entering into PageController mumbaiAndNagpurLetter ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		model.addAttribute("lettertypename",letterTypeName);
		log.info("sewarthId in mumbaiAndNagpurLetter::"+sewarthId);
		return "MumbaiAndNagpurLetter.html";
	}

	@GetMapping(value="/court/")
	public String court(ModelMap model,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController court ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		System.out.println("sewarthId in court::"+sewarthId);
		log.info("sewarthId in court::"+sewarthId);

		return "letter15.html";
	}

	/* All letter's DataTable's */

	@GetMapping(value="/sampleLetterTable/")
	public String sampleLetterTable(ModelMap model)
	{
		log.info("Entering into PageController sampleLetterTable ");

		return "letter1DataTable.html";
	}
	@GetMapping(value="/shaskiyLetterTable/")
	public String shaskiyLetterTable(ModelMap model)
	{
		log.info("Entering into PageController shaskiyLetterTable ");


		return "letter2DataTable.html";
	}

	@GetMapping(value="/dhanadeshLetterTable/")
	public String dhanadeshLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dhanadeshLetterTable ");


		return "letter3DataTable.html";
	}

	@GetMapping(value="/shaskiy2LetterTable/")
	public String shaskiy2LetterTable(ModelMap model)
	{
		log.info("Entering into PageController shaskiy2LetterTable ");


		return "letter4DataTable.html";
	}

	@GetMapping(value="/otherLetterTable/")
	public String otherLetterTable(ModelMap model)
	{
		log.info("Entering into PageController otherLetterTable ");


		return "letter5DataTable.html";
	}

	@GetMapping(value="/dcpsLetterTable/")
	public String dcpsLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dcpsLetterTable ");


		return "letter6DataTable.html";
	}

	@GetMapping(value="/dhanakarshLetterTable/")
	public String dhanakarshLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dhanakarshLetterTable ");


		return "letter7DataTable.html";
	}

	@GetMapping(value="/niwasiLetterTable/")
	public String niwasiLetterTable(ModelMap model)
	{
		log.info("Entering into PageController niwasiLetterTable ");


		return "letter8DataTable.html";
	}

	@GetMapping(value="/rtiLetterTable/")
	public String rtiLetterTable(ModelMap model)
	{
		log.info("Entering into PageController rtiLetterTable ");


		return "letter9DataTable.html";
	}

	@GetMapping(value="/leaderLetterTable/")
	public String leaderletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController leaderletterLetterTable ");


		return "letter10DataTable.html";
	}

	@GetMapping(value="/commissionerletterTable/")
	public String commissionerletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController commissionerletterLetterTable ");


		return "letter11DataTable.html";
	}

	@GetMapping(value="/apaleLetterTable/")
	public String apaleletterletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController letterLetterTable ");


		return "letter12DataTable.html";
	}

	@GetMapping(value="/mumbaiLetterTable/")
	public String mumbailetterletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController mumbailetterLetterTable ");


		return "letter13DataTable.html";
	}

	@GetMapping(value="/nagpurLetterTable/")
	public String nagpurletterletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController nagpurletterLetterTable ");


		return "letter14DataTable.html";
	}

	@GetMapping(value="/courtLetterTable/")
	public String courtletterLetterTable(ModelMap model)
	{
		log.info("Entering into PageController courtletterLetterTable ");


		return "letter15DataTable.html";
	}
	@GetMapping(value="/sampleChLetterTable/")
	public String sampleChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController sampleChLetterTable ");


		return "letter1ChDataTable.html";
	}

	@GetMapping(value="/shaskiyChLetterTable/")
	public String shaskiyChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController shaskiyChLetterTable ");


		return "letter2ChDataTable.html";
	}

	@GetMapping(value="/dhanadeshChLetterTable/")
	public String dhanadeshChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dhanadeshChLetterTable ");


		return "letter3ChDataTable.html";
	}

	@GetMapping(value="/shaskiy2ChLetterTable/")
	public String shaskiy2ChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController shaskiy2ChLetterTable ");


		return "letter4ChDataTable.html";
	}

	@GetMapping(value="/otherChLetterTable/")
	public String otherChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController otherChLetterTable ");


		return "letter5ChDataTable.html";
	}

	@GetMapping(value="/dcpsChLetterTable/")
	public String dcpsChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dcpsChLetterTable ");


		return "letter6ChDataTable.html";
	}

	@GetMapping(value="/dhanakarshChLetterTable/")
	public String dhanakarshChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController dhanakarshChLetterTable ");


		return "letter7ChDataTable.html";
	}

	@GetMapping(value="/niwasiChLetterTable/")
	public String niwasiChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController niwasiChLetterTable ");


		return "letter8ChDataTable.html";
	}

	@GetMapping(value="/rtiChLetterTable/")
	public String rtiChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController rtiChLetterTable ");


		return "letter9ChDataTable.html";
	}

	@GetMapping(value="/leaderChLetterTable/")
	public String leaderChLetterTable(ModelMap model)
	{
		log.info("Entering into PageController leaderChLetterTable ");


		return "letter10ChDataTable.html";
	}

	@GetMapping(value="/commissionerChLetterTable/")
	public String commissionerChletterTable(ModelMap model)
	{
		log.info("Entering into PageController commissionerChLetterTable ");


		return "letter11ChDataTable.html";
	}

	@GetMapping(value="/apaleChletterTable/")
	public String apaleChletterTable(ModelMap model)
	{
		log.info("Entering into PageController apaleChletterTable ");


		return "letter12ChDataTable.html";
	}

	@GetMapping(value="/mumbaiChletterTable/")
	public String mumbaiChletterTable(ModelMap model)
	{
		log.info("Entering into PageController mumbaiChletterTable ");


		return "letter13ChDataTable.html";
	}

	@GetMapping(value="/nagpurChletterTable/")
	public String nagpurChletterTable(ModelMap model)
	{
		log.info("Entering into PageController nagpurChletterTable ");


		return "letter14ChDataTable.html";
	}

	@GetMapping(value="/courtChletterTable/")
	public String courtChletterTable(ModelMap model)
	{
		log.info("Entering into PageController courtChletterTable ");
		return "letter15ChDataTable.html";
	}

	@GetMapping(value="/statsReportSearch/")
	public String statReportSearch(ModelMap model, @RequestParam("sewarthId") String sewarthId){
		log.info("Entering into PageController statReportSearch ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in statsReportSearch::"+sewarthId);
		return "StatReports.html";
	}

	@GetMapping(value="/statsReportSearchPAO/")
	public String statReportSearchpao(ModelMap model, @RequestParam("sewarthId") String sewarthId){
		log.info("Entering into PageController statReportSearchPAO ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in statsReportSearch::"+sewarthId);
		return "StatReportsPao.html";
	}


	@GetMapping(value="/office/")
	public String office(ModelMap model)
	{
		log.info("Entering into PageController office ");
		return "sectionoffice.html";
	}
	@GetMapping(value="/inward/")
	public String inward(ModelMap model ,@RequestParam("sewarth") String sewarthId)
	{
		log.info("Entering into PageController inward ");
		populateModelBasedOnUser(authDao,model,sewarthId);
		log.info("sewarthId in inward::"+sewarthId);
		return "inwardRegi.html";
	}

}
