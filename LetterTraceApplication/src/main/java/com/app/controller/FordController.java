package com.app.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.dao.AuthenticationDao;
import com.app.dao.FordProcessDao;
import com.app.dao.LetterProcessDao;
import com.app.model.User;

@Controller
@RequestMapping(value="/app")
public class FordController {

	 private static final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private FordProcessDao letterDao;

    
	@PostMapping(value="/letterDataFord4/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String letterOneDataStoreFord(@RequestBody MultiValueMap<String, String> letterinfo)
	{
		LOGGER.info("Entering into FordController letterOneDataStoreFord()");

		@SuppressWarnings("unused")
		java.sql.Date sqlChkDate=null;
		
		String ltrMedium=letterinfo.getFirst("ltrMedium");
		String ltrType=letterinfo.getFirst("ltrType");
		String hiddenLtrMediumNum=letterinfo.getFirst("hiddenLtrMediumNum");
		//String ltrLocation=letterinfo.getFirst("ltrLocation");
		String ltrOutNo=letterinfo.getFirst("ltrOutNum");
		String ltrDate=letterinfo.getFirst("lDate");
		String ltrSubject=letterinfo.getFirst("lSubject");
		String supplement=letterinfo.getFirst("supplement");
		String toDept=letterinfo.getFirst("ltrToDept");
		//String fromDept=letterinfo.getFirst("ltrRelativeDept");
		String remark=letterinfo.getFirst("remark");
		//String nagpurLtr=letterinfo.getFirst("nagpurLtr");
		//String subLtr=letterinfo.getFirst("subLetter");
		//String chkNumber=letterinfo.getFirst("checkNumber");
		//String chkDate=letterinfo.getFirst("checkDate");
		//String amount=letterinfo.getFirst("amount");
		//String bnkName=letterinfo.getFirst("bankName");
		//String manualLtrMedium=letterinfo.getFirst("manualLtrMedium");
		//String rao =letterinfo.getFirst("raoselect");
		//String mumbaiType =letterinfo.getFirst("mumbaiType");
		//String subMumbai=letterinfo.getFirst("subMumbai");
		//String mumbaiGpf=letterinfo.getFirst("mumbaiGpf");
		//String gpfSubSelect=letterinfo.getFirst("gpfsubselect");
		String ddoNumBandra=letterinfo.getFirst("ddoNumBandra");
		//String phNum=letterinfo.getFirst("phnum");
		String ltrFromDnameMarathi=letterinfo.getFirst("ltrFromDnameMarathi");
		String ltrFromDnameEng=letterinfo.getFirst("ltrFromDnameEng");
		String ltrAccepter=letterinfo.getFirst("ltrAccepter");
		String salgnaShakha=letterinfo.getFirst("salgnaShakha");
		String DdoSection=letterinfo.getFirst("ddobranch");
		
		

//		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
//		System.out.println("data::"+ltrMedium+"/"+hiddenLtrMediumNum+"/"+ltrType+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark+
//				"/"+nagpurLtr+"/"+subLtr+"/"+chkNumber+"/"+chkDate+"/"+amount+"/"+bnkName+"/"+manualLtrMedium+"/"+rao+"/"+mumbaiType+"/"+subMumbai+"/"+mumbaiGpf+"/"+gpfSubSelect+"/"+ddoNumBandra+"/"+ddoNumBandra+"/"+phNum+"/"+ltrFromDnameMarathi+"/"+ltrFromDnameEng+"/"+ltrAccepter+"/"+salgnaShakha+"/"+DdoSection);
//			
		try {
			
			Date myDate=new SimpleDateFormat("dd/MM/yyyy").parse(ltrDate);
			java.sql.Date sqlLDate = new java.sql.Date(myDate.getTime());
			System.out.println("SqlDate:"+sqlLDate);
			
			LOGGER.info("Get myDate::"+sqlLDate);

			
			/*
			 
			if(chkDate!=null && !chkDate.isEmpty())
			{
				System.out.println("chkDate::"+chkDate);
				DateFormat formatters  = new SimpleDateFormat("yyyy-MM-dd");
				Date chDate=formatter.parse(chkDate);
				sqlChkDate= new java.sql.Date(myDate.getTime());
			}
			*/
			
			 int Ltrcount=letterDao.getLtrCountFord("lettertypeone");
			String inwardStr="PAO/"+toDept+"/"+ltrMedium+"/"+ltrType+"/"+(Ltrcount+1)+"/"+ltrDate+"";
			               System.out.println("InwardStr::"+inwardStr);
			boolean supp=Boolean.parseBoolean(supplement);
			int rowAffect=letterDao.storeLetterOneDataFord(ltrMedium,hiddenLtrMediumNum,ltrType,ltrOutNo,sqlLDate,ltrSubject,supp,salgnaShakha,ltrAccepter,remark,toDept,ddoNumBandra,ltrFromDnameMarathi,ltrFromDnameEng,DdoSection);
			if(rowAffect>0)
			{
				return "dashboardAdminFord";
			}
			System.out.println("Result::"+rowAffect);
			
			LOGGER.info("Result::"+rowAffect);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
			LOGGER.info("Excpetion::"+e);

			e.printStackTrace();
		}
		
		LOGGER.info("Exit from FordController letterOneDataStoreFord()");

		
		return "page-error-400";
	}
	
	@PostMapping(value="/letterDataFord1/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String letterTwoDataStoreFord(@RequestBody MultiValueMap<String, String> letterinfo)
	{
		LOGGER.info("Entering into FordController letterTwoDataStoreFord()");

		
		java.sql.Date sqlLDate=null;
		java.sql.Date sqlLChkDate=null;
		
		String ltrMedium=letterinfo.getFirst("ltrMedium");
		String ltrType=letterinfo.getFirst("ltrType");
		String hiddenLtrMediumNum=letterinfo.getFirst("hiddenLtrMediumNum");
	//	String ltrLocation=letterinfo.getFirst("ltrLocation");
		String ltrOutNo=letterinfo.getFirst("ltrOutNum");
		String ltrDate=letterinfo.getFirst("lDate");
		String ltrSubject=letterinfo.getFirst("lSubject");
		String supplement=letterinfo.getFirst("supplement");
		String toDept=letterinfo.getFirst("ltrToDept");
	//	String fromDept=letterinfo.getFirst("ltrRelativeDept");
		String remark=letterinfo.getFirst("remark");
		String chkNumber=letterinfo.getFirst("checkNumber");
		String chkDate=letterinfo.getFirst("checkDate");
		String amount=letterinfo.getFirst("amount");
	//	String manualLtrMedium=letterinfo.getFirst("manualLtrMedium");
		String ltrAccepter=letterinfo.getFirst("ltrAccepter");
		String salgnaShakha=letterinfo.getFirst("salgnaShakha");
		
		

		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
		System.out.println("data::"+ltrMedium+"/"+hiddenLtrMediumNum+"/"+ltrType+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+remark+
				"/"+chkNumber+"/"+chkDate+"/"+amount+"/"+ltrAccepter+"/"+salgnaShakha);
			
		try {
			
			if(ltrDate!=null && !ltrDate.isEmpty())
			{
			  Date myDate=new SimpleDateFormat("dd/MM/yyyy").parse(ltrDate);
			  sqlLDate= new java.sql.Date(myDate.getTime());
			  System.out.println("SqlDate:"+sqlLDate);
			  
				LOGGER.info("SqlDate"+sqlLDate);

			  
			}
			if(chkDate!=null && !chkDate.isEmpty())
			{
				Date myChkDate=new SimpleDateFormat("dd/MM/yyyy").parse(chkDate);
				sqlLChkDate= new java.sql.Date(myChkDate.getTime());
			 System.out.println("SqlDate:"+sqlLChkDate);
			}
            boolean supp=Boolean.parseBoolean(supplement);
            
            int Ltrcount=letterDao.getLtrCountFord("lettertypetwo");
			String inwardStr="PAO/"+toDept+"/"+ltrMedium+"/"+ltrType+"/"+(Ltrcount+1)+"/"+ltrDate+"";
			               System.out.println("InwardStr::"+inwardStr);
			               
			int rowAffect=letterDao.storeLetterTwoDataFord(ltrMedium,hiddenLtrMediumNum,ltrType,ltrOutNo,sqlLDate,ltrSubject,supp,salgnaShakha,ltrAccepter,remark,toDept,chkNumber,sqlLChkDate,amount,inwardStr);
			if(rowAffect>0)
			{
				return "dashboardAdminFord";
			}
			System.out.println("Result::"+rowAffect);
			
			LOGGER.info("Result"+rowAffect);

			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOGGER.info("Exit from FordController letterTwoDataStoreFord()");

		return "page-error-400";
	}
	@PostMapping(value="/letterDataFord2/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String letterThreeDataStoreFord(@RequestBody MultiValueMap<String, String> letterinfo)
	{
		LOGGER.info("Entering into FordController letterThreeDataStoreFord()");

		java.sql.Date sqlLDate=null;
		java.sql.Date sqlLChkDate=null;
		
		String ltrMedium=letterinfo.getFirst("ltrMedium");
		String ltrType=letterinfo.getFirst("ltrType");
		String hiddenLtrMediumNum=letterinfo.getFirst("hiddenLtrMediumNum");
	//	String ltrLocation=letterinfo.getFirst("ltrLocation");
		String ltrOutNo=letterinfo.getFirst("ltrOutNum");
		String ltrDate=letterinfo.getFirst("lDate");
		String ltrSubject=letterinfo.getFirst("lSubject");
		String supplement=letterinfo.getFirst("supplement");
		String toDept=letterinfo.getFirst("ltrToDept");
	//	String fromDept=letterinfo.getFirst("ltrRelativeDept");
		String remark=letterinfo.getFirst("remark");
		String chkNumber=letterinfo.getFirst("checkNumber");
		String chkDate=letterinfo.getFirst("checkDate");
		String bankName=letterinfo.getFirst("bankName");
	//	String manualLtrMedium=letterinfo.getFirst("manualLtrMedium");
		String ltrAccepter=letterinfo.getFirst("ltrAccepter");
		String salgnaShakha=letterinfo.getFirst("salgnaShakha");
		
		

		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
		System.out.println("data::"+ltrMedium+"/"+hiddenLtrMediumNum+"/"+ltrType+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+remark+
				"/"+chkNumber+"/"+chkDate+"/"+bankName+"/"+ltrAccepter+"/"+salgnaShakha);
			
		try {
			
			if(ltrDate!=null && !ltrDate.isEmpty())
			{
			  Date myDate=new SimpleDateFormat("dd/MM/yyyy").parse(ltrDate);
			  sqlLDate= new java.sql.Date(myDate.getTime());
			  System.out.println("SqlDate:"+sqlLDate);
			}
			if(chkDate!=null && !chkDate.isEmpty())
			{
				Date myChkDate=new SimpleDateFormat("dd/MM/yyyy").parse(chkDate);
				sqlLChkDate= new java.sql.Date(myChkDate.getTime());
			 System.out.println("SqlDate:"+sqlLChkDate);
			}
			boolean supp=Boolean.parseBoolean(supplement);
			
			int Ltrcount=letterDao.getLtrCountFord("lettertypetwo");
			String inwardStr="PAO/"+toDept+"/"+ltrMedium+"/"+ltrType+"/"+(Ltrcount+1)+"/"+ltrDate+"";
			               System.out.println("InwardStr::"+inwardStr);
			int rowAffect=letterDao.storeLetterThreeDataFord(ltrMedium,hiddenLtrMediumNum,ltrType,ltrOutNo,sqlLDate,ltrSubject,supp,salgnaShakha,ltrAccepter,remark,toDept,chkNumber,sqlLChkDate,bankName,inwardStr);
			if(rowAffect>0)
			{
				return "dashboardAdminFord";
			}
			System.out.println("Rresult::"+rowAffect);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOGGER.info("Exit from FordController letterThreeDataStoreFord()");

		return "page-error-400";
	}
	@PostMapping(value="/letterDataFord5/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String letterFiveDataStoreFord(@RequestBody MultiValueMap<String, String> letterinfo)
	{
		LOGGER.info("Entering into FordController letterFiveDataStoreFord()");

		java.sql.Date sqlLDate=null;
		java.sql.Date sqlLChkDate=null;
		
		String ltrMedium=letterinfo.getFirst("ltrMedium");
		String ltrType=letterinfo.getFirst("ltrType");
		String hiddenLtrMediumNum=letterinfo.getFirst("hiddenLtrMediumNum");
		String ltrFrom=letterinfo.getFirst("ltrFrom");
		String mobile=letterinfo.getFirst("mobile");
		String ltrOutNo=letterinfo.getFirst("ltrOutNum");
		String ltrDate=letterinfo.getFirst("lDate");
		String ltrSubject=letterinfo.getFirst("lSubject");
		String supplement=letterinfo.getFirst("supplement");
		String toDept=letterinfo.getFirst("ltrToDept");
		String remark=letterinfo.getFirst("remark");
	//	String manualLtrMedium=letterinfo.getFirst("manualLtrMedium");
		String ltrAccepter=letterinfo.getFirst("ltrAccepter");
		String salgnaShakha=letterinfo.getFirst("salgnaShakha");
		
		///System.out.println("data::"+ltrMedium+"/"+ltrType+"/"+ltrLocation+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+fromDept+"/"+remark);
		System.out.println("data::"+ltrMedium+"/"+hiddenLtrMediumNum+"/"+ltrType+"/"+ltrOutNo+"/"+ltrDate+"/"+ltrSubject+"/"+supplement+"/"+toDept+"/"+remark+
				"/"+ltrAccepter+"/"+salgnaShakha+"/"+ltrFrom+"/"+mobile);	
		try {
			
			if(ltrDate!=null && !ltrDate.isEmpty())
			{
			  Date myDate=new SimpleDateFormat("dd/MM/yyyy").parse(ltrDate);
			  sqlLDate= new java.sql.Date(myDate.getTime());
			  System.out.println("SqlDate:"+sqlLDate);
			}
		
			boolean supp=Boolean.parseBoolean(supplement);
			int rowAffect=letterDao.storeLetterFiveDataFord(ltrMedium,hiddenLtrMediumNum,ltrType,ltrOutNo,sqlLDate,ltrSubject,supp,salgnaShakha,ltrAccepter,remark,toDept,mobile,ltrFrom);
			if(rowAffect>0)
			{
				return "dashboardAdminFord";
			}
			System.out.println("Rresult::"+rowAffect);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOGGER.info("Exit from FordController letterFiveDataStoreFord()");
		return "page-error-400";

	}
	
	@GetMapping(value="/ltrdataFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<LinkedHashMap>> fetchletterInfoFord()
	{
		LOGGER.info("Entering into FordController fetchletterInfoFord()");

		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<LinkedHashMap> datalist=letterDao.fetchAlldataFord();
		for(int i=0;i<datalist.size();i++)   
		{
			ldata=datalist.get(i);
			System.out.println("data--1"+ldata);
		}
		
		LOGGER.info("exit from FordController fetchletterInfoFord()");

		return new ResponseEntity<List<LinkedHashMap>>(datalist, HttpStatus.OK);
	}
	
	@GetMapping(value="/empdataFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchEmpInfoFord(@RequestParam("deptId") String deptId)
	{
		LOGGER.info("Entering into FordController fetchEmpInfoFord()");

		Map<String,Object> ldataMap=new LinkedHashMap<>();
		List<Map> lDataList=new ArrayList<>();
		List<String> divList=new ArrayList();
		System.out.println("deptId"+deptId);
		String empdata=null;
		
		
		divList.add("Answerable");
		divList.add("Non-Answerable");
		divList.add("Informative");
	    List<String> empdatalist=letterDao.fetchEmpdataFord(deptId);
	    int count=letterDao.getTotalLtrCountFord();
	    System.out.println("count::"+count);
	    
		for(int i=0;i<empdatalist.size();i++)
		{
			empdata=empdatalist.get(i);
			System.out.println("data"+empdata);
		}
		 List<LinkedHashMap> datalist=letterDao.fetchAlldataFord();
			for(int i=0;i<datalist.size();i++)
			{
				ldataMap=datalist.get(i);
				System.out.println("data"+ldataMap);
				ldataMap.put("empList",empdatalist);
				ldataMap.put("divList",divList);
				lDataList.add(ldataMap);
			}
		System.out.println("FinalThirdAdminList:"+lDataList);
		
		LOGGER.info("Entering into FordController FinalThirdAdminList::"+lDataList);
 
		return new ResponseEntity<List<Map>>(lDataList, HttpStatus.OK);
	}
	
	@GetMapping(value="/ltrddodataFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchletterddoInfoFord()
	{
		LOGGER.info("Entering into FordController fetchletterddoInfoFord()");

		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<Map> datalist=letterDao.fetchddoListFord();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			//System.out.println("data"+ldata);
		}
		
		LOGGER.info("Exit from FordController fetchletterddoInfoFord()" );

		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	@GetMapping(value="/ltrdatatwoFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchltrdataInfoTwoFord()
	{
		LOGGER.info("Entering into FordController fetchltrdataInfoTwoFord()");

		
		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<Map> datalist=letterDao.fetchltrdatatwoFord();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			//System.out.println("data"+ldata);
		}
		LOGGER.info("Exit from FordController fetchltrdataInfoTwoFord()" );

		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
	@GetMapping(value="/ltrdatathreeFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchltrdataInfoThreeFord()
	{
		LOGGER.info("Entering into FordController fetchltrdataInfoThreeFord()");
		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<Map> datalist=letterDao.fetchltrdataThreeFord();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			//System.out.println("data three"+ldata);
		}
		LOGGER.info("Exit from FordController fetchltrdataInfoThreeFord()" );

		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	@GetMapping(value="/ltrdatafourFord/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchltrdataInfoFourFord()
	{
		LOGGER.info("Entering into FordController fetchltrdataInfoFourFord()");
		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<Map> datalist=letterDao.fetchltrdataFourFord();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			System.out.println("data four"+ldata);
		}
		LOGGER.info("Exit from FordController fetchltrdataInfoFourFord()" );

		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
}
