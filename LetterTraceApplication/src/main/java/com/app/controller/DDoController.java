package com.app.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.dao.DDoAdminDao;


@Controller
@RequestMapping(value="/app")
public class DDoController {
	
	private static final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private DDoAdminDao ddoAdminDao;
	
	
	@PostMapping(value="/ddoadmin/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String DDoDataStore(@RequestBody MultiValueMap<String, String> ddoinfo)
	{
		LOGGER.info("Entering into DDoController DDoController()");
		@SuppressWarnings("unused")
		
	
		String ltrdeptName=ddoinfo.getFirst("ltrdeptName");
		String senderofficeName=ddoinfo.getFirst("senderofficeName");
		String ddonumber=ddoinfo.getFirst("ddonumber");
		String ddosection=ddoinfo.getFirst("ddosection");
		
		int rowAffect=ddoAdminDao.storeDDoData(ltrdeptName,senderofficeName,ddonumber,ddosection);
		
		
		if(rowAffect>0)
		{
			LOGGER.info("Exit from DDoController DDoController()");
			return "ddoAdmin";
		}
		
		LOGGER.info("Exit from DDoController DDoController()");
		return "page-error-400";
	}
	
	
	@PostMapping(value="/bankadmin/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String BankDataStore(@RequestBody MultiValueMap<String, String> bankinfo)
	{
		@SuppressWarnings("unused")
		
	
		String bankName=bankinfo.getFirst("bankNames");
		System.out.println(bankName);
		int rowAffect=ddoAdminDao.storeBankData(bankName);
		
		
		if(rowAffect>0)
		{
			return "ddoAdmin";
		}
		return "page-error-400";
	}
	
	
	@PostMapping(value="/bankbranch/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String BankBranchStore(@RequestBody MultiValueMap<String, String> bankbranchinfo)
	{
		@SuppressWarnings("unused")
		
	
		
		String bankBranchName=bankbranchinfo.getFirst("bankBranchName");
		String bankid=bankbranchinfo.getFirst("bankidhidden");
		
		
		int bankidInt = Integer.parseInt(bankid);
		int rowAffect=ddoAdminDao.storeBankBranchData(bankBranchName,bankidInt);
		
		
		if(rowAffect>0)
		{
			return "ddoAdmin";
		}
		return "page-error-400";
	}
	
	
	@GetMapping(value="/bankNames/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchBankNames()
	{
		Map<String,Object> ldata=new LinkedHashMap<>();
	  List<Map> datalist=ddoAdminDao.fetchBank();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			//System.out.println("data"+ldata);
		}
		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
	/*@GetMapping(value="/bankidone/")
	@ResponseBody
	public ResponseEntity<String> getBranchList(@RequestParam("bankID") String bankId)
	{
		
		// int rowAffect=ddoAdminDao.storeBankid(bankId);
				System.out.println("BankID::"+bankId);
			
		 return new ResponseEntity<String>(bankId, HttpStatus.OK);
          
	}*/
	

}
