package com.app.controller;

import com.app.dao.*;
import com.app.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value="/auditor")
public class AuditorController extends BaseController {

    @Autowired
    private WorkflowDao workflowDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @Autowired
    JournalDao journalDao;

    @Autowired
    private DDoAdminDao ddoAdminDao;

    @Autowired
    private BankAdminDao bankAdminDao;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private UserDao userDao;

    @GetMapping(value="/getletters/")
    @ResponseBody
    public  ResponseEntity<List<WorkflowVO>> getAssignedLtrList(@RequestParam("sewrathId") String sevarthId,@RequestParam("ltrStatus") String ltrStatus){
        log.info("AuditorController ::getAssignedLtrList()");
        List<WorkflowVO> ltrWorkflowList;
        StringBuffer whereClause = new StringBuffer("");

        if ("pending".equalsIgnoreCase(ltrStatus)) {
            whereClause.append("toUser='" + sevarthId + "' and isAssigned=true and ltrStatus in ( 'accepted', 'rejected') ");
        }
        else if ("completed".equalsIgnoreCase(ltrStatus)) {
            whereClause.append("toUser='" + sevarthId + "' and isAssigned=true and added_tz > DATE_SUB(NOW(), INTERVAL 7 DAY) and ltrStatus in ( 'completed', 'outwarded')  ");
        }
        else {
            whereClause.append("toUser='" + sevarthId + "' and isAssigned=true and ltrStatus='" + ltrStatus + "'");
        }

        ltrWorkflowList=workflowDao.getLtrWorkflowList(whereClause.toString());

    for(WorkflowVO workflowVO:ltrWorkflowList) {
        List<BaseVO> baseVOList=baseDao.getLetterDispalyList("ltrInwardNum='"+workflowVO.getLtrInwardNum()+"'");
            List<ReferenceData> referenceDataList=referenceDataDao.getReferenceData("id="+workflowVO.getLtrCategory()+"");
            if(!referenceDataList.isEmpty()){
                ReferenceData referenceData=referenceDataList.get(0);
                workflowVO.setLtrCategoryName(referenceData.getEntityValue());
            }
            if(!baseVOList.isEmpty()){
                BaseVO baseVO=baseVOList.get(0);
                workflowVO.setLtrSenderNameInMarathi(baseVO.getLtrSenderMarathi());

                if ("in_progress".equalsIgnoreCase(ltrStatus)) {
                    try {
                        java.util.Date dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(baseVO.getAdded_tz().toString());
                        workflowVO.setLtrCreateDate(new Date(dt.getTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return new ResponseEntity<List<WorkflowVO>>(ltrWorkflowList, HttpStatus.OK);
    }


    @GetMapping(value="/getroutedata")
    @ResponseBody
    public  ResponseEntity<HashMap<String,Object>> getRouteReferenceData( @RequestParam("sewarthId")String sewarthId){
        log.info("AuditorController ::getRouteReferenceData()");
        HashMap<String,Object> routeRefDataMap = new HashMap<>();

        List<ReferenceData> listSubDept = referenceDataDao.getReferenceData( "entity_key = 'sub_department' and entity_value not in ('अधिकारी')");
        routeRefDataMap.put("listSubDept", listSubDept);

        User user = userDao.getUserList("sevarth_id = '" + sewarthId + "'").get(0);
        routeRefDataMap.put("userNondaniShakha", user.getNondaniShakha());

        ReferenceData nondani = referenceDataDao.getReferenceData(
                "entity_key = 'sub_department' and entity_value = '" + user.getNondaniShakha() +"'").get(0);
        routeRefDataMap.put("userNondaniShakhaId", nondani.getId());

        List<DDO> listDDOAdmin = ddoAdminDao.getDDOData( "");
        routeRefDataMap.put("listDDOAdmin", listDDOAdmin);

        List<Bank> listBanks = bankAdminDao.getBankData("");
        routeRefDataMap.put("listBanks", listBanks);


        return new ResponseEntity<>(routeRefDataMap, HttpStatus.OK);
    }


    @PostMapping(value="/postaduditerdata" ,consumes="application/json;charset=UTF-8")
    public ResponseEntity<String> postLetters(@RequestBody WorkflowVO workflowVO, @RequestParam("sewrathId")String sevarthId,
                                              @RequestParam("ltrStatus")String ltrStatus)throws Exception {
        log.info("AuditorController :: postLetters()");

        List<UserMapping> userMappingList;
        String data = "";
        log.info("data::"+workflowVO);

        if(ltrStatus.equalsIgnoreCase("rejected") || ltrStatus.equalsIgnoreCase("processed") ){
            userMappingList = userMappingDao.getUserMapping("ad='" + sevarthId + "'");
            if (userMappingList != null) {
                UserMapping userMapping = userMappingList.get(0);
                workflowVO.setToUser(userMapping.getSo());
            }
        }else{
            workflowVO.setToUser(sevarthId);
        }
        workflowVO.setFromUser(sevarthId);
        workflowVO.setLtrStatus(ltrStatus);
        workflowVO.setAdded_by(sevarthId);
        workflowVO.setUpdate_by(sevarthId);
        workflowVO.setAssigned(true);

        int rowAffect=workflowDao.addNewWorkflow(workflowVO);
        if(rowAffect>0)
        {
            return ResponseEntity.ok(data);
        }

        return ResponseEntity.badRequest().body("Error");
    }

    @GetMapping(value="/karyavivarannondwahi")
    @ResponseBody
    public List<HashMap<String,Object>> karyaVivaranNondWahi(@RequestParam("sewarthId")String sewarthId) throws Exception {
        log.info("AuditorController :: karyaVivaranNondWahi for : " + sewarthId);
        return journalDao.karyaVivaranNondWahi(sewarthId);
    }

}
