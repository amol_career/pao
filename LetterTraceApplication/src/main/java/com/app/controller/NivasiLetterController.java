package com.app.controller;

import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.WorkflowVO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Calendar;
import java.util.Map;

@Controller
@RequestMapping(value="/app")
public class NivasiLetterController extends BaseLetterController {

    @Override
    @PostMapping(value="/nivasiLetters/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    protected String saveLetter(ModelMap model,@RequestBody MultiValueMap<String, String> letterInfo) {
        try {
            String lettertypeNamehidden = letterInfo.getFirst("lettertypeName");
            String sewarthId=letterInfo.getFirst("sevarthId");
            String nondaniShakha = letterInfo.getFirst("nondaniShakha");
            String deptName = letterInfo.getFirst("salgnaShakha");
            String ltrSubjectName = letterInfo.getFirst("ltrSubjectName");
            
            String lettertypename=letterInfo.getFirst("lettertypeName");
            String employeeName=letterInfo.getFirst("employeeName");
            String employeebranch=letterInfo.getFirst("employeebranch");
            String success ="Your data is submmited Successfully";
            
            log.info("***** lettertypeNamehidden::"+lettertypeNamehidden);
            log.info("***** sewarthId::"+sewarthId);
            log.info("***** nondaniShakha::"+nondaniShakha);
            log.info("***** deptName::"+deptName);
            log.info("***** ltrSubjectName::"+ltrSubjectName);

            ReferenceData ltrReferenceData = authenticationDao.getReferenceDataByKeyAndValue("letter_type",lettertypeNamehidden);
            ReferenceData deptReferenceData = authenticationDao.getReferenceDataByKeyAndValue("sub_department",deptName);
            if(ltrReferenceData == null)
                log.warn("Reference data is not available for letter  "+ lettertypeNamehidden);
            if(deptReferenceData == null)
                log.warn("Reference data is not available for department  "+ deptName);

            Map<String,Integer> counts = commonLetterDao.getIndividualAndAllLetterCount(ltrReferenceData.getId(),nondaniShakha);

            log.info("Collected letter counts for inward number ::  "+counts);
            Integer individualLtrCount = 0;
            Integer allLtrCount = 0;

            if(counts.get("IndividualLtrCount")!=null){
                individualLtrCount = counts.get("IndividualLtrCount").intValue();
            }

            if(counts.get("AllLetterCount")!=null){
                allLtrCount = counts.get("AllLetterCount").intValue();
            }

            String inwardNo = generateInwardNumber(sewarthId,nondaniShakha,ltrReferenceData.getEntityValue(),
                    individualLtrCount,allLtrCount);

            log.info("Inward Id has been prepared..."+inwardNo);
            BaseVO vo = populateBaseVOFromModel(letterInfo,sewarthId,
                    inwardNo, nondaniShakha,individualLtrCount+1,ltrReferenceData.getId(),
                    deptReferenceData.getId());

            String toUser = commonLetterDao.getSewartIdForWorkflowNo(vo.getLtrToSubDept(),vo.getLtrAcceptor());

            WorkflowVO workflowVO = populateWorkflowVO(vo.getLtrToSubDept(),sewarthId,toUser,
                    vo.getLtrInwardNum(),vo.getLtrSubject(),ltrReferenceData.getId(),
                    vo.getLtrDate());

            int rowAffect = commonLetterDao.storeData(vo, workflowVO);

            if(rowAffect>0)	{
            	model.addAttribute("InwardString",inwardNo);
                model.addAttribute("employeebranch",employeebranch);
                model.addAttribute("employeeName",employeeName);
                model.addAttribute("sewarthIdC",sewarthId);
                model.addAttribute("lettertypenameC",lettertypename);
                model.addAttribute("nondaniShakhaByC",nondaniShakha);
                model.addAttribute("successMsg",success+lettertypename);
                log.info("Niwasi Letter has been stores successfully");
                return "NiwasiLetter";
            }
        } catch (Exception e) {
            log.error("Error occured while storing letter. :: NivasiLetterController :: Error - " + e.getMessage());
        }
        return "page-error-400";
    }

}
