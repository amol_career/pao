package com.app.controller;

import com.app.dao.AuthenticationDao;
import com.app.dao.ReferenceDataDao;
import com.app.dao.UserMappingDao;
import com.app.dao.WorkflowDao;
import com.app.model.LetterReport;
import com.app.model.ReferenceData;
import com.app.model.StatsReport;
import com.app.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Controller
@RequestMapping(value="/app")
public class ReportController extends BaseController {

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private WorkflowDao workflowDao;

    @Autowired
    AuthenticationDao authenticationDao;

      /* @RequestMapping(value="/searchltrStatus11/",method=RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
       public ResponseEntity<List<StatsReport>> getReportStats(@RequestParam("sevarthId") String sewarthId,
                                                               @RequestParam("date") String date) throws Exception {
        log.info("ReportController :: getReportStats() :: sewarthId:"+sewarthId+ " :: Date :" + date);
        List<StatsReport> statReports = null;
        StatsReport report = null;
        Map<StatsReport,List<String>> result = null;
        Map<String,Integer> stats = null;
        try{
            if(sewarthId != null){
                   // Get from & to date from UI and assign to below fromDate & toDate
                    String fromDate = null;
                    String toDate = null;
                    statReports = new ArrayList<>();
                    User user = authenticationDao.getUser(sewarthId);
                    log.info("User :" + user);
                    if(user != null){
                            List<ReferenceData> referenceData = referenceDataDao.getReferenceData("entity_key='role'");
                            String roleName = getRoleName(referenceData,user);
                            log.info("Got role name :["+  roleName +"] for role id :"+ user.getRoleId());
                            List<Map<String,Object>> userMappings = userMappingDao.getUserMappingForReport(roleName,
                                                                                             getYourReporty(roleName),sewarthId);
                            if(userMappings!=null && userMappings.size()>0){
                                log.info("User mapping for reporty : "+userMappings.toString());
                                result = new HashMap<>();
                                //prepare reporty list and its AD's list
                                for(Map<String,Object> map : userMappings){
                                    report = new StatsReport();
                                    report.setSewarthId(map.get("reporty").toString());
                                    report.setEmplName(map.get("reporty_name").toString());
                                    if(result.containsKey(report)){
                                        List adList = result.get(report);
                                        adList.add(map.get("ad").toString());
                                    }else{
                                        List<String> adList = new ArrayList<>();
                                        adList.add(map.get("ad").toString());
                                        result.put(report,adList);
                                    }
                                }
                                //Prepare letter count for all AD's under each reporty
                                for(StatsReport statsReport : result.keySet()){
                                    List<String> adList = result.get(statsReport);
                                    List<Map<String,Object>> ltrCounts = workflowDao.getInwardNoAndStatus(adList,
                                            getFormattedDateForReport(fromDate,true),
                                            getFormattedDateForReport(toDate,false));
                                    if(ltrCounts != null && ltrCounts.size()>0){
                                        log.info("Got letter counts for AD's "+ adList +"::"+ltrCounts.toString());
                                        stats = new HashMap<>();
                                        for(Map<String,Object> counts : ltrCounts){
                                            String status = counts.get("ltrStatus").toString();
                                            if(stats.containsKey(status)){
                                                stats.put(status,stats.get(status).intValue()+1);
                                            }else{
                                                stats.put(status,1);
                                            }
                                        }
                                        log.info("Letter status map has been prepared :: "+stats);
                                        if(stats.get("completed")!=null)
                                            statsReport.setLtrCompletedCount(stats.get("completed"));
                                        if(stats.get("in_progress")!=null)
                                            statsReport.setLtrInProgressCount(stats.get("in_progress"));
                                        if(stats.get("new")!=null)
                                            statsReport.setLtrNewCount(stats.get("new"));
                                        if(stats.get("rejected")!=null)
                                            statsReport.setLtrRejectedCount(stats.get("rejected"));
                                    }
                                    statReports.add(statsReport);
                                }
                            }
                        log.info("Report list has been prepared ::"+ statReports);
                    }else{
                        throw new Exception("User should be configure in DB.");
                    }
            }else{
                throw new Exception("Sewarth id should pass while calculating reports.");
            }
        }catch(Exception e){
            log.error("Error occurred while generating stat report for Sewarth id: "+ sewarthId +" Error::"+e.getMessage());
        }
       return new ResponseEntity<List<StatsReport>>(statReports, HttpStatus.OK);
    }*/


    // @RequestMapping(value="/searchltrStatus/",method=RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
    @GetMapping(value="/searchltrStatus/")
    @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getReportStats(@RequestParam("sevarthId") String sewarthId,
                                                                 @RequestParam("fromDate") String fromDate,
                                                                 @RequestParam("toDate") String toDate) throws Exception {
        HashMap<String,Object> reportMap = new HashMap<>();
       // Report report = null;
        User user = null;
        List<ReferenceData> listReferenceData = null;
        ReferenceData roleReferenceData = null;
        String roleNameAsColumnName = null;
        List<Map<String,Object>> userMappings = null;
        Map<String,Integer> stats = null;
        List<StatsReport> statReports = null;
        //log.info("ReportController :: getReportStats1() :: sewarthId:"+sewarthId);
        try{
            // String sewarthId ="DATJSGF7101";//"DATNAPM6501";
            //String sewarthId ="DATNAPM6501";
            //String date = null;
            if(sewarthId != null){
                // Get from & to date from UI and assign to below fromDate & toDate
                fromDate = null;
                toDate = null;
                //report = new Report();
                user = authenticationDao.getUser(sewarthId);
                log.info("User ["+ user + "] has been fetched for sewarth id "+sewarthId);
                if(user !=null){
                    listReferenceData = referenceDataDao.getReferenceData("entity_key='role'");
                    roleReferenceData = getRoleReferenceData(listReferenceData,user);
                    log.info("Reference data for role fetched :"+ roleReferenceData);
                    roleNameAsColumnName = getRollMappingForColumnName(roleReferenceData.getEntityValue());

                    if(isUserRoleIsAD(user,roleReferenceData)){
                        log.info("Provided user role validated as : AD");
                        List<LetterReport> letterReportList = new ArrayList<>();
                        List<String> ad = new ArrayList<>();
                        LetterReport letterReport = null;
                        ad.add(sewarthId);
                        List<Map<String,Object>> ltrCounts = workflowDao.getInwardNoAndStatusForAD(ad,
                                getFormattedDateForReport(fromDate,true),
                                getFormattedDateForReport(toDate,false));
                        log.info("Letter count for AD has been fetched ::" + ltrCounts);
                        if(ltrCounts!=null && ltrCounts.size() > 0){
                            Map<LetterReport,Map<String,Integer>> tempMapForStates = new HashMap<>();
                            for(Map<String,Object> record : ltrCounts){
                                letterReport = new LetterReport();
                                letterReport.setLtrId((Integer)record.get("id"));
                                letterReport.setLtrType(record.get("ltrType").toString());
                                if(tempMapForStates.containsKey(letterReport)){
                                    tempMapForStates.get(letterReport).put(record.get("ltrStatus").toString(),((Long)record.get("count")).intValue());
                                }else{
                                    HashMap<String,Integer> tempMap = new HashMap();
                                    tempMap.put(record.get("ltrStatus").toString(),((Long)record.get("count")).intValue());
                                    tempMapForStates.put(letterReport,tempMap);
                                }
                            }
                            log.info("Partial letter report has been prepared ::"+ tempMapForStates);
                            for(LetterReport ltrReport : tempMapForStates.keySet()){
                                Map<String,Integer> tempMap = tempMapForStates.get(ltrReport);
                                if(tempMap.get("completed")!=null)
                                    ltrReport.setCompletedLetters(tempMap.get("completed"));
                                if(tempMap.get("in_progress")!=null)
                                    ltrReport.setInProgressLetters(tempMap.get("in_progress"));
                                if(tempMap.get("new")!=null)
                                    ltrReport.setNewLetters(tempMap.get("new"));
                                if(tempMap.get("rejected")!=null)
                                    ltrReport.setRejectedLetters(tempMap.get("rejected"));
                                letterReportList.add(ltrReport);
                            }
                        }else{
                            log.info("Letter count received as empty for "+ ad);
                        }
                        log.info("Letter reports for all letter has been generated ::"+letterReportList);
                        //report.setLetterReportList(populateLetterName(letterReportList));
                        reportMap.put("ltrReport", populateLetterName(letterReportList));
                    }else{
                        log.info("Provided user role validated as : other than AD");
                        userMappings = userMappingDao.getUserMappingForReport(roleNameAsColumnName,
                                getYourReporty(roleNameAsColumnName),sewarthId);
                        if(userMappings!=null && userMappings.size()>0){
                            log.info("User mapping for reporty : "+userMappings.toString());
                            Map<StatsReport,List<String>> tempMapForStates = new HashMap<>();
                            statReports = new ArrayList<>();
                            StatsReport statRport = null;
                            //prepare reporty list and its AD's list
                            for(Map<String,Object> map : userMappings){
                                statRport = new StatsReport();
                                statRport.setSewarthId(map.get("reporty").toString());
                                statRport.setEmplName(map.get("reporty_name").toString());
                                if(tempMapForStates.containsKey(statRport)){
                                    List adList = tempMapForStates.get(statRport);
                                    adList.add(map.get("ad").toString());
                                }else{
                                    List<String> adList = new ArrayList<>();
                                    adList.add(map.get("ad").toString());
                                    tempMapForStates.put(statRport,adList);
                                }
                            }
                            //Prepare letter count for all AD's under each reporty
                            for(StatsReport statsReport : tempMapForStates.keySet()){
                                List<String> adList = tempMapForStates.get(statsReport);
                                log.info("Going to fetch letter count for ad's" + adList);
                                List<Map<String,Object>> ltrCounts = workflowDao.getInwardNoAndStatus(adList,
                                        getFormattedDateForReport(fromDate,true),
                                        getFormattedDateForReport(toDate,false));
                                if(ltrCounts != null && ltrCounts.size()>0){
                                    log.info("Got letter counts for AD's "+ adList +"::"+ltrCounts.toString());
                                    stats = new HashMap<>();
                                    for(Map<String,Object> counts : ltrCounts){
                                        String status = counts.get("ltrStatus").toString();
                                        if(stats.containsKey(status)){
                                            stats.put(status,stats.get(status).intValue()+1);
                                        }else{
                                            stats.put(status,1);
                                        }
                                    }
                                    log.info("Letter status map has been prepared :: "+stats);
                                    if(stats.get("completed")!=null)
                                        statsReport.setLtrCompletedCount(stats.get("completed"));
                                    if(stats.get("in_progress")!=null)
                                        statsReport.setLtrInProgressCount(stats.get("in_progress"));
                                    if(stats.get("new")!=null)
                                        statsReport.setLtrNewCount(stats.get("new"));
                                    if(stats.get("rejected")!=null)
                                        statsReport.setLtrRejectedCount(stats.get("rejected"));
                                }else{
                                    log.info("Lettrs count recived as empty for AD's "+ adList);
                                }
                                statReports.add(statsReport);
                            }
                        }
                        log.info("Stat reports has been generated::"+ statReports);
                        //report.setStatsReportList(statReports);
                        reportMap.put("statReport", statReports);
                    }
                    log.info("Report has been generated "+reportMap);
                }else{
                    throw new Exception("User should be configure in DB.");
                }
            }else{
                throw new Exception("Sewarth id should pass while generating reports.");
            }
        }catch(Exception e){
            log.error("Error occurred while generating stat report for Sewarth id: "+ sewarthId +" Error::"+e.getMessage());
        }
        return new ResponseEntity<HashMap<String,Object>>(reportMap, HttpStatus.OK);
    }

    private List<LetterReport> populateLetterName(List<LetterReport> letterReports){
        if(letterReports!=null && letterReports.size()>0){
            try {
                log.info("Got letter reports to populate letter name from refrence data::" + letterReports);
                List<ReferenceData> referenceDataList = referenceDataDao.getReferenceData("entity_key = 'letter_type'");
                Map<String, String> map = new HashMap<>();
                for (ReferenceData refData : referenceDataList) {
                    map.put(String.valueOf(refData.getId()), refData.getEntityValue());
                }
                log.info("Letter id -> name map has been prepared..." + map);
                for (LetterReport ltr : letterReports) {
                    log.info("LetterReport ::" + ltr);
                    log.info("LtrType ::" + ltr.getLtrType() + " ==> map " + map.get(ltr.getLtrType()));
                    String ltrNameForReport = getLetterNameForReport(map.get(ltr.getLtrType()));
                    if(ltrNameForReport!=null){
                        ltr.setLtrType(ltrNameForReport);
                    }else{
                        ltr.setLtrType(map.get(ltr.getLtrType()));
                    }

                }
                log.info("Letter report after letter name changed : " + letterReports);
            }catch(Exception e){
                log.error(e.getMessage());
            }
        }
        return letterReports;
    }

    private String getLetterNameForReport(String ltrType){
        Map<String,String> ltrNames = new HashMap<>();
        ltrNames.put("सर्वसाधारण पत्रे","सर्वसाधारण टपाल");
        ltrNames.put("शासकीय पत्रे","शासकीय टपाल");
        ltrNames.put("धनादेश","धनादेश");
        ltrNames.put("अर्धशासकीय पत्रे","अर्धशासकीय टपाल");
        ltrNames.put("मुंबई","महालेखापाल , मुंबई कार्यालयाकडून प्राप्त टपाल / आदेश");
        ltrNames.put("नागपूर","महालेखापाल ,नागपुर कार्यालयाकडून प्राप्त टपाल /आदेश");
        ltrNames.put("DCPS शाखा","DCPS शाखेकरीता टपाल");
        ltrNames.put("धनाकर्ष","धनाकर्ष");
        ltrNames.put("निवासी लेखा","निवासी लेखा परीक्षा अधिकारी, कार्यालयाकडून प्राप्त टपाल");
        ltrNames.put("माहितीची अधिकार पत्रे","माहितीच्या अधिकाराखाली प्राप्त टपाल");
        ltrNames.put("मा.विधानसभा सदस्य, मा सचिव यांच्याकडील पत्रे","मा. लोकप्रतिनिधी यांचेकडून प्राप्त टपाल");
        ltrNames.put("लोकायुक्त कार्यालयाकडून येणारी पत्रे","लोकायुक्त कार्यालयाकडून प्राप्त टपाल");
        ltrNames.put("आपले सरकार","आपले सरकार पोर्टलद्वारा प्राप्त टपाल");
        ltrNames.put("न्यायालयीन पत्रे","न्यायालयीन टपाल");
        ltrNames.put("PPO-मुंबई","PPO-मुंबई");
        ltrNames.put("GPO-मुंबई","GPO-मुंबई");
        ltrNames.put("CPO-मुंबई","CPO-मुंबई");
        ltrNames.put("GPF-मुंबई","GPF-मुंबई");
        ltrNames.put("LTA-मुंबई","LTA-मुंबई");
        ltrNames.put("LETTER-मुंबई","LETTER-मुंबई");
        ltrNames.put("PPO-नागपूर","PPO-नागपूर");
        ltrNames.put("GPO-नागपूर","GPO-नागपूर");
        ltrNames.put("CPO-नागपूर","CPO-नागपूर");
        ltrNames.put("GPF-नागपूर","GPF-नागपूर");
        ltrNames.put("LTA-नागपूर","LTA-नागपूर");
        ltrNames.put("LETTER-नागपूर","LETTER-नागपूर");
        return ltrNames.get(ltrType);
    }

    private boolean isUserRoleIsAD(User user, ReferenceData referenceData){
        boolean result = false;
        List<String> adRoles = new ArrayList<>();
        adRoles.add("लेखापाल");
        adRoles.add("वरिष्ठ लेखापाल");
        if(adRoles.contains(referenceData.getEntityValue())) result = true;
        return result;
    }

    private String getYourReporty(String roleName){
        Map<String,String> map = new HashMap<>();
        map.put("pao","dypao");
        map.put("dypao","apao");
        map.put("apao","so");
        map.put("so","ad");
        return  map.get(roleName);
    }

    private String getRoleName(List<ReferenceData> referenceData, User user){
        String roleName = null;
        for(ReferenceData refData : referenceData){
            if(refData.getId()==user.getRoleId()){
                roleName = getRollMappingForColumnName(refData.getEntityValue());
                break;
            }
        }
        return roleName;
    }

    private ReferenceData getRoleReferenceData(List<ReferenceData> listReferenceData, User user){
        ReferenceData referenceData = null;
        for(ReferenceData refData : listReferenceData){
            if(refData.getId()==user.getRoleId()){
                referenceData = refData;
                break;
            }
        }
        return referenceData;
    }


    private String getRollMappingForColumnName(String role){
        Map<String,String> result = new HashMap<>();
        result.put("उप.अ.व ले.अ.","dypao");
        result.put("स.ले.अ.","so");
        result.put("स.अ.व ले.अ.","apao");
        result.put("मा.अ.व ले.अ.","pao");
        return result.get(role);
    }

}
