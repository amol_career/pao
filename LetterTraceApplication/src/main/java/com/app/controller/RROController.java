package com.app.controller;

import com.app.dao.*;
import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.UserMapping;
import com.app.model.WorkflowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/rro")
public class RROController extends BaseController {

    @Autowired
    private WorkflowDao workflowDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @Autowired
    LetterDao letterDao;

    @Autowired
    BaseDao baseDao;

    @GetMapping(value="/getletters")
    @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getAssignedLtrList(@RequestParam("branch") String branch,
                                                                     @RequestParam("ltrStatus")String ltrStatus) {
        log.info("RROController :: getAssignedLtrList() for branch " + branch + " and ltrStatus " + ltrStatus);
        HashMap<String, Object> letterMap = new HashMap<>();
        try {
            ReferenceData referenceData = referenceDataDao.getReferenceData("entity_value = '" + branch + "'").get(0);

            if ("rejected".equalsIgnoreCase(ltrStatus)) {
                List<WorkflowVO> rejectedLtrWorkflowList = workflowDao.getLtrWorkflowList("dept_id='" + referenceData.getId() + "' and isAssigned=true and ltrStatus in('rejected')");
                for (WorkflowVO workflowVO : rejectedLtrWorkflowList) {
                    List<UserMapping> userMappingList = userMappingDao.getUserMapping("so='" + workflowVO.getFromUser() + "'");
                    if (!userMappingList.isEmpty()) {
                        UserMapping userMapping = userMappingList.get(0);
                        workflowVO.setLtrFromUserName(userMapping.getSo_name());
                    }
                }
                letterMap.put("ltrWorkflowList", rejectedLtrWorkflowList);
            } else if ("outwarded".equalsIgnoreCase(ltrStatus)) {
                List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList(
                        "route_nondani_shakha='" + referenceData.getId() + "' and isAssigned=true and ltrStatus='" + ltrStatus + "'");
                letterMap.put("ltrWorkflowList", ltrWorkflowList);
            } else if ("completed".equalsIgnoreCase(ltrStatus)) {
                //  LocalDate weekBeforeToday = LocalDate.now().minusDays(7);

                List<WorkflowVO> ltrWorkflowListcompleted = workflowDao.getLtrWorkflowList(
                        "route_nondani_shakha='" + referenceData.getId() + "' and isAssigned=true and ltrStatus='" + ltrStatus + "'");
                for (WorkflowVO workflowVO : ltrWorkflowListcompleted) {
                    Date date = new Date(workflowVO.getUpdated_tz().getTime());
                    workflowVO.setLtrCompleteDate(date);
                }

                letterMap.put("ltrWorkflowList", ltrWorkflowListcompleted);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return new ResponseEntity<HashMap<String, Object>>(letterMap, HttpStatus.OK);


    }

	  @GetMapping(value="/getallletters")
	  @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getAllLetters(@RequestParam("branch") String branch){
        log.info("RROController :: getAllLetters()");
        HashMap<String,Object> letterMap = new HashMap<>();
        try {
            List<BaseVO> baseVOList =baseDao.getLetterDispalyList("nondani_shakha='"+branch+"'");
            log.info("Data::"+baseVOList);
            letterMap.put("allLtrList", baseVOList);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<HashMap<String,Object>>(letterMap, HttpStatus.OK);
    }

    @PostMapping(value="/postLetters", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> postLetters(@RequestBody WorkflowVO workflowVO, @RequestParam("sewarthId")String sewarthId,
                                              @RequestParam("ltrStatus")String ltrStatus)throws Exception {
        log.info("RROController ::postLetters() for sewarthID: " + sewarthId + " ltrStatus: " + ltrStatus);

        if ("completed".equalsIgnoreCase(ltrStatus)) {
            List<WorkflowVO> ltrList = workflowDao.getLtrWorkflowList("route_outward is not null and ltrStatus = 'completed'");
            workflowVO.setLtrOutwardNum(workflowVO.getLtrOutwardNum() + "/" + (ltrList.size() + 1));

            letterDao.updateLetter("set refOutwardNum = '" + workflowVO.getLtrOutwardNum() + "'",
                    " where  ltrInwardNum = '" + workflowVO.getLtrInwardNum() + "'");
        }else if("new".equalsIgnoreCase(ltrStatus)){

             List<UserMapping> userMappingList = userMappingDao.getUserMapping("sub_dept_id='" + workflowVO.getDeptId() + "'");
            if (userMappingList != null && !userMappingList.isEmpty()) {
                UserMapping userMapping = userMappingList.get(0);
                workflowVO.setToUser(userMapping.getSo());
            }

        }

        workflowVO.setLtrStatus(ltrStatus);

        int rowAffect=workflowDao.addNewWorkflow(workflowVO);
        if(rowAffect>0)
        {
            return ResponseEntity.ok("Success");
        }
        return ResponseEntity.badRequest().body("Error");
    }

}
