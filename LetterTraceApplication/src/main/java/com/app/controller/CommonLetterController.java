package com.app.controller;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.app.dao.CommonLetterDao;
import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.WorkflowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping(value="/app")
public class CommonLetterController extends BaseLetterController {

    @PostMapping(value="/commonletters/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String saveLetter(ModelMap model, @RequestBody MultiValueMap<String, String> letterinfo) {
        try {

            String lettertypeNamehidden = letterinfo.getFirst("lettertypeName");
            String nondaniShakha = letterinfo.getFirst("nondaniShakha");
            
            String deptName = letterinfo.getFirst("salgnaShakha");
            String ltrSubjectName = letterinfo.getFirst("ltrSubjectName");
            String sewarthId=letterinfo.getFirst("sevarthId");
            String lettertypename=letterinfo.getFirst("lettertypeName");
            String employeeName=letterinfo.getFirst("employeeName");
            String employeebranch=letterinfo.getFirst("employeebranch");
            String itrSubject = letterinfo.getFirst("itrSubject");
            String success ="Your data is submmited Successfully";
            log.info("***** lettertypeNamehidden::"+lettertypeNamehidden);
            log.info("***** sewarthId::"+sewarthId);
            log.info("***** nondaniShakha::"+nondaniShakha);
            log.info("***** deptName::"+deptName);
            log.info("***** ltrSubjectName::"+ltrSubjectName);
            log.info("***** itrSubject::"+itrSubject);

            ReferenceData ltrReferenceData = authenticationDao.getReferenceDataByKeyAndValue("letter_type",lettertypeNamehidden);
            ReferenceData deptReferenceData = authenticationDao.getReferenceDataByKeyAndValue("sub_department",deptName);
            if(ltrReferenceData == null)
               log.warn("Reference data is not available for letter  "+ lettertypeNamehidden);
            if(deptReferenceData == null)
                log.warn("Reference data is not available for department  "+ deptName);

            Map<String,Integer> counts = commonLetterDao.getIndividualAndAllLetterCount(ltrReferenceData.getId(),nondaniShakha);

            log.info("Collected letter counts for inward number ::  "+counts);
            Integer individualLtrCount = 0;
            Integer allLtrCount = 0;

            if(counts.get("IndividualLtrCount")!=null){
                individualLtrCount = counts.get("IndividualLtrCount").intValue();
            }

            if(counts.get("AllLetterCount")!=null){
                allLtrCount = counts.get("AllLetterCount").intValue();
            }

            String inwardNo = generateInwardNumber(sewarthId,nondaniShakha,ltrReferenceData.getEntityValue(),
                    individualLtrCount,allLtrCount);

            log.info("Inward Id has been prepared..."+inwardNo.toString());
            BaseVO vo = populateBaseVOFromModel(letterinfo,sewarthId,
                    inwardNo, nondaniShakha,individualLtrCount+1,ltrReferenceData.getId(),
                    deptReferenceData.getId());

            String toUser = commonLetterDao.getSewartIdForWorkflowNo(vo.getLtrToSubDept(),vo.getLtrAcceptor());

            WorkflowVO workflowVO = populateWorkflowVO(vo.getLtrToSubDept(),sewarthId,toUser,
                                                       vo.getLtrInwardNum(),vo.getLtrSubject(),ltrReferenceData.getId(),
                                                       vo.getLtrDate());

            int rowAffect = commonLetterDao.storeData(vo, workflowVO);

            if(rowAffect>0)	{
            	model.addAttribute("InwardString",inwardNo);
                model.addAttribute("employeebranch",employeebranch);
                model.addAttribute("employeeName",employeeName);
                model.addAttribute("sewarthIdC",sewarthId);
                model.addAttribute("lettertypenameC",lettertypename);
                model.addAttribute("nondaniShakhaByC",nondaniShakha);
                model.addAttribute("successMsg",success+lettertypename);

                log.info("Letter has been stores sucessfully");
                return "commonletters";
            }
        } catch (Exception e) {
            log.error("Error occured while storing letter. :: CommonLetterController :: Error - " + e.getMessage());
        }
        return "page-error-400";
    }

    @RequestMapping(value="/ddomarathiid/",method= RequestMethod.GET,produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchList(@RequestParam("ddomarathiId") String ddomarathiId) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddomarathiId,"MARATHI");

        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: getBranchList :: Error - " + e.getMessage());
        }

        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @RequestMapping(value="/ddoenglishid/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchListE(@RequestParam("ddoEnglishId") String ddoEnglishId) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddoEnglishId,"ENGLISH");
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. ::getBranchListE() :: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @RequestMapping(value="/ddonumberid/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBranchListN(@RequestParam("ddoNumberId") String ddonumberid)
    {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOSelectBranches(ddonumberid,"DDONUMBER");
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. ::getBranchListN:: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @GetMapping(value="/ltrmediumdropdown/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchLetterMediumDropdown() {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchLtrMediumDropdown();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchLetterMediumDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @GetMapping(value="/ltracceptor/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchLetterAcceptorDropdown() {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchLtrAcceptorDropdown();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchLetterMediumDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return  new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @GetMapping(value="/ltrmainbranch/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchLetterMainBranchDropdown()	{
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchLtrMainBranchDropdown();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchLetterMainBranchDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @RequestMapping(value="/mbranchval/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getMainBranchVal(@RequestParam("Mainbranchval") String mainbranchval) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchSubBranches(mainbranchval);
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: getMainBranchVal :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }
    @RequestMapping(value="/getSubBranches/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ReferenceData>> getSubBranchVal(@RequestParam("Mainbranchval") String mainbranchval) {
        log.info("Entering into CommonLetterController::getSubBranchVal()");
        List<ReferenceData> datalist = null;
        try{
            datalist = commonLetterDao.fetchSubBranchesFromMain(mainbranchval);
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: getMainBranchVal :: CommonLetterController :: Error - " + e.getMessage());
        }
        log.info("Exiting from CommonLetterController::getSubBranchVal()");
        return new ResponseEntity<List<ReferenceData>>(datalist, HttpStatus.OK);
    }



    @RequestMapping(value="/lettersubject/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getLetterSubject(@RequestParam("Subbranchval") String subbranchval) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchltrSubject(subbranchval);
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: getLetterSubject :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @GetMapping(value="/ltrddodropdown/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchDdoDropDown() {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchDDOInfoDropDown();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchDdoDropDown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }

    @GetMapping(value="/ltrSubBranchforsarvshakha/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchLetterSubBranchDropdown()    {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchLtrSubBranchDropdownforSarvShakha();
        }catch(Exception e){
            log.error("Error while preparing DDO info for dept. :: fetchLetterSubBranchDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }
    
    @GetMapping(value="/bankNamesForDropDown/", produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<HashMap<String,Object>>> fetchBankNameDropdownFromDB()    {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchBankNameDropdown();
           
        }catch(Exception e){
            log.error("Error while preparing Bank Names  :: fetchBankNameDropdown :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }
    @RequestMapping(value="/bankval/",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<HashMap<String,Object>>> getBankBranchVal(@RequestParam("bankName") String bankName) {
        List<HashMap<String,Object>> datalist = null;
        try{
            datalist = commonLetterDao.fetchBankBranchNameDropdown(bankName);
        }catch(Exception e){
            log.error("Error while preparing BankBranch. :: getMainBranchVal :: CommonLetterController :: Error - " + e.getMessage());
        }
        return new ResponseEntity<List<HashMap<String,Object>>>(datalist, HttpStatus.OK);
    }


}
