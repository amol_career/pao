package com.app.controller;

import com.app.model.BaseVO;
import com.app.model.ReferenceData;
import com.app.model.WorkflowVO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value="/app")
public class MumbaiAndNagpurLtrController extends BaseLetterController {


    @Override
    @PostMapping(value="/mumbai&NagpurLetters/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    protected String saveLetter(ModelMap model,@RequestBody MultiValueMap<String, String> letterInfo) {
        try {
            Map<String, String> inwardNos = new HashMap<>();
            List<String> ltrTypes = letterInfo.get("praptcheckbox");
            String sewarthId=letterInfo.getFirst("sevarthId");
            String nondaniShakha = letterInfo.getFirst("nondaniShakha");
            String lettertypename=letterInfo.getFirst("lettertypeName");
            String employeeName=letterInfo.getFirst("employeeName");
            String employeebranch=letterInfo.getFirst("employeebranch");
            String success ="Your data is submmited Successfully";


            if(ltrTypes != null && ltrTypes.size()>0){
                for(String ltrType : ltrTypes){
                    String subDept = null;
                    BaseVO baseVO = null;
                    WorkflowVO workflowVO = null;
                    StringBuffer subInwardNos = null;
                    ReferenceData ltrReferenceData = authenticationDao.getReferenceDataByKeyAndValue("letter_type",ltrType+"-"+lettertypename);
                    if("LETTER".equals(ltrType)){
                        List<String> subDepts = letterInfo.get("salgnaShakha");
                        int subLtrCount = 1;
                        String inwardNo = null;
                        subInwardNos = new StringBuffer();
                        for(String dept : subDepts){
                            baseVO = generateBaseVO(letterInfo,ltrType,dept,lettertypename,ltrReferenceData);
                            if(inwardNo == null){
                                inwardNo = baseVO.getLtrInwardNum();
                            }
                            if(subDepts.size()==1){
                                baseVO.setLtrInwardNum(inwardNo);
                            }else{
                                baseVO.setLtrInwardNum(inwardNo+"-"+subLtrCount);
                            }
                            log.info("Sub Invert Id for letter has been generated.."+baseVO.getLtrInwardNum());
                            String toUser = commonLetterDao.getSewartIdForWorkflowNo(baseVO.getLtrToSubDept(),baseVO.getLtrAcceptor());
                            workflowVO = populateWorkflowVO(baseVO.getLtrToSubDept(),sewarthId,toUser,
                                    baseVO.getLtrInwardNum(),baseVO.getLtrSubject(),ltrReferenceData.getId(),
                                    baseVO.getLtrDate());
                            int rowAffect = commonLetterDao.storeData(baseVO, workflowVO);
                            if(rowAffect > 0){
                                if(subLtrCount ==1){
                                    subInwardNos.append(workflowVO.getLtrInwardNum());
                                }else{
                                    subInwardNos.append(",").append(workflowVO.getLtrInwardNum());
                                }
                            }
                            subLtrCount ++;
                        }
                        inwardNos.put(ltrType,subInwardNos.toString());
                    }else{
                        subDept = letterInfo.getFirst(ltrType.toLowerCase()+"submenu");
                        baseVO = generateBaseVO(letterInfo,ltrType,subDept,lettertypename,ltrReferenceData);
                        String toUser = commonLetterDao.getSewartIdForWorkflowNo(baseVO.getLtrToSubDept(),baseVO.getLtrAcceptor());
                        workflowVO = populateWorkflowVO(baseVO.getLtrToSubDept(),sewarthId,toUser,
                                baseVO.getLtrInwardNum(),baseVO.getLtrSubject(),ltrReferenceData.getId(),
                                baseVO.getLtrDate());
                        int rowAffect = commonLetterDao.storeData(baseVO, workflowVO);
                        if(rowAffect > 0){
                            inwardNos.put(ltrType,workflowVO.getLtrInwardNum());
                        }
                    }
                }

                for(String key : inwardNos.keySet()){
                    model.addAttribute(key+"InwardNo",inwardNos.get(key));
                }
                model.addAttribute("employeebranch",employeebranch);
                model.addAttribute("employeeName",employeeName);
                model.addAttribute("sewarthIdC",sewarthId);
                model.addAttribute("lettertypenameC",lettertypename);
                model.addAttribute("nondaniShakhaByC",nondaniShakha);

                //  success
                model.addAttribute("successMsg",success+ " - "+lettertypename);
                log.info("Letter has been stores successfully");
            }else {
                log.error("Letter type should not null");
            }
            return "MumbaiAndNagpurLetter";

        } catch (Exception e) {
            log.error("Error occured while storing letter. :: ChequeController :: Error - " + e.getMessage());
        }
        return "page-error-400";
    }

    private BaseVO generateBaseVO(MultiValueMap<String, String> letterInfo, String ltrType, String subDept,
                                  String lettertypename,
                                  ReferenceData ltrReferenceData) throws Exception{
        BaseVO baseVO = null;
        String sewarthId = letterInfo.getFirst("sevarthId");
        String nondaniShakha = letterInfo.getFirst("nondaniShakha");
        String ltrRevised = letterInfo.getFirst(ltrType.toLowerCase()+"Revised");
        String supplement = letterInfo.getFirst(ltrType.toLowerCase()+"supplement");
        ReferenceData deptReferenceData = authenticationDao.getReferenceDataByKeyAndValue("sub_department",subDept);
        String ltrAcceptor =  letterInfo.getFirst(ltrType.toLowerCase()+"LtrAccepter");
        if(ltrReferenceData == null)
            log.warn("Reference data is not available for letter  "+ lettertypename);
        if(deptReferenceData == null)
            log.warn("Reference data is not available for department  "+ subDept);

        Map<String,Integer> counts = commonLetterDao.getIndividualAndAllLetterCount(ltrReferenceData.getId(),nondaniShakha);

        log.info("Collected letter counts for inward number ::  "+counts);
        Integer individualLtrCount = 0;
        Integer allLtrCount = 0;

        if(counts.get("IndividualLtrCount")!=null){
            individualLtrCount = counts.get("IndividualLtrCount").intValue();
        }

        if(counts.get("AllLetterCount")!=null){
            allLtrCount = counts.get("AllLetterCount").intValue();
        }

        String inwardNo = generateInwardNumber(sewarthId,nondaniShakha,ltrReferenceData.getEntityValue(),
                individualLtrCount,allLtrCount);

        log.info("Inward Id has been prepared..."+inwardNo);
        baseVO = populateBaseVOFromModel(letterInfo,sewarthId,
                inwardNo, nondaniShakha,individualLtrCount+1,ltrReferenceData.getId(),
                deptReferenceData.getId());
        //Set explicitly acceptor for mumbai & Nagpur letter
        if(ltrRevised !=null && ltrRevised.equals("Yes")) baseVO.setLtrReceived(true);
        if(supplement !=null && supplement.equals("Yes")) baseVO.setLtrSupplement(true);

        baseVO.setLtrRevised(true);
        baseVO.setLtrAcceptor(ltrAcceptor);
        return baseVO;
    }

}
