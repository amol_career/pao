package com.app.controller;

import com.app.dao.ReferenceDataDao;
import com.app.dao.UserMappingDao;
import com.app.dao.WorkflowDao;
import com.app.model.ReferenceData;
import com.app.model.UserMapping;
import com.app.model.WorkflowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/pao")
public class PAOController extends BaseController {

    @Autowired
    private WorkflowDao workflowDao;

    @Autowired
    private UserMappingDao userMappingDao;

    @Autowired
    private ReferenceDataDao referenceDataDao;

    @GetMapping(value="/getletters")
    @ResponseBody
    public ResponseEntity<HashMap<String,Object>> getAssignedLtrList(@RequestParam("sewarthId") String sewarthId){
        log.info("PAOController ::getAssignedLtrList() for sewarthId " + sewarthId);
        HashMap<String,Object> letterMap = new HashMap<>();

        try {
            List<WorkflowVO> ltrWorkflowList = workflowDao.getLtrWorkflowList("toUser='"+sewarthId+"' and isAssigned=true");
            letterMap.put("ltrWorkflowList", ltrWorkflowList);

            List<ReferenceData> listLtrCategory = referenceDataDao.getReferenceData("entity_key = 'category'");
            letterMap.put("listLtrCategory", listLtrCategory);

            List<ReferenceData> listSubDept = referenceDataDao.getReferenceData("entity_key = 'sub_department' and entity_value not in ('अधिकारी')");
            letterMap.put("listSubDept", listSubDept);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<HashMap<String,Object>>(letterMap, HttpStatus.OK);
    }

    @GetMapping(value="/getemployees")
    @ResponseBody
    public ResponseEntity<HashMap<String, String>> getEmployees(@RequestParam("deptId") String deptId) {
        HashMap<String, String> eligibleEmps = new HashMap();

        List<UserMapping> listEmp = userMappingDao.getUserMapping("sub_dept_id = " + deptId +"");
        for (UserMapping emp :listEmp) {
            if (! eligibleEmps.containsKey(emp.getDypao())) {
                eligibleEmps.put(emp.getDypao(), emp.getDypao_name());
            }

            if (! eligibleEmps.containsKey(emp.getApao())) {
                eligibleEmps.put(emp.getApao(), emp.getApao_name());
            }
        }

        return new ResponseEntity<>(eligibleEmps, HttpStatus.OK);
    }

    @PostMapping(value="/postLetters", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> postLetters(@RequestParam("inward") String inward, @RequestParam("sub") String sub,
                                         @RequestParam("date") String date,
                                         @RequestParam("deptId") String deptId, @RequestParam("ltrType") String ltrType,
                                         @RequestParam("emp") String emp, @RequestParam("sewarthId") String sewarthId)throws Exception {
        log.info("SectionOfficerController ::postLetters() for sewarthID " + sewarthId);
        WorkflowVO workflowVO = new WorkflowVO();

        workflowVO.setLtrInwardNum(inward);
        workflowVO.setLtrSubject(sub);
        workflowVO.setFromUser(sewarthId);
        workflowVO.setToUser(emp);
        workflowVO.setLtrDate(getFormattedUIDate(date));
        workflowVO.setLtrStatus("in_progress");
        if (! "".equalsIgnoreCase(ltrType)) {
            workflowVO.setLtrType(Integer.parseInt(ltrType));
        }
        if (! "".equalsIgnoreCase(deptId)) {
            workflowVO.setDeptId(Integer.parseInt(deptId));
        }

        int rowAffect=workflowDao.addNewWorkflow(workflowVO);
        if(rowAffect>0)
        {
            return ResponseEntity.ok("Success");
        }
        return ResponseEntity.badRequest().body("Error");
    }

}
