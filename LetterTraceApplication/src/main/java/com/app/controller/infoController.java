package com.app.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.app.dao.InfoDao;

@Controller
@RequestMapping(value="/app")
public class infoController
{
	 private static final Logger LOGGER=LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private InfoDao infodao;
	
	@PostMapping(value="/infodata/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addInfoData(@RequestBody MultiValueMap<String, String> addinfo)
	{
		LOGGER.info("Entering into infoController addInfoData()");
		
		String date=addinfo.getFirst("date");
		String subject=addinfo.getFirst("subject");
		String info=addinfo.getFirst("info");
		String file=addinfo.getFirst("file");
		
		System.out.println("data::"+subject+"/"+date+"/"+info+"/"+file);
		LOGGER.info("Existing data ::"+subject+"/"+date+"/"+info+"/"+file);
	
			int rowAffect=infodao.AddInfo(subject,date,info, file);
			if(rowAffect>0)
			{
				return "inoffice";
			}
			System.out.println("Result::"+rowAffect);
			
			LOGGER.info("Result data ::"+rowAffect);
			LOGGER.info("Exit from infoController addInfoData()");

		return "page-error-400";
	}
	
	@PostMapping(value="/infodata2/",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addInfoData2(@RequestBody MultiValueMap<String, String> addinfo)
	{
		LOGGER.info("Entering into infoController addInfoData2()");
		
		String date=addinfo.getFirst("date");
		String subject=addinfo.getFirst("subject");
		String info=addinfo.getFirst("info");
		String file=addinfo.getFirst("file");
		
		System.out.println("data::"+subject+"/"+date+"/"+info+"/"+file);
			
			int rowAffect=infodao.AddInfo2(subject,date,info, file);
			if(rowAffect>0)
			{
				return "outo ffice";
			}
			System.out.println("Result::"+rowAffect);
			LOGGER.info("Result::"+rowAffect);

			LOGGER.info("Exit from infoController addInfoData2()");

		return "page-error-400";
	
	  }
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/infodata/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchEmpInfo()
	{
		LOGGER.info("Entering into infoController fetchEmpInfo()");

		
		Map<String,Object> ldata=null;
	     List<Map> datalist=infodao.fetchInfo();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			System.out.println("data"+ldata);
		}
		
		LOGGER.info("Current Data::"+ldata);

		LOGGER.info("Exit from infoController fetchEmpInfo()");
     	return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping(value="/infodata2/", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity< List<Map>> fetchInfo2()
	{
		LOGGER.info("Entering into infoController fetchInfo2()");

		
		Map<String,Object> ldata=null;
	     List<Map> datalist=infodao.fetchInfo2();
		for(int i=0;i<datalist.size();i++)
		{
			ldata=datalist.get(i);
			System.out.println("data"+ldata);
			LOGGER.info("Current Data::"+ldata);

		}
		LOGGER.info("Exit into infoController fetchInfo2()");
		return new ResponseEntity<List<Map>>(datalist, HttpStatus.OK);
	}
	
		
}
