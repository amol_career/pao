package com.app.controller;

import com.app.dao.UserMappingDao;
import com.app.model.UserMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/userhierarchy")
public class UserMappingController extends BaseController{

    @Autowired
    UserMappingDao userMappingDao;

    @GetMapping(value="/getUserHierarchyList", produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    private ResponseEntity<List<UserMapping>> getUserMapping(@RequestParam("where") String where)
    {
        log.info( getClass().getName() + " :: getUserMappingList()");
        List<UserMapping> userMappingList;
        userMappingList = userMappingDao.getUserMapping(where);
        return new ResponseEntity<List<UserMapping>>(userMappingList, HttpStatus.OK);
    }

    @PostMapping(value="/createUserHierarchy", consumes="application/json;charset=UTF-8")
    public long createUserHierarchy(@RequestBody UserMapping userMapping){
        log.info( getClass().getName() + " :: createUserHierarchy()");
        return userMappingDao.createUserMapping(userMapping);
    }

    @PostMapping(value="/updateUserHierarchy", consumes="application/json;charset=UTF-8")
    public void updateUserHierarchy(@RequestBody UserMapping userMapping){
        log.info( getClass().getName() + " :: updateUserHierarchy()");
        userMappingDao.updateUserMapping(userMapping);
    }

    @PostMapping(value="/deleteUserHierarchy", consumes="application/json;charset=UTF-8")
    public boolean deleteUserHierarchy(@RequestParam("id") int id){
        log.info( getClass().getName() + " :: deleteUserHierarchy()");
        return userMappingDao.deleteUserMapping(id);
    }


}
