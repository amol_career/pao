package com.app.controller;

import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class BaseLetterController extends BaseController {

	protected  abstract String saveLetter(ModelMap model, @RequestBody MultiValueMap<String, String> letterinfo);

}